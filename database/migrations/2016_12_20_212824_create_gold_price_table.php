<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoldPriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gold_price', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_name');
            $table->string('muavao')->nullable();;
            $table->string('banra')->nullable();;
            $table->string('thaydoi',20)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gold_price');
    }
}
