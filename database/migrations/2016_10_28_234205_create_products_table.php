<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned();
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');
            $table->integer('category_product_id')->unsigned();
            $table->foreign('category_product_id')->references('id')->on('category_products')->onDelete('cascade');
            $table->string('title');
            $table->text('tomtat');
            $table->float('laisuat', 8, 2);
            $table->integer('vaytoida')->unsigned();
            $table->integer('vaytoida_str')->unsigned()->default(0);
            $table->integer('thoihan')->unsigned();
            $table->text('chitiet');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
