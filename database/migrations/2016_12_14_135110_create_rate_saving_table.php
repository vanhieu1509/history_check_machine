<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateSavingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_saving', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned();
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');
            $table->integer('sotiengui');
            $table->string('month1',6);
            $table->string('month3',6);
            $table->string('month6',6);
            $table->string('month12',6);
            $table->string('month24',6);
            $table->string('unlimited',6);
            $table->string('date_update',15)->nullable();
            $table->string('date_new',15)->nullable();
            $table->tinyInteger('auto')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_saving');
    }
}
