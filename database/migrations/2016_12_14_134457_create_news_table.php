<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_news_id')->unsigned();
            $table->foreign('category_news_id')->references('id')->on('category_news')->onDelete('cascade');
            $table->string('title');
            $table->string('slug');
            $table->text('sumary');
            $table->text('content');
            $table->string('image')->nullable();
            $table->string('source')->nullable();
            $table->tinyInteger('showimg')->nullable()->default(1);
            $table->integer('number_view')->default(0);
            $table->string('staff_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
