<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_products')->insert([
            'id' => 1,
            'name' => 'Vay mua bất động sản',
        ]);
        DB::table('category_products')->insert([
            'id' => 2,
            'name' => 'Vay mua xe',
        ]);
        DB::table('category_products')->insert([
            'id' => 3,
            'name' => 'Vay mua nhà',
        ]);
        DB::table('category_products')->insert([
            'id' => 4,
            'name' => 'Vay tiêu dùng',
        ]);
        DB::table('category_products')->insert([
            'id' => 5,
            'name' => 'Vay kinh doanh',
        ]);
        DB::table('category_products')->insert([
            'id' => 6,
            'name' => 'Vay xây dựng - sửa nhà',
        ]);
        DB::table('category_products')->insert([
            'id' => 7,
            'name' => 'Vay sản xuất nông nghiệp',
        ]);
        DB::table('category_products')->insert([
            'id' => 8,
            'name' => 'Vay du học',
        ]);
        DB::table('category_products')->insert([
            'id' => 9,
            'name' => 'Thấu chi có đảm bảo',
        ]);
        DB::table('category_products')->insert([
            'id' => 10,
            'name' => 'Vay mua nhà ở xã hội',
        ]);
        DB::table('category_products')->insert([
            'id' => 11,
            'name' => 'Vay chứng khoán',
        ]);
        DB::table('category_products')->insert([
            'id' => 12,
            'name' => 'Vay cầm cố chứng từ có giá',
        ]);
    }
}
