<?php

use Illuminate\Database\Seeder;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 16; $i++) {
            DB::table('staff')->insert([
                'id' => $i+1,
                'email' => $faker->email,
                'password' => '4297f44b13955235245b2497399d7a93',
                'fullname' => $faker->name ,
                'level' => $faker->randomElement($array = array ('admin','st_consult','st_news')),
                'number_phone' =>'09'.$faker->numberBetween(100000000,999999999),
                'avatar' => 'http://localhost/Finance/storage/app/public/staff/avatar/default.png',
                'bank_id' =>  $faker->numberBetween($min = 1, $max = 16),
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ]);
        }
    }
}
