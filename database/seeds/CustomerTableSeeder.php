<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 100; $i++) {
            DB::table('customers')->insert([
                'id' => $i+1,
                'fullname' => $faker->text($maxNbChars = 6) ,
                'number_phone' => '09'.$faker->numberBetween(10000000,99999999),
                'email' => $faker->email ,
                'province_id' => $faker->numberBetween(1,100),
                'address' => $faker->address,
                'gender' => $faker->randomElement($array = array ('0','1')),
                'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'job' =>$faker->jobTitle,
                'inconme' => $faker->numberBetween(100000000,999999999),
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ]);
        }
    }
}
