<?php

use Illuminate\Database\Seeder;

class CategoryConsultStaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 100; $i++) {
            DB::table('category_consult_staff')->insert([
                'id' => $i+1,
                'staff_id' => $i+1,
                'category_consult_id' => 1,
            ]);
        }
        for ($i = 100; $i < 200; $i++) {
            if($i %2 ==0){
                DB::table('category_consult_staff')->insert([
                    'id' => $i+1,
                    'staff_id' => $i-99,
                    'category_consult_id' => 2,
                ]);
            }
        }
        for ($i = 200; $i < 300; $i++) {
            if($i % 3 ==0){
                DB::table('category_consult_staff')->insert([
                    'id' => $i+1,
                    'staff_id' => $i-199,
                    'category_consult_id' => 3,
                ]);
            }
        }
    }
}
