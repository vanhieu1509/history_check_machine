<?php

use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 1; $i < 100; $i++) {
            DB::table('banks')->insert([
                'id' => $i+1,
                'name' => $faker->text($maxNbChars = 6) ,
                'fullname' => $faker->name,
                'link' => 'http://'.$faker->text($maxNbChars = 6).'.com.vn' ,
                'intro' => $faker->text($maxNbChars = 50),
                'logo' => 'http://localhost/Finance/storage/app/public/avatar/default.png',
                'created_at' => DB::raw('NOW()'),
            ]);
        }
    }
}
