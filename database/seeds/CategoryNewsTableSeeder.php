<?php

use Illuminate\Database\Seeder;

class CategoryNewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_news')->insert([
            'id' => 1,
            'name' =>"Tin Trong Nước",
            'slug' =>str_slug('Tin Trong Nước', '-')
        ]);
        DB::table('category_news')->insert([
            'id' => 2,
            'name' =>"Tin Thế Giới",
            'slug' =>str_slug('Tin Thế Giới', '-')
        ]);
        DB::table('category_news')->insert([
            'id' => 3,
            'name' =>"Tin Khuyến Mãi",
            'slug' =>str_slug('Tin Khuyến Mãi', '-')
        ]);
    }
}
