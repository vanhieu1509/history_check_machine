<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GoldPriceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gold_price')->insert([
            'id' => 1,
            'bank_name' =>"ĐÔNG Á BANK",
            'muavao' =>'',
            'banra' =>'',
            'thaydoi' =>''
        ]);
        DB::table('gold_price')->insert([
            'id' => 2,
            'bank_name' =>"VIETINBANK",
            'muavao' =>'',
            'banra' =>'',
            'thaydoi' =>''
        ]);
        DB::table('gold_price')->insert([
            'id' => 3,
            'bank_name' =>"TPBANK",
            'muavao' =>'',
            'banra' =>'',
            'thaydoi' =>''
        ]);
        DB::table('gold_price')->insert([
            'id' => 4,
            'bank_name' =>"MARITIME BANK",
            'muavao' =>'',
            'banra' =>'',
            'thaydoi' =>''
        ]);
        DB::table('gold_price')->insert([
            'id' => 5,
            'bank_name' =>"SACOMBANK",
            'muavao' =>'',
            'banra' =>'',
            'thaydoi' =>''
        ]);
        DB::table('gold_price')->insert([
            'id' => 6,
            'bank_name' =>"SCB",
            'muavao' =>'',
            'banra' =>'',
            'thaydoi' =>''
        ]);
        DB::table('gold_price')->insert([
            'id' => 7,
            'bank_name' =>"EXIMBANK",
            'muavao' =>'',
            'banra' =>'',
            'thaydoi' =>''
        ]);
    }
}
