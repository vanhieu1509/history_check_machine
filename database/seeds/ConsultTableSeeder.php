<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        ;
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 100; $i++) {
            DB::table('consults')->insert([
                'id' => $i+1,
                'customer_id' => $i+1,
                'category_consult_id' =>$faker->randomElement($array = array ('1','2','3')),
                'staff_id' =>0,
                'content' => $faker->text($maxNbChars = 200),
                'status' => $faker->randomElement($array = array ('1','2','0')),
                'note' =>  $faker->text($maxNbChars = 30),
                'created_at' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'updated_at' =>$faker->date($format = 'Y-m-d', $max = 'now'),
            ]);
        }
    }
}
