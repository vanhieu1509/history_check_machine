<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 1; $i < 100; $i++) {
            DB::table('provinces')->insert([
                'id' => $i+1,
                'name' => $faker->country,
                'type' =>$faker->randomElement($array = array ('Thành Phố','Tỉnh')),
            ]);
        }
    }
}
