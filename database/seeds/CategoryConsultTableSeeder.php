<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryConsultTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_consults')->insert([
            'id' => 1,
            'name' => 'Vay Vốn',
        ]);
        DB::table('category_consults')->insert([
            'id' => 2,
            'name' => 'Gửi Tiết Kiệm',
        ]);
        DB::table('category_consults')->insert([
            'id' => 3,
            'name' => 'Khác',
        ]);
    }
}
