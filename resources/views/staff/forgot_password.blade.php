<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!--Bootstrap 3-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Theme style AdminLTE -->
    <link rel="stylesheet" href="{{asset('lib/AdminLTE/dist/css/AdminLTE.min.css')}}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <style>
        .panel{
            margin: 100px auto;
            background: #f2f2f2;
            border-radius: 10px;
            max-width: 400px;
        }
        body {
            background: url("{{asset("public/img/login_background.jpg")}}") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .box-body button{
            margin-top: 20px;

        }
        .form-group{
            margin: 0px;
            padding: 0px;
        }
        .text-center p{
            margin: 0px;
        }
        h2{
            color: #188288;
        }
        .modal-content{
            border-radius: 6px;
        }
        .modal-header{
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }


    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div >
            <div class="panel panel-default ">
                <div class="panel-body">
                    <section class="login-form">
                        <div class="text-center">
                            <img src="{{asset("public/img/finance_logo.png")}}">
                            <h2 class="text-center">Forgot Password?</h2>
                            <p>You can reset your password here.</p>
                            <div class="panel-body">
                                <form class="form-horizontal" role="forgot">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <div class="input-group input-group-lg">
                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                <input type="email" name="email" class="form-control" placeholder="Email">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <button type="button" id="btn_send" class="btn btn-lg btn-primary btn-block">Send</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </section>
                    <div>
                            <a  class="col-md-6" href="login">Back to Login Page</a>
                            <img id="progress" style="padding: 3px; text-align: center" src="{{asset('public/img/bar_progress.gif')}}" alt="" class="col-md-6" >
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal modal-forgot fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"> <!--Background header-->
                <button type="button" style="color: white" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">
                    <!--Title-->
                </h4>
            </div>
            <div class="modal-body">
                <p class="text-center">
                    <!--Content-->
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_close" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- jQuery 2.2.3 -->
<script src="{{asset('lib/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!--Bootstrap 3-->
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#progress').hide();
        $('#btn_send').on('click',function () {
            var input = $('form[role=forgot]').serialize();
            $.ajax({
                url:"{{route('controller/staff/forgot-password')}}",
                type: "POST",
                dataType: "json",
                data: input,
                beforeSend:function () {
                    $('#progress').show();
                },
                success: function (result) {
                    $('#progress').hide();

                    if(result['status']=='Success'){
                        $('.modal-header').css("background-color","#68CC46");
                        $('.modal-title').html('<p style="color: white">Gửi yêu cầu thành công</p>');
                        $('.modal-body p').html(
                                '<p>Yêu cầu tạo lại password đã được gửi đến Email của bạn. <br>' +
                                'Vui lòng kiểm tra hộp thư và làm theo hướng dẫn!</p>'
                        );
                        $('.modal-body p').addClass('text-success');
                        $('.modal-forgot').modal('show');
                        setTimeout(function(){
                            window.location.href = "login";
                        }, 3000);
                    }else {
                        $('.modal-header').css("background-color","#E8075A");
                        $('.modal-title').html('<p style="color: white">Gửi yêu cầu thất bại</p>');

                        $('.modal-body p').html(result['error']);
                        $('.modal-body p').addClass('text-danger');
                        $('.modal-forgot').modal('show');
                    }
                }
            })
        });
    });
</script>
</body>
</html>