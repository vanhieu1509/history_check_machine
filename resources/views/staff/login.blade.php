<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Bootstrap 3-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Theme style AdminLTE -->
    <link rel="stylesheet" href="{{asset('lib/AdminLTE/dist/css/AdminLTE.min.css')}}">
    <title>Login</title>
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            outline: none;
        }
        body {
            background: url("{{asset("public/img/login_background.jpg")}}") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .login-form {
            font: 16px/2em Lato, serif;
            margin: 100px auto;
            max-width: 400px;
        }

        form[role=login] {
            color: #5d5d5d;
            background: #f2f2f2;
            padding: 26px;
            border-radius: 10px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
        }
        form[role=login] img {
            display: block;
            margin: 0 auto;
            margin-bottom: 20px;
        }
        form[role=login] input{
            border-radius: 6px;
        }
        form[role=login] input,
        form[role=login] button {
            font-size: 18px;
            margin: 16px 0;
        }
        form[role=login] > div {
            text-align: center;
            text-decoration: underline;
        }
        h2{
            color: #188288;
        }
        .modal-content{
            border-radius: 6px;
        }
        .modal-header{
            border-top-right-radius: 6px;
            border-top-left-radius: 6px;
        }
        /*.form-links {*/
        /*text-align: center;*/
        /*margin-top: 1em;*/
        /*margin-bottom: 50px;*/
        /*}*/
        /*.form-links a {*/
        /*color: #fff;*/
        /*}*/
    </style>
</head>
<body >
<section class="container">
    <section class="login-form">
        <form method="post" action="" role="login">
            <img src="{{asset("public/img/logokt.png")}}"  class="img-responsive" alt="" />
            <h2 class="text-center">Đăng Nhập Tra Cứu Lịch Sử Máy</h2>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="email" id="email" name="email" placeholder="Email" required class="form-control input-lg" />
            <input type="password" id="password" name="password" placeholder="Password" required class="form-control input-lg" />
            <button type="button" id="btn_login" class="btn btn-lg btn-primary btn-block">Sign in</button>
            <div>
                <a href="forgot-password">Forgot password</a>
            </div>
        </form>
        {{--<div class="form-links">--}}
        {{--<a href="">www.website.com</a>--}}
        {{--</div>--}}
    </section>
</section>
<div class="modal modal-login fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" style="color: white">Login Error</h4>
            </div>
            <div class="modal-body">
                <p class="text-center text-danger"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- jQuery 2.2.3 -->
<script src="{{asset('lib/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!--Bootstrap 3-->
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        function Login(){
            var input = $('form[role=login]').serialize();
            var aaaa = $('form[role=login]').serializeArray();
            $.each(aaaa, function(i, field){
                console.log(field.name + ":" + field.value + " ");
            });
            $.ajax({
                url: "{{route('controller/login')}}",
                data:input,
                dataType: "json",
                type: "post",
                success: function (result) {
                    if(result['status'] =='Success'){
                        if(result['level'] == 'admin' || result['level'] == 'st_news'){
                            window.location.href = "{{route('home')}}"
                        }else {
                            window.location.href = "{{route('staff/home')}}"
                        }
                    }else{
                        $('.modal-header').css("background-color","#E8075A");
                        $('.modal-body p').html(result['error']);
                        $('.modal-login').modal('show');
                    }
                }
            });
        }
        $('#btn_login').on('click',function () {
            Login();
        });
        $(document).keypress(function(e) {
            if(e.which == 13) {
                Login();
            }
        });
    });
</script>
</body>
</html>