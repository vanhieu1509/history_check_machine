<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Bootstrap 3-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Theme style AdminLTE -->
    <link rel="stylesheet" href="{{asset('lib/AdminLTE/dist/css/AdminLTE.min.css')}}">
    <title>Reset Password</title>
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            outline: none;
        }
        body {
            background: url("{{asset("public/img/login_background.jpg")}}") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .login-form {
            font: 16px/2em Lato, serif;
            margin: 100px auto;
            max-width: 400px;
        }

        form[role=reset_password] {
            color: #5d5d5d;
            background: #f2f2f2;
            padding: 26px;
            border-radius: 10px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
        }
        form[role=reset_password] img {
            display: block;
            margin: 0 auto;

        }
        form[role=reset_password] input{
            border-radius: 6px;
        }
        form[role=reset_password] input,
        form[role=reset_password] button {
            font-size: 18px;
            margin: 16px 0;
        }
        form[role=reset_password] > div {
            text-align: center;
            text-decoration: underline;
        }
        h2{
            color: #188288;
            margin-top: 10px;
            margin-bottom: 5px;
        }
    </style>
</head>
<body >
    <section class="container">
        <section class="login-form ">

            <form method="post" action="" role="reset_password">
                <img src="{{asset("public/img/finance_logo.png")}}"  class="img-responsive" alt="" />
                <h2 class="text-center">Reset Password</h2>
                <p class="text-center"> Enter New Password</p>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="email" value="{{$email}}">
                <input type="password" name="password" placeholder="New Password"  class="form-control input-lg" />
                <input type="password" name="password_confirmation" placeholder="Confirm Password"  class="form-control input-lg" />
                <button type="button" id="btn_reset" class="btn btn-lg btn-primary btn-block">Reset</button>
                <a href="login">Back to Login page</a>

            </form>
        </section>
    </section>
    <div class="modal modal-forgot fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"> <!--Background header-->
                    <button type="button" style="color: white" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                    <h4 class="modal-title">
                        <!--Title-->
                    </h4>
                </div>
                <div class="modal-body">
                    <p class="text-center">
                        <!--Content-->
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn_close" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<!-- jQuery 2.2.3 -->
<script src="{{asset('lib/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!--Bootstrap 3-->
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btn_reset').on('click',function () {
                var input = $('form').serialize();
                $.ajax({
                    url: '{{route('controller/staff/reset-password')}}',
                    dataType: 'json',
                    type: 'POST',
                    data: input,
                    success: function (result) {
                        if(result['status']=='Success'){
                            $('.modal-header').css("background-color","#68CC46");
                            $('.modal-title').html('<p style="color: white">Thành Công</p>');
                            $('.modal-body p').html(
                                    '<h4>Password đã được thay đổi!</h4>'
                            );
                            $('.modal-body p').addClass('text-success');
                            $('.modal-forgot').modal('show');
                            setTimeout(function(){
                                window.location.href = "login";
                            }, 3000);
                        }else {
                            $('.modal-header').css("background-color","#E8075A");
                            $('.modal-title').html('<p style="color: white">Lỗi</p>');

                            $('.modal-body p').html('<h4>'+result['error']+'</h4>');
                            $('.modal-body p').addClass('text-danger');
                            $('.modal-forgot').modal('show');
                        }
                    }
                });
            });
        });
    </script>
</body>
</html>