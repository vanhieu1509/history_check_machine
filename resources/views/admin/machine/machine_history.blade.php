@extends("admin.master")
@section('css')
    <style>
        #panel_table {
            padding: 0px;
            border-color: #5CB85C;
            border-style: groove;
        }
        .content{
            margin: 0px;
        }
       
        #tb_machine{
            width: 100%!important;
        }
        .panel-heading{
            background-color: #5CB85C!important;
            color: white!important;
        }
        .modal-content{
            border-radius: 6px;
        }
        .modal-header{
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;

        }
        #modal_delete .modal-header{
            background-color: #D9534F;
        }
        #modal_edit .modal-header{
            background-color: #428BCA;
        }
        .modal-header .modal-title{
            text-align: center;
            color: white;
        }
        thead th{
            text-align: center;
        }
        #edit_logo{
            opacity: 1.0;
            height: 133px;
            width: 300px;
        }
        #edit_logo:hover {

            opacity: 0.5;
            filter: alpha(opacity=50); /* For IE8 and earlier */
        }
        #edit_advanced{
            text-align: left!important;
        }
    </style>
@stop
@section('content')
   
   <div class="container">
       <div class="row">
           <div class="col-md-11">
               
               @if ($machine_info)
           <h2>Lịch Sử Chỉnh Sửa Máy: {{$machine_info->name}}-{{$machine_info->model}}-{{$machine_info->seri}}-{{$machine_info->nsx}}</h2>
           @else
               <h2>Không tìm thấy thông tin máy</h2>
           @endif
           <!--content-->
           @if(count($logs)>0)
               @foreach ($logs as $log)
                           <div class="table-responsive">
           <table class="table table-bordered table-dark" style="background-color: #93e4f0; margin-bottom: 30px">
               
               <thead>
                   <th style="width: 15%" >Thời gian: {{$log->created_at}}</th>
                   <th style="width: 35%">Trước</th>
                   <th style="width: 35%">Sau</th>
                   <th style="width: 15%">User thay đổi</th>
               </thead>
               <tbody>
               @if ($log->name_log !='')
               <tr>
                   <td >Tên</td>
                   <td >{{$log->before_name}}</td>
                   <td >{{$log->after_name}}</td>
                   <td >{{$log->user_id}}</td>
               </tr>
               @endif
               @if ($log->model_log !='')
                   <tr>
                       <td >Model</td>
                       <td >{{$log->before_model}}</td>
                       <td >{{$log->after_model}}</td>
                       <td >{{$log->user_id}}</td>
                   </tr>
               @endif
               @if ($log->seri_log !='')
                   <tr>
                       <td >Model</td>
                       <td >{{$log->before_seri}}</td>
                       <td >{{$log->after_seri}}</td>
                       <td >{{$log->user_id}}</td>
                   </tr>
               @endif
               @if ($log->nsx_log !='')
                   <tr>
                       <td >Nhà Sản Xuất</td>
                       <td >{{$log->before_nsx}}</td>
                       <td >{{$log->after_nsx}}</td>
                       <td >{{$log->user_id}}</td>
                   </tr>
               @endif
               @if ($log->content_log !='')
                   <tr>
                       <td >Lịch Sử Kiểm Kiểm Tra</td>
                       <td >{!! $log->before_content !!}</td>
                       <td >{!! $log->after_content !!}</td>
                       <td >{{$log->user_id}}</td>
                   </tr>
               @endif
               </tbody>
           </table>
                           </div>
               @endforeach
           @else
           <h3>Không có lịch sử chỉnh sửa</h3>
               @endif
       </div>
       </div>
   </div>
@stop
