@extends("admin.master")
@section('css')
    <style>
        #panel_table {
            padding: 0px;
            border-color: #5CB85C;
            border-style: groove;
        }
        .content{
            margin: 0px;
        }
        #tb_machine{
            width: 100%!important;
        }
        .panel-heading{
            background-color: #5CB85C!important;
            color: white!important;
        }
        .modal-content{
            border-radius: 6px;
        }
        .modal-header{
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;

        }
        #modal_delete .modal-header{
            background-color: #D9534F;
        }
        #modal_edit .modal-header{
            background-color: #428BCA;
        }
        .modal-header .modal-title{
            text-align: center;
            color: white;
        }
        thead th{
            text-align: center;
        }
        #edit_logo{
            opacity: 1.0;
            height: 133px;
            width: 300px;
        }
        #edit_logo:hover {

            opacity: 0.5;
            filter: alpha(opacity=50); /* For IE8 and earlier */
        }
        #edit_advanced{
            text-align: left!important;
        }
    </style>
@stop
@section('machine','active')
@section('machine-list','active')
@section('content')
   
   <div class="container">
       <div class="row">
           <!--content-->
           <div id="panel_table" class="panel panel-info col-md-9 col-md-offset-1">
               <div class="panel-heading text-center">Danh Sách Máy</div>
               <div class="panel-body">
                   <button class="btn btn-primary" style="margin: 5px" id="see_all">Xóa Lọc</button>
                   <table id="tb_machine" class="table table-bordered table-hover">
                       <thead>
                       <th>ID</th>
                       <th>Tên Máy </th>
                       <th>Tên Công Ty </th>
                       <th>Model </th>
                       <th>Seri</th>
                       <th>Hãng Sản Xuất</th>
                       {{--<th>Ngày tạo</th>--}}
                       @if($staff_info->level == 'admin')
                       <th>Chỉnh sửa</th>
                       @else
                           <th>Chi tiết</th>
                           @endif
                       @if($staff_info->level == 'admin')
                       <th>Xóa</th>
                       @endif
                       </thead>
                   </table>
               </div>
           </div> <!--End content-->
           <!--modal delete-->
           <div id="modal_delete" class="modal fade" role="dialog">
               <div class="modal-dialog">
                   <!-- Modal content-->
                   <div class="modal-content">
                       <div class="modal-header">
                           <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Xác thực tác vụ</h4>
                       </div>
                       <div class="modal-body text-center">

                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-danger" id="btn_remove">Xóa</button>
                           <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                       </div>
                   </div>

               </div>
           </div>  <!--End modal delete-->
           <!--modal edit-->
           <div id="modal_edit" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg">
                   <!-- Modal content-->
                   <div class="modal-content">
                       <div class="modal-header">
                           <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Chi Tiết Máy</h4>
                       </div>
                      
                       <div class="modal-body">
                           @if ($staff_info->email == 'kythuat-01@khoitoan.com.vn' || $staff_info->email == 'vovantrungspkt@gmail.com')
                           <a type="button" target="_blank" class="btn btn-primary" id="btn_history"  href="#">Lịch sử chỉnh sửa</a>
                           @endif
                           <div class="row">
                               
                               <input type="hidden" name="_token" value="{{csrf_token()}}">
                               <div class="form-group col-md-5">
                                   <label>Tên Máy</label>
                                   <input type="text" class="form-control" id="edit_name" name="edit_name" placeholder="vd: Cân điện tử">
                               </div>
                               <div class="form-group col-md-7">
                                   <label>Tên Công ty</label>
                                   <select class="form-control" id="edit_company_id" name="edit_company_id">
                                       <option disabled selected value> -- Chọn Công Ty -- </option>
                                       @foreach ($list as $value)
                                           <option value="{{$value->id}}">{{$value->name}}</option>
                                       @endforeach
                                   </select>
                               </div>
                           </div>
                           <div class="row">
                               <div class="form-group col-md-4">
                                   <label>Model</label>
                                   <input type="text" class="form-control" id="edit_model" name="edit_model"
                                          placeholder="vd: BDIXXX">
                               </div>
                               <div class="form-group col-md-4">
                                   <label>Seri</label>
                                   <input type="text" class="form-control" id="edit_seri" name="edit_seri" placeholder="vd: CAS">
                               </div>
                               <div class="form-group col-md-4">
                                   <label>Hãng sản xuất</label>
                                   <input type="text" class="form-control" id="edit_nsx" name="edit_nsx" placeholder="vd: CAS">
                               </div>
                           </div>
                           <div class="row">
                               <div class="form-group col-md-12">
                                   <label>Lịch sử</label>
                                   <textarea class="form-control" id="edt_intro" name="edt_intro"
                                             placeholder="Bước 1: ...."></textarea>
                               </div>
                           </div>
                           <div id="notification" class="alert alert-success hide">
                               <strong></strong>
                               <span></span>
                           </div>
                       </div>
                       <div class="modal-footer">
                           @if($staff_info->level == 'admin')
                               <button type="button" class="btn btn-primary"  id="btn_edit">Chỉnh sửa</button>
                           @endif
                               <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>


                       </div>
                   </div>

               </div>
           </div><!--End modal edit-->
       </div>
   </div>
@stop
@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            var id='';
            var name = '';
            var company_id =''
            var model = '';
            var seri = '';
            var nsx = '';
            var intro = '';
            if (level == 'admin') {
                CKEDITOR.replace('edt_intro', {
                    filebrowserImageBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Images',
                    filebrowserFlashBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Flash',
                    filebrowserImageUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                    filebrowserFlashUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                    filebrowserUploadUrl:       '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    filebrowserBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html',
                    extraPlugins : 'timestamp',

                });
            } else {

                CKEDITOR.replace('edt_intro', {
                    filebrowserImageBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Images',
                    filebrowserFlashBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Flash',
                    filebrowserImageUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                    filebrowserFlashUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                    extraPlugins: 'timestamp',

                });
            }

if (level == 'admin') {
    var table = $('#tb_machine').DataTable({
        scrollY: 300,
        scrollCollapse: true,
        "processing": true,
        "serverSide": true,
        "ajax": '{{route('controller/admin/machine-list')}}',
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Vietnamese.json"
        },
        "columns": [
            {data: 'id',name:'machine.id'},
            {data: 'name',name:'machine.name'},
            {data: 'company_name',name:'company.name'},
            {data: 'model',name:'machine.model'},
            {data: 'seri'},
            {data: 'nsx'},
            // {data: 'created_at', name:'machine.created_at'},
            {data: 'edit', orderable: false, "searchable": false},
            {data: 'delete', orderable: false, "searchable": false}
        ]
    });
}else {
    var table = $('#tb_machine').DataTable({
        scrollY: 300,
        scrollCollapse: true,
        "processing": true,
        "serverSide": true,
        "ajax": '{{route('controller/admin/machine-list')}}',
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Vietnamese.json"
        },
        "columns": [
            {data: 'id',name:'machine.id'},
            {data: 'name',name:'machine.name'},
            {data: 'company_name',name:'company.name'},
            {data: 'model',name:'machine.model'},
            {data: 'seri'},
            {data: 'nsx'},
            // {data: 'created_at'},
            {data: 'edit', orderable: false, "searchable": false},
           
        ]
    });
}
            var $_GET = {};

            document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
                function decode(s) {
                    return decodeURIComponent(s.split("+").join(" "));
                }

                $_GET[decode(arguments[1])] = decode(arguments[2]);
            });
            if ($_GET['id']) {
                table.columns(0).search($_GET['id']).draw();
            }
            $('#see_all').on('click',function () {
                table.columns(0).search('').draw();
                table.search('').draw();

            });
            $('#tb_machine').on('click','.icon_delete',function () {
                var fullname = $(table.row( $(this).parents('tr') ).data()['fullname']).text();
                var id = $(table.row( $(this).parents('tr') ).data()['id']).text();
                $('#modal_delete .modal-body').html(
                        "<p>Bạn muốn xóa</p>" +
                "<strong>"+fullname+"</strong>");
                $('#btn_remove').removeClass('hide')
                $('#modal_delete').modal('show');

                $('#btn_remove').on('click',function () {
                    $.ajax({
                        url: "{{route('controller/admin/machine-remove')}}",
                        data:{
                            'id': id
                        },
                        beforeSend: function () {
                            $('.modal-body').html('<img src="{{asset('public/img/loading.gif')}}">');
                        },
                        dataType: "text",
                        type: "get",
                        success: function (result) {
                            if(result == "Removed"){
                                table.row( $(this).parents('tr') ).remove().draw();
                                $('.modal-body').html(
                                        "<strong style='font-size: 1.5em' class='text-success'>Đã xóa <i class='glyphicon glyphicon-ok'></i></strong>");
                                $('#btn_remove').addClass('hide')
                            }else {
                                $('.modal-body').html("<strong>Lỗi kết nối, vui lòng thử lại sau!</strong>");
                                $('#modal_delete').modal('show');
                            }
                        }
                    });
                });
            });
            /*Edit Bank*/
            $('#tb_machine').on('click','.icon_edit',function () {
                CKEDITOR.instances['edt_intro'].setData('');
                $('#notification').addClass('hide');
                 id = $(table.row( $(this).parents('tr') ).data()['id']).text();
                $.ajax({
                    url: "{{route('controller/admin/machine-getInfo')}}",
                    data:{
                        'id': id
                    },
                    dataType: "json",
                    type: "get",
                    success: function (result) {
                         name = result['name'];
                         company_id = result['company_id'];
                         model = result['model'];
                         seri = result['seri'];
                         nsx = result['nsx'];
                         intro = result['content'];
                        $('#edit_name').val(name);
                        $('#edit_model').val(model);
                        $('#edit_seri').val(seri);
                        $('#edit_nsx').val(nsx);
                        $('#edit_company_id').val(company_id);
                        $('#edit_company_id').prop('disabled', 'disabled');
                        if(intro !=''){
                            CKEDITOR.instances['edt_intro'].setData(intro);
                        }
                        $('#btn_history').attr('href','history-machine?id='+id);
                        $('#modal_edit').modal('show');
                    }
                });

            });
            $('#btn_edit').on('click',function () {
                $('#notification').addClass('hide');
                var edit_name = $('#edit_name').val();
                var edit_model = $('#edit_model').val();
                var edit_seri = $('#edit_seri').val();
                var edit_nsx = $('#edit_nsx').val();
                var edit_intro =  CKEDITOR.instances['edt_intro'].getData();
                if((name == edit_name)&&(intro == edit_intro)&&(model == edit_model)&&(edit_seri == seri)&&(edit_nsx == nsx)){
                    $('#notification').removeClass('hide');
                    $('#notification').removeClass('alert-success');
                    $('#notification').addClass('alert-danger');
                    $('#notification strong').text('Error:')
                    $('#notification span').text('Dữ liệu không có gì thay đổi, vui lòng kiểm tra lại!');
                }else{
                    $.ajax({
                        url: "{{route('controller/admin/machine-edit')}}",
                        data:{
                            'id':id,
                            'name': edit_name,
                            'model': edit_model,
                            'seri': edit_seri,
                            'nsx': edit_nsx,
                            'intro': edit_intro
                        },
                        dataType: "json",
                        type: "post",
                        success: function (result) {
                            $("html,body").animate({scrollTop: 0}, 1000);
                            if(result['status'] == "Success"){
                                $('#notification').removeClass('hide');
                                $('#notification').removeClass('alert-danger');
                                $('#notification').addClass('alert-success');
                                $('#notification strong').text('Success:')
                               
                                $('#notification span').text('Dữ liệu đã được thay đổi!!');
                                $('#edit_name').val(name);
                                setTimeout(function () {
                                    $('#modal_edit').modal('hide');
                                    table.draw(true);
                                },2000);
                            }else {
                                $('#notification').removeClass('hide');
                                $('#notification').removeClass('alert-success');
                                $('#notification').addClass('alert-danger');
                                $('#notification strong').html('Error:')
                                $('#notification span').html('<ul>'+result['error']+'</ul>');
                            }
                        }
                    });
                }
            });
            $('#edit_advanced').on('click',function () {
                if($('#edit_advanced_form').hasClass('hide')){
                    $('#edit_advanced_form').removeClass('hide');

                }else {
                    $('#edit_advanced_form').addClass('hide');
                }
            });


        });

    </script>
@stop
