@extends("admin.master")
@section('content')

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{$count_consult_today}}</h3>

                    <p>Yêu cầu mới</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{$count_news_today}}</h3>

                    <p>Bài viết</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{$count_customer_today}}</h3>

                    <p>Khách hàng đăng ký</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{$visitor[7]->hits}}</h3>

                    <p>Lượt truy cập hôm nay</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Lượt truy cập</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="line-chart" style="height: 300px;"></div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var it1 = {{$visitor[0]->hits}}
        var line = new Morris.Line({
            parseTime: false,
            element: 'line-chart',
            resize: true,
            data: [
                    {y: '{{Carbon\Carbon::parse($visitor[0]->date)->format('d-m-Y')}}', item1: {{$visitor[0]->hits}}},
                    {y: '{{Carbon\Carbon::parse($visitor[1]->date)->format('d-m-Y')}}', item1: {{$visitor[1]->hits}}},
                    {y: '{{Carbon\Carbon::parse($visitor[2]->date)->format('d-m-Y')}}', item1: {{$visitor[2]->hits}}},
                    {y: '{{Carbon\Carbon::parse($visitor[3]->date)->format('d-m-Y')}}', item1: {{$visitor[3]->hits}}},
                    {y: '{{Carbon\Carbon::parse($visitor[4]->date)->format('d-m-Y')}}', item1: {{$visitor[4]->hits}}},
                    {y: '{{Carbon\Carbon::parse($visitor[5]->date)->format('d-m-Y')}}', item1: {{$visitor[5]->hits}}},
                    {y: '{{Carbon\Carbon::parse($visitor[6]->date)->format('d-m-Y')}}', item1: {{$visitor[6]->hits}}},
                    {y: '{{Carbon\Carbon::parse($visitor[7]->date)->format('d-m-Y')}}', item1: {{$visitor[7]->hits}}}
                ],
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Lượt truy cập'],
            lineColors: ['#3c8dbc'],
            hideHover: 'auto'
        });
    </script>
@endsection