<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.1.1.js"
            integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
            integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
    <style>
        textarea {
    resize: none;
}
    </style>
</head>
<body>

<div class='container'>
    <div class="row">
        <div class="8">
            <form class="form-horizontal" id="consult_form" data-toggle="validator"  method="post" action="controller/add-custommer">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <fieldset>
                    <!-- Form Name -->
                    <legend>Yêu Cầu</legend>
                    <!-- Text input-->

                    <div class="form-group">

                        <label class="col-md-4 control-label" for="txtEmail">Họ Tên</label>
                        <div class="col-md-3">
                            <input id="txtHoten" name="txtHoten" type="text" placeholder="Võ Văn Hiếu"
                                   class="form-control input-md" required="">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="radios">Giới tính</label>
                        <div class="col-md-4">
                            <label class="radio-inline" for="radios-0">
                                <input type="radio" name="gender" id="radios-0" value="1" checked="checked">Nam
                            </label>
                            <label class="radio-inline" for="radios-1">
                                <input type="radio" name="gender" id="radios-1" value="0">Nữ
                            </label>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="txtEmail">Email</label>
                        <div class="col-md-3">
                            <input id="txtEmail" name="txtEmail" type="email" placeholder="abc@gmail.com"
                                   class="form-control input-md" required="">
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="txtPhone">Số điện thoại</label>
                        <div class="col-md-3">
                            <input id="txtPhone" name="txtPhone" type="text" placeholder="Số Điện Thoại"
                                   class="form-control input-md" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="selectbasic">Chọn Tỉnh / Thành phố</label>
                        <div class="col-md-3">
                            <select id="txtProvince" name="txtProvince" class="form-control">
                                @foreach($provinces  as $a)
                                    <option value="{{$a->id}}">{{$a->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="txtAddress">Địa chỉ</label>
                        <div class="col-md-3">
                            <textarea class="form-control" id="txtAddress" name="txtAddress"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="selectbasic">Danh mục tư vấn</label>
                        <div class="col-md-3">
                            <select id="txtConsult" name="txtConsult" class="form-control">
                                @foreach($categoryconsults  as $a)
                                    <option value="{{$a->id}}">{{$a->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="txtAddress">Nội dung tư vấn</label>
                        <div class="col-md-3">
                            <textarea class="form-control" id="txtRequire" name="txtRequire" required=""></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="singlebutton"></label>
                        <div class="col-md-4">
                            <button type="submit" id="sendbtn" name="sendbtn" class="btn btn-primary">Gửi</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
<script type="text/javascript">
    function validateText(id)
    {
    if($("#"+id).val()==null || $("#"+id).val()=="" || $.trim($("#"+id).val())=="")
    {
    var div = $("#"+id).closest("div");
    div.removeClass("has-success");
    $("#glypcn"+id).remove();
    div.addClass("has-error has-feedback");
    div.append('<span id="glypcn'+id+'" class="glyphicon glyphicon-remove form-control-feedback"></span>');
    return false;
    }
    else{
    var div = $("#"+id).closest("div");
    div.removeClass("has-error");
    $("#glypcn"+id).remove();
    div.addClass("has-success has-feedback");
    div.append('<span id="glypcn'+id+'" class="glyphicon glyphicon-ok form-control-feedback"></span>');
    return true;
    }

    }
function validatePhone(id)
    {
    var phone_regex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
    if(!phone_regex.test($("#"+id).val()))
    {
    var div = $("#"+id).closest("div");
    div.removeClass("has-success");
    $("#glypcn"+id).remove();
    div.addClass("has-error has-feedback");
    div.append('<span id="glypcn'+id+'" class="glyphicon glyphicon-remove form-control-feedback"></span>');
    return false;
    }
    else{
    var div = $("#"+id).closest("div");
    div.removeClass("has-error");
    $("#glypcn"+id).remove();
    div.addClass("has-success has-feedback");
    div.append('<span id="glypcn'+id+'" class="glyphicon glyphicon-ok form-control-feedback"></span>');
    return true;
    }

    };
    function validateEmail(id)
    {
    var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    if(!email_regex.test($("#"+id).val()))
    {
    var div = $("#"+id).closest("div");
    div.removeClass("has-success");
    $("#glypcn"+id).remove();
    div.addClass("has-error has-feedback");
    div.append('<span id="glypcn'+id+'" class="glyphicon glyphicon-remove form-control-feedback"></span>');
    return false;
    }
    else{
    var div = $("#"+id).closest("div");
    div.removeClass("has-error");
    $("#glypcn"+id).remove();
    div.addClass("has-success has-feedback");
    div.append('<span id="glypcn'+id+'" class="glyphicon glyphicon-ok form-control-feedback"></span>');
    return true;
    }

    }

    $(document).ready(function () {

        $("#birthday").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: '1940:2010'
        });
       // $("#sendbtn").click(function()
//{
    $('#txtHoten').blur(function(){
       if(!validateText("txtHoten"))
    {
    return false;
    }
    })
$('#txtEmail').blur(function(){
    if(!validateEmail("txtEmail"))
    {
    return false;
    }
})
$('#txtPhone').blur(function(){
    if(!validatePhone("txtPhone"))
    {
    return false;
    }
    })
$('#txtAddress').blur(function(){
    if(!validateText("txtAddress"))
    {
    return false;
    }
})
$('#txtRequire').blur(function(){
    if(!validateText("txtRequire"))
    {
    return false;
    }
})


$("#sendbtn").click(function()
{

       if(!validateText("txtHoten"))
    {
    return false;
    }


    if(!validateEmail("txtEmail"))
    {
    return false;
    }


    if(!validatePhone("txtPhone"))
    {
    return false;
    }

    if(!validateText("txtAddress"))
    {
    return false;
    }
    if(!validateText("txtRequire"))
    {
    return false;
    }

    $("form#consult_form").submit();
});

    });
</script>
</body>
</html>