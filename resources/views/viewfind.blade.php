<!DOCTYPE html>
<html>
<head>
	<title>Xin chào</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
	<div class="row">
	@if (count($data))

	<h2>Các sản phẩm phù hợp</h2>
		<div class="col-md-8">
			<table class='table table-bordered'>
				<thead style="background-color: #e2e6f2">
					<tr>
						<th>Sản phẩm</th>
						<th>Lãi suất (%)</th>
						<th>Vay tối đa (Triệu)</th>
						<th>Thời hạn (Tháng)</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $a )
					<tr>
						<td>
							<a href="viewproduct/{{$a->id}}">{{$a->title}}</a>
						</td>
						<td>
							{{$a->laisuat}}
						</td>
						<td>
							{{$a->vaytoida}}
						</td>
						<td>
							{{$a->thoihan}}
						</td>
					</tr>
	
					@endforeach
				</tbody>
			</table>
		</div>
		@else 
		<h2>Không tìm thấy sản phầm nào phù hợp </h2>
		@endif
	</div>
</div>
</body>
</html>