<!doctype html>
<body >
<section class="container">
    <div class="row">
        <h2>ResetPassword</h2>
        <hr>
        <table>
            <tbody>
                <p>Xin chào!</p>
                <p>Chúng tôi vừa mới nhận được yêu cầu Reset Password từ tài khoản <strong>{{$email or ''}}</strong> của bạn</p>
                <p>Nếu bạn là người đã gửi yêu cầu này, vui lòng click vào nút phía dưới để lấy lại mật khẩu đã mất.</p>
                <p>Thân!</p>
                <p>Admin</p>
                <div style="text-align:center; ">
                    <a style="margin: 0 auto; padding: 10px; background-color: darkblue;text-decoration: none; color: white" href="{{$content or 'http://hahaha.com'}}">Reset Password</a>
                </div>​
            </tbody>
        </table>

    </div>
</section>

</body>
