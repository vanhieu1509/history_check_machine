<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />
   
    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset("lib/Canvas/style.css")}}" type="text/css" />
    <link rel="stylesheet" href="{{asset("lib/Canvas/css/swiper.css")}}" type="text/css" />
    <link rel="stylesheet" href="{{asset("lib/Canvas/css/dark.css")}}" type="text/css" />
    <link rel="stylesheet" href="{{asset("lib/Canvas/css/font-icons.css")}}" type="text/css" />
    <link rel="stylesheet" href="{{asset("lib/Canvas/css/animate.css")}}" type="text/css" />
    <link rel="stylesheet" href="{{asset("lib/Canvas/css/magnific-popup.css")}}" type="text/css" />

    <link rel="stylesheet" href="{{asset("lib/Canvas/css/responsive.css")}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
     @yield('meta')
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    @yield('css')
    <!-- Document Title
    ============================================= -->
    <title>@yield('title')</title>

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">

                    <a href="{{action("HomeController@getViewHome")}}" class="standard-logo" data-dark-logo="{{asset("lib/Canvas")}}/images/logo-dark.png"><img src="{{asset("lib/Canvas")}}/images/logo.png" alt="Canvas Logo"></a>
                    <a href="{{action("HomeController@getViewHome")}}" class="retina-logo" data-dark-logo="{{asset("lib/Canvas")}}/images/logo-dark.png"><img src="{{asset("lib/Canvas")}}/images/logo.png" alt="Canvas Logo"></a>

                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu" class="style-2">
                    <ul>
                        <li class="@yield('home_active')"><a href="{{action("HomeController@getViewHome")}}"><div>Trang chủ</div></a></li>
                        <li class="@yield('about_active')"><a href="{{route("about")}}"><div>Giới Thiệu</div></a></li>
                        <li class="@yield('news_active')"><a href="#tintuc"><div>Tin tức</div></a>
                            <ul>
                                <li class="@yield('national_news_active')"><a href="{{route("national-news")}}"><div><i class="icon-newspaper"></i>Tài chính trong nước</div></a></li>
                                <li class="@yield('world_news_active')"><a href="{{route("world-news")}}"><div><i class="icon-globe"></i>Tài chính Thế giới</div></a>                                </li>
                                <li class="@yield('promotion_news_active')"><a href="{{route("promotion-news")}}"><div><i class="icon-gift"></i>Tin khuyến mãi</div></a>                                </li>
                            </ul>
                        </li>
                        <li class="@yield('compare_active')" ><a href="#sosanh"><div>So sánh</div></a>
                            <ul>
                                <li class="@yield('laisuatchovay_compare_active')"><a href="{{route('laisuatvayvon')}}"><div><i class="icon-newspaper"></i>Lãi suất cho vay</div></a></li>
                                <li  class="@yield('laisuatguitietkiem_compare_active')"><a href="{{route('laisuatguitietkiem')}}"><div><i class="icon-globe"></i>Lãi suất gửi tiết kiệm</div></a>                                </li>
                            </ul>
                        </li>
                        <li  class="@yield('utilities_active')" ><a href="#"><div>Tiện ích</div></a>
                            <ul>
                                <li class="@yield('rate_utilities_active')"><a href="{{action('HomeController@getViewExchange')}}"><div><i class="icon-newspaper"></i>Tỷ giá hối đoái</div></a></li>
                                <li class="@yield('gold_utilities_active')"><a href="{{action('HomeController@getViewGold')}}"><div><i class="icon-globe"></i>Tỷ giá vàng</div></a>                                </li>
                                <li class="@yield('tool_utilities_active')"><a href="{{action('HomeController@getViewToolBorrow')}}"><div><i class="icon-globe"></i>Công cụ tính toán</div></a>                                </li>
                            </ul>
                        </li>
                        <li class="@yield('faq_active')"><a href="{{route("guest/faq")}}"><div>Câu hỏi thường gặp</div></a></li>
                    </ul>
                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header><!-- #header end -->

    @yield('banner')
    <!-- Content
    ============================================= -->
   @yield('page_title')
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">
                @yield('content')
            </div>
        </div>
    </section><!-- #content end -->
    @yield('semi-footer')
    <!-- Footer
    ============================================= -->
    <!-- Footer
		============================================= -->
    <footer id="footer" class="dark">

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

            <div class="container clearfix">

                <div class="col_half">
                    <img src="{{asset("lib/Canvas/")}}/images/footer-logo.png" alt="Footer Logo" class="footer-logo">
                </div>

                <div class="col_half col_last tright">
                    <div class="copyrights-menu copyright-links fright clearfix">
                        <a href="{{action("HomeController@getViewHome")}}">Trang chủ</a>/<a href="{{route("about")}}">Thông tin</a>/<a href="{{route("guest/faq")}}">FAQs</a>
                    </div>
                    <div class="fright clearfix">
                        <a href="#" class="social-icon si-small si-borderless nobottommargin si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless nobottommargin si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless nobottommargin si-gplus">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless nobottommargin si-pinterest">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless nobottommargin si-vimeo">
                            <i class="icon-vimeo"></i>
                            <i class="icon-vimeo"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless nobottommargin si-github">
                            <i class="icon-github"></i>
                            <i class="icon-github"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless nobottommargin si-yahoo">
                            <i class="icon-yahoo"></i>
                            <i class="icon-yahoo"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless nobottommargin si-linkedin">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                    </div>
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="{{asset("lib/Canvas/js/jquery.js")}}"></script>
<script type="text/javascript" src="{{asset("lib/Canvas/js/plugins.js")}}"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="{{asset("lib/Canvas/js/functions.js")}}"></script>
@yield('javascript')
</body>
</html>