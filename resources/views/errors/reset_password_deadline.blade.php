<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Forgot Passwod</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset("lib//dist/css/AdminLTE.min.css")}}">
    <link rel="stylesheet" href="{{asset("public/css/reset_password_error.css")}}">
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.0.min.js"></script>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style rel="stylesheet">
        body{
            background: url("{{asset("public/img/login_background.jpg")}}") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        .row>div{
            margin-top: 70px!important;
            max-width: 600px;
            margin: 0 auto;
        }
        .panel-heading{
            background-color: #E8075A;
        }
        .panel-heading>h5{
            color: white;
        }
        .panel-body{
            background-color: white;
        }
    </style>
</head>
<body>
<!-- Static navbar -->
<div class="container">
    <div class="row ">
        <div>
            <div class="panel-heading">
                <h5 class="text-center" >
                    <i class="fa fa-times fa-times-circle fa-3x "></i>
                </h5>

            </div>
            <div class="panel-body">
                <h4 class="text-danger">XÁC NHẬN ĐƯỜNG DẪN</h4>
                <h5 class="text-danger">Bạn có thể đang gặp một trong số các lỗi sau: </h5>
                <ul>
                    <div class="container">
                        <li class=""><i>Link xác nhận không đúng!</i></li>
                        <li class=" "><i>Link đã hết hạn.</i></li>
                        <li class=" "><i>Link đã được xác nhận rồi.</i></li>
                    </div>

                </ul>
            </div>
            <div class="panel-footer text-center">
                <p class="">Bạn hãy kiểm tra lại đường link mà chúng tôi đã cung cấp trong email!</p>
                <a href="login" class="btn btn-info">Back to Login Page</a>
            </div>
        </div>
    </div>
</div> <!-- /container -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('lib/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
