<!DOCTYPE html>
<html>
<head>
	<title>Hiển thị sản phẩm</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
@foreach($products as $a)
<div class="row">
    <div class="col-md-12 text-center">
        <h2>{{$a->title}}</h2>
    </div>
    <div class="col-md-12">{{$a->tomtat}}</div>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Lãi Suất (%)</th>
                    <th>Vay Tối Đa (Triệu)</th>
                    <th>Thời Hạn Vay (Tháng)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$a->laisuat}}</td>
                    <td>{{$a->vaytoida}}</td>
                    <td>{{$a->thoihan}}</td>
                </tr>
                <tr>

            </tbody>
        </table>
    </div>
</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
		{!!$a->chitiet!!}
</div>
	</div>
	@endforeach
</div>
</body>
</html>