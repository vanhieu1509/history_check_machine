@extends('admin.master')
@section('content')
    <div class="container">
        <div class="row">
            <!--content-->
            <div id="panel_table" style="padding: 0px;" class="panel panel-info col-md-9 col-md-offset-1">
                <div class="panel-heading text-center">Danh Sách Ngân Hàng</div>
                <div class="panel-body" >

                    <table id="tb_banks" class="table table-bordered table-hover">
                        <thead >
                            <th class="text-center"></th>
                            <th class="text-center">Tên Ngân Hàng </th>
                            <th class="text-center">3 Tháng (%)</th>
                            <th class="text-center">6 Tháng (%)</th>
                            <th class="text-center">12 Tháng (%)</th>
                        </thead>
                        <tbody>
                            <tr class="text-center">
                                <td class="text-center"><img src="{{asset("public/img/icon_gold.png")}}" style="width: 50px; height: 50px;"></td>
                                <td>Ngân Hàng Đông Á</td>
                                <td>0.5</td>
                                <td>0.7</td>
                                <td>0.6</td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-center"><img src="{{asset("public/img/icon_sliver.png")}}" style="width: 50px; height: 50px;"></td>
                                <td>Ngân Hàng Nam Á</td>
                                <td>0.4</td>
                                <td>0.5</td>
                                <td>0.6</td>
                            </tr>
                            <tr class="text-center">
                                <td class="text-center"><img src="{{asset("public/img/cu.png")}}" style="width: 50px; height: 50px;"></td>
                                <td>Ngân Hàng Á Châu</td>
                                <td>0.35</td>
                                <td>0.5</td>
                                <td>0.4</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> <!--End content-->
        </div>
    </div>

@stop