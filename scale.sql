-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 19, 2018 lúc 11:06 AM
-- Phiên bản máy phục vụ: 10.1.25-MariaDB
-- Phiên bản PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `scale`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `company`
--

INSERT INTO `company` (`id`, `name`, `about`, `updated_at`, `created_at`, `disable`) VALUES
(1, 'Công ty Thái Bình - KV2', '<p><strong>sdfsdfsddfsdfsd</strong></p>\n', '2018-11-19 09:23:12', '0000-00-00 00:00:00', 0),
(2, 'CÔNG TY HUNG WAY - Q7', '<p>C&ocirc;ng ty tốt</p>\n', '2018-11-19 09:33:47', '0000-00-00 00:00:00', 0),
(3, 'Công ty lampart', '<p>fsfsdfds</p>\n', '2018-11-19 09:34:26', '2018-11-15 12:12:08', 0),
(4, 'công ty Mình', '<p>sfsdfsd</p>\n', '2018-11-15 12:14:15', '2018-11-15 12:13:59', 1),
(5, 'Công Ty SAMSUNG VN', '', '2018-11-19 08:35:58', '2018-11-19 08:35:58', 0),
(6, 'Công Ty Sony VN', '<p>Hello V&otilde; Văn Hiếu</p>\n', '2018-11-19 09:26:21', '2018-11-19 09:26:21', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `machine`
--

CREATE TABLE IF NOT EXISTS `machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nsx` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disable` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `machine`
--

INSERT INTO `machine` (`id`, `company_id`, `name`, `model`, `seri`, `nsx`, `content`, `updated_at`, `created_at`, `disable`) VALUES
(1, 2, 'Discoloration tester', 'GT-7035UCC', '8901658', 'Gotech', '<p><strong>23/11/1994</strong> : Kiểm tra m&aacute;y chạy c&oacute; m&ugrave;i kh&eacute;t =&gt; Đ&atilde; sửa - Trung fsfsd</p>\n', '2018-11-14 09:01:03', '2018-11-14 09:01:03', 0),
(2, 1, 'Tensile', '2515', '1014', 'MESDAN', '<p><strong>24/05/2018</strong> : M&aacute;y kh&ocirc;ng kết nối m&aacute;y t&iacute;nh - Trung</p>\n\n<p>OK</p>\n', '2018-11-14 09:01:03', '2018-11-14 09:01:03', 0),
(3, 0, 'Cân điện tử', '', '', '', '<p>M&aacute;y a</p>\n', '2018-11-19 05:01:41', '2018-11-19 05:01:41', 0),
(4, 5, 'Cân Điện Tử', 'AXXX', '6543', 'CAS', '<p>OK</p>\n', '2018-11-19 08:43:45', '2018-11-19 08:43:45', 0),
(5, 5, 'Cân Điện Tử', 'YHT4', '6544', 'CAS', '<p>Hiệu chuẩn OK</p>\n', '2018-11-19 09:20:49', '2018-11-19 09:20:49', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
