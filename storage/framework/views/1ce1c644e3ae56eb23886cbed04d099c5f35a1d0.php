<?php $__env->startSection('css'); ?>
    <style>
        thead th{
            text-align: center;
        }
        #panel_table {
            padding: 0px;
            border-color: #5CB85C;
            border-style: groove;
        }
        .content{
            margin: 0px;
        }
        #tb_banks{
            width: 100%!important;
        }
        .panel-heading{
            background-color: #5CB85C!important;
            color: white!important;
        }
        .modal-content{
            border-radius: 6px;
        }
        .modal-header{
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;

        }
        #modal_delete .modal-header{
            background-color: #D9534F;
        }
        #modal_edit .modal-header{
            background-color: #428BCA;
        }
        .modal-header .modal-title{
            text-align: center;
            color: white;
        }
        #edit_logo{
            opacity: 1.0;
            height: 133px;
            width: 300px;
        }
        #edit_logo:hover {

            opacity: 0.5;
            filter: alpha(opacity=50); /* For IE8 and earlier */
        }
        #edit_advanced{
            text-align: left!important;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('news','active'); ?>
<?php $__env->startSection('news-list','active'); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <!--content-->
            <div id="panel_table" class="panel panel-info col-md-9 col-md-offset-1">
                <div class="panel-heading text-center">Danh Sách Tin Tức</div>
                <div class="panel-body">
                    <input type="hidden" name="_token" id="token" value="<?php echo e(csrf_token()); ?>">
                    <table id="tb_news" class="table table-bordered table-hover">
                        <thead>
                        <th>ID</th>
                        <th>Loại Tin Tức</th>
                        <th>Tiêu đề</th>
                        <th>Nhân Viên Đăng</th>
                        <th>Chỉnh Sửa</th>
                        <th>Xóa</th>
                        </thead>
                    </table>
                </div>
            </div> <!--End content-->
            <!--modal delete-->
            <div id="modal_delete" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Xác thực tác vụ</h4>
                        </div>
                        <div class="modal-body text-center">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" id="btn_remove">Xóa</button>
                            <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>

                </div>
            </div>  <!--End modal delete-->
            <!--modal edit-->
            <div id="modal_edit" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thay đổi thông tin</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <form>
                                    <div id="notification" class="alert alert-success hide">
                                        <strong></strong>
                                        <span></span>
                                    </div>

                                    <div class="form-group  form_content">
                                        <div class="form-group col-md-8">
                                            <label for="usr">Tiêu đề:</label>
                                            <input type="text" class="form-control" name="title" id="edit_title">
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="usr">Loại tin tức:</label>
                                            <select class="form-control" id="slt_category_news" name="category_news_id" title="Lựa chọn loại tin">
                                                <?php $__currentLoopData = $category_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <label for="usr">Nội dung tóm tắt của tin:</label>
                                            <textarea style=" resize:vertical; max-height: 150px!important;" type="text" name="sumary" class="form-control" id="edit_sumary"></textarea>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="usr">Nhân viên đăng tin</label>
                                            <input type="text" class="form-control" data="" name="staff_id" id="edit_staff_name" value="" readonly>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="usr">Đường dẫn tới trang tin: </label>
                                            <input type="text" class="form-control" data="" name="link" id="edit_link" value="" readonly>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="usr">Nguồn cung cấp tin: </label>
                                            <input type="text" class="form-control" data="" name="source" id="edit_source" value="">
                                        </div>
                                    </div>
                                    <div class="form-group  form_content">
                                        <div class="form-group col-md-12" >
                                            <label>Nội dung tin:</label>
                                            <textarea class="form-control" id="edt_content" name="content"
                                                      placeholder="www.abbank.com"></textarea>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary"  id="btn_edit">Chỉnh sửa</button>
                            <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>


                        </div>
                    </div>

                </div>
            </div><!--End modal edit-->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var news_id = '';

            CKEDITOR.replace('content', {
                filebrowserImageBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Images',
                filebrowserFlashBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Flash',
                filebrowserImageUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

            });
            var table = $('#tb_news').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '<?php echo e(route('controller/admin/news-list')); ?>',
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Vietnamese.json"
                },
                "columns": [
                    {data: 'id',name: 't1.id'},
                    {data: 'type',name: 't2.name'},
                    {data: 'title'},
                    {data: 'staff_name',name:'t3.fullname'},
                    {data: 'edit', orderable:false, "searchable": false },
                    {data: 'delete', orderable:false, "searchable": false  }
                ]
            });
            //Click button Delete
            $('#tb_news').on('click', '.icon_delete', function () {
                var title = $(table.row($(this).parents('tr')).data()['title']).text();
                var link = $(table.row($(this).parents('tr')).data()['id']).attr('data');
                var id = $(table.row($(this).parents('tr')).data()['id']).text();
                var token = $('#token').val();
                $('#modal_delete .modal-body').html(
                        "<p>Bạn muốn xóa tin: <strong>" + title + "</strong></p>" +
                        "<span> ở đường dẫn: <a href=" + link + "><strong>" + link + "</strong></a></span>");
                $('#btn_remove').removeClass('hide')
                $('#modal_delete').modal('show');

                $('#btn_remove').on('click', function () {
                    $.ajax({
                        url: "<?php echo e(route('controller/admin/news-remove')); ?>",
                        data: {
                            'id': id,
                            '_token': token
                        },
                        beforeSend: function () {
                            $('.modal-body').html('<img src="<?php echo e(asset('public/img/loading.gif')); ?>">');
                        },
                        dataType: "text",
                        type: "post",
                        success: function (result) {
                            if (result == "Removed") {
                                table.row($(this).parents('tr')).remove().draw();
                                $('.modal-body').html(
                                        "<strong style='font-size: 1.5em' class='text-success'>Đã xóa <i class='glyphicon glyphicon-ok'></i></strong>");
                                $('#btn_remove').addClass('hide')
                            } else {
                                $('.modal-body').html("<strong>Lỗi kết nối, vui lòng thử lại sau!</strong>");
                                $('#modal_delete').modal('show');
                            }
                        }
                    });
                });
            });
            $('#tb_news').on('click', '.icon_edit', function () {
                $('#notification').addClass('hide');
                var id = $(table.row($(this).parents('tr')).data()['id']).text();
                news_id = id;
                var token = $('#token').val();
                $.ajax({
                    url: "<?php echo e(route('controller/admin/news-getInfo')); ?>",
                    data: {
                        'id': id,
                        '_token': token
                    },
                    dataType: "json",
                    type: "post",
                    success: function (result) {
                        var category_id = result['category_news_id'];
                        var title = result['title'];
                        var slug = result['slug'];
                        var sumary = result['sumary'];
                        var content = result['content'];
                        var image = result['image'];
                        var source = result['source'];
                        var staff_id = result['staff_id'];
                        var name = result['name'];
                        var link = result['link'];
                        var fullname = result['fullname'];

                        $('#edit_title').val(title);
                        $('#slt_category_news').val(category_id);
                        $('#edit_sumary').val(sumary);
                        $('#edit_staff_name').val(fullname);
                        $('#edit_link').val(link);
                        $('#edit_source').val(source);
                        if (content != '') {
                            CKEDITOR.instances['edt_content'].setData(content);
                        }
                        $('#modal_edit').modal('show');
                    }
                });
            });
            $('#btn_edit').on('click', function () {
                var token = $('#token').val();
                var title = $('#edit_title').val();
                var category_news_id = $('#slt_category_news').val();
                var sumary = $('#edit_sumary').val();
                var source = $('#edit_source').val();
                var content = CKEDITOR.instances['edt_content'].getData();
                $.ajax({
                    url: "<?php echo e(route('controller/admin/news-edit')); ?>",
                    data: {
                        'id': news_id,
                        'title': title,
                        'category_news_id': category_news_id,
                        'sumary': sumary,
                        'edt_content': content,
                        'source': source,
                        '_token': token
                    },
                    dataType: "json",
                    type: "post",
                    success: function (result) {
                        console.log(result['data']);
                        if (result['status'] == 1) {
                            $('#notification').removeClass('hide');
                            $('#notification').removeClass('alert-danger');
                            $('#notification').addClass('alert-success');
                            $('#notification strong').text('Success:')
                            $('#notification span').text('Dữ liệu đã được thay đổi!!');
                            $('#edit_link').val(edit_link);
                            setTimeout(function () {
                                $('#modal_edit').modal('hide');
                                table.draw(true);
                            }, 2000);
                        } else {
                            $('#notification').removeClass('hide');
                            $('#notification').removeClass('alert-success');
                            $('#notification').addClass('alert-danger');
                            $('#notification strong').html('Error:');
                            $('#notification span').html('<ul>' + result['message'] + '</ul>');
                        }
                    }
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>