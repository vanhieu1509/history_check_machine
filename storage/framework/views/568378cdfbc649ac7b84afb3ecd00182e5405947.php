<?php $__env->startSection('css'); ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css" rel="stylesheet">
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
<!-- polyfiller file to detect and load polyfills -->
<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
<script>
  webshims.setOptions('waitReady', false);
  webshims.setOptions('forms-ext', {types: 'date'});
  webshims.polyfill('forms forms-ext');
</script>
<style>

  #panel_table {
    padding: 0px;
    border-color: #5CB85C;
    border-style: groove;
  }
  .content{
    margin: 0px;
  }
  .panel-heading{
    background-color: #5CB85C!important;
    color: white!important;
  }
  .modal-content{
    border-radius: 6px;
  }
  .modal-header{
      color: white;
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
  }
  #modal_delete .modal-header{
    background-color: #D9534F;
  }
  #modal_detail .modal-header{
    background-color: #428BCA;
  }
 
  thead th{
    text-align: center;
  }
  td{
    text-align:center;
    vertical-align:middle;
  }
  /*CSS cho button selector-bootstrap*/
  .bootstrap-select >button {
      background-color: white;!important;
      font-size: 16px;
  }
  /*Khi đưa con trỏ vào button selector thì sẽ như nào*/
  .bootstrap-select >button:hover{
      background-color: whitesmoke;

  }
  /*Khi click vào button selector thì sẽ như nào*/
  .open>.dropdown-toggle.btn-default{
      background: whitesmoke!important;

  }
  /*Text cho option*/
  .text {
      font-size: 16px;
  }
  /*Biểu tượng selected bên option*/
  .check-mark{
      color: green;
  }
  #file{
      font-size: 14px;
  }


</style>
<link rel="stylesheet" href="<?php echo e(asset("lib/AdminLTE/plugins/iCheck/square/blue.css")); ?>">
<link rel="stylesheet" href="<?php echo e(asset("lib/AdminLTE/plugins/iCheck/all.css")); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('consults','active'); ?>
<?php $__env->startSection('list_request','active'); ?>
<?php $__env->startSection('content_header'); ?>

    <div class="col-md-4 col-xs-6" style="padding:0px; margin-bottom: 2px">
        <select class="form-control  selectpicker" multiple name="slt_status" id="slt_status" title="Lựa chọn trạng thái yêu cầu">
            <option value="0">Chưa tư vấn</option>
            <option value="1">Đang tư vấn</option>
            <option value="2">Đã tư vấn</option>
        </select>

    </div >
    <div >
        <button style="margin-left: 10px" class="btn btn-primary" id="btn_show">Hiển thị</button>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="container" >
        <div class="row">
            <!--content-->
            <div id="panel_table" class="panel panel-info col-md-11" style="display: block" >
                <div class="panel-heading text-center">Danh Sách Yêu Cầu</div>
                <div class="panel-body" >
                    <div class="table-responsive col-sm-11 col-md-12">
                        <table id="tb_consult" class="table table-bordered table-hover">
                            <thead>
                            <th style=" align-items: center!important; height: 100%">
                                <div class="text-center" style="margin-left: 20px;">
                                    <input  type="checkbox" id="cb_all" class="cb_all hide text-center" title="">
                                </div>
                            </th>
                            <th>Tên Khách Hàng</th>
                            <th>Số Điện Thoại</th>
                            <th>Danh Mục Tư Vấn</th>
                            <th>Nội Dung</th>
                            <th>Tình Trạng</th>
                            <th>Chi tiết</th>
                            </thead>
                        </table>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-2 ">
                            <button type="button" name="btn_delete" id="btn_delete" class=" form-control btn btn-danger">Xóa</button>
                        </div>
                    </div>
                </div>
            </div> <!--End content-->
            <!-- modal_delete-->
            <div id="modal_delete" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Xác thực tác vụ</h4>
                        </div>
                        <div class="modal-body text-center">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger"  id="btn_remove">Xóa</button>
                            <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>

                </div>
            </div>  <!--End modal delete-->
            <!--Start modal edit-detail-->
            <div id="modal_detail" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Chi tiết yêu cầu</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <form id="form_edit">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <input type="hidden" id="consult_id" name="consult_id" value="">
                                    <input type="hidden" id="customer_id" name="customer_id" value="">
                                    <div class="form-group col-md-12">
                                        <div>
                                            <label>Nội dung</label>
                                            <textarea  name="consult_content" class="form-control" value="" id="consult_content" style="height: 100px; max-width: 100%"></textarea>
                                        </div>
                                    </div>
                                     <div class="form-group col-md-12">
                                        <div>
                                            <label>Ghi chú</label>
                                            <textarea  name="consult_note" class="form-control" value="" id="consult_note" style="height: 100px; max-width: 100%"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div >
                                            <label>Trạng thái yêu cầu</label>
                                            <select class="form-control selectpicker"  id="slt_status" name="slt_status" title="Lựa chọn trạng thái của yêu cầu">
                                                <option value="0">Chưa tư vấn</option>
                                                <option value="1">Đang tư vấn</option>
                                                <option value="2">Đã tư vấn</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div>
                                            <label>Lĩnh vực tư vấn</label>
                                            <select class="form-control selectpicker" id="slt_category_consult"  name="slt_category_consult" title="Lựa chọn lĩnh vực yêu cầu">
                                                <?php $__currentLoopData = $category_consult_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_consult): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <option value="<?php echo e($category_consult->id); ?>"><?php echo e($category_consult->name); ?> </option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div>
                                            <label>Nhân Viên Tư Vấn</label>
                                            <select class="form-control selectpicker"  id="slt_staff" name="slt_staff" data-live-search="true" title="Lựa chọn Nhân Viên tư vấn">
                                                <option value="0" disabled>Lựa chọn 1 nhân viên</option>
                                                <?php $__currentLoopData = $staff_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <option value="<?php echo e($staff->id); ?>"><?php echo e($staff->fullname); ?> - <?php if($staff->level == 'st_consult'): ?> Tư Vấn Viên <?php elseif($staff->level == 'st_news'): ?> Tin Tức Viên <?php elseif($staff->level == 'admin'): ?> Admin  <?php endif; ?> </option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div>
                                            <label>Tên Khách Hàng</label>
                                            <input type="text" class="form-control" name="customer_fullname" value="" id="customer_fullname">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div>
                                            <label>Số Điện Thoại</label>
                                            <input type="text" class="form-control" name="customer_number_phone" value="" id="customer_number_phone">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <div>
                                            <label>Ngày sinh</label>
                                            <input type="text" name="customer_birthday" id="customer_birthday" class="text-input datepicker form-control"  placeholder="DD.MMM.YYYY"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div>
                                            <label>Địa chỉ</label>
                                            <input type="text" class="form-control" name="customer_address" value="" id="customer_address">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn_edit_modal">Chỉnh sửa</button>
                            <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>

                </div>
            </div><!--End modal edit-->
        </div>
       
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset("lib/AdminLTE/plugins/iCheck/icheck.min.js")); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        //UI CheckBox with iCheck
        $('input[type="checkbox"].cb_all').iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        });
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
        });
        $(document).ready(function () {


            //SelectorPicker for Category-Consult
            $('.selectpicker').selectpicker({
                style: 'btn-default',
                size: 4
            });


            //Datatables
            var tb_consult =  $('#tb_consult');
            var table = tb_consult.DataTable({
                "destroy": true,
                "order": [[ 1, "asc" ]],
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "<?php echo e(route('controller/admin/consult-list')); ?>",
                    "data": function ( d ) {
                        d.slt_status = $('#slt_status').val();
                    }
                },
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Vietnamese.json"
                },
                "drawCallback": function() {
                    $('input[type="checkbox"].cb_select').iCheck({
                        checkboxClass: 'icheckbox_minimal-blue'
                    });
                    $('#cb_all').iCheck('uncheck');
                },
                "columnDefs": [

                    { "width": "20%", "targets": 4 }
                ],
                "columns": [
                    {data: 'cb_select', orderable:false, "searchable": false},
                    {data: 'fullname',  "searchable": false},
                    {data: 'number_phone', "searchable": false},
                    {data: 'name',"searchable": false},
                    {data: 'content', orderable:false, "searchable": false},
                    {data: 'status', orderable:false, searchable: false},
                    {data: 'detail', orderable:false, "searchable": false},
                ]});


            //Event CheckBox Checked and UnChecked
            var cb_all =  $('#cb_all');
            cb_all.on('ifChecked', function(){
                $('.cb_select').iCheck('check');
            });
            cb_all.on('ifUnchecked', function(){
                $('.cb_select').iCheck('uncheck');
            });
            tb_consult.on('ifChecked','.cb_select', function(){
                $(this).parents('tr') .addClass('slted')
            });
            tb_consult.on('ifUnchecked','.cb_select', function(){
                $(this).parents('tr') .removeClass('slted')
            });
            $('#btn_show').on('click',function () {
                table.ajax.reload();
            });
            //Event when click button Delete
            $('#btn_delete').on('click',function () {
                var arr_customer_id = [];
                var temp_customer_id ='';
                var $html = '';
                if(table.rows('.slted').data().length >0){
                    for (var j = 0; j <table.rows('.slted').data().length ; j++) {
                        $html = $html +'<li>'+  $(table.rows('.slted').data()[j]['fullname']).html() +'</li>'
                    }
                    $('#modal_delete .modal-body').removeClass('text-center');
                    $('#modal_delete .modal-body').html(
                            "<p><strong>Bạn muốn xóa yêu cầu của khách hàng: </strong></p>" +
                            "<ul>"+$html+"</ul>");
                    $('#btn_remove').removeClass('hide')
                    $('#modal_delete').modal('show');

                }else{
                    $('#modal_delete .modal-body').html("<strong>Vui lòng lựa chọn các mục cần xóa!</strong>");
                    $('#btn_remove').addClass('hide')
                    $('#modal_delete').modal('show');
                }

            });
            $('#btn_remove').on('click',function () {
                //Check length Row item selected
                var customer_id_list = $('.slted .customer_id');
                var temp_customer_id_list = [];
                for(var i=0; i<customer_id_list.length; i++){
                    temp_customer_id_list[i] = $(customer_id_list[i]).val();
                }
                $.ajax({
                    url: "<?php echo e(route('controller/admin/consult-remove')); ?>",
                    data:{
                        'customer_id_list': temp_customer_id_list
                    },
                    beforeSend: function () {
                        $('#modal_delete .modal-body').addClass('text-center');
                        $('#modal_delete .modal-body').html('<img src="<?php echo e(asset('public/img/loading.gif')); ?>">');
                    },
                    dataType: "text",
                    type: "get",
                    success: function (result) {
                        if(result == "Removed"){
                            var info = table.page.info();
                            table.page(info.page).rows('.slted').remove().draw(false);
                            $('#modal_delete .modal-body').addClass('text-center');
                            $('#modal_delete .modal-body').html(
                                    "<strong style='font-size: 1.5em' class='text-success'>Đã xóa <i class='glyphicon glyphicon-ok'></i></strong>");
                            $('#btn_remove').addClass('hide')
                            setTimeout(function () {
                                $('#modal_delete').modal('hide');
                            },2000);
                        }else {
                            $('#modal_delete .modal-body').addClass('text-center');
                            $('#modal_delete .modal-body').html("<strong>Lỗi kết nối, vui lòng thử lại sau!</strong>");
                            $('#modal_delete').modal('show');
                        }
                    }
                });

            });
            table.on('click','.btn_status',function () {
                var index = table
                        .row( $(this).parents('tr') )
                        .index();
                var status = null;
                var id = $(table.row( $(this).parents('tr') ).data()['content']).attr('data');
//              $($('.btn_status')[index]).html(1);
                if( $($('.btn_status button')[index]).hasClass('btn_none')){
                    status = 1;
                }else if($($('.btn_status button')[index]).hasClass('btn_ing')){
                    status = 2;
                }else{
                    status = 0;
                }
                $.ajax({
                    url: "<?php echo e(route('controller/admin/consults-changeStatus')); ?>",
                    beforeSend: function () {
                        $($('.btn_status button')[index]).attr('disabled','disabled');
                    },
                    data:{
                        'status': status,
                        'id': id
                    },
                    dataType: "json",
                    type: "get",
                    success: function (result) {
                       if(result['status']=='Success'){
                           if( $($('.btn_status button')[index]).hasClass('btn_none')){
                               $($('.btn_status')[index]).html('<button class="btn_ing btn btn-primary">Đang tư vấn</button>');
                           }else if($($('.btn_status button')[index]).hasClass('btn_ing')){
                               $($('.btn_status')[index]).html('<button class="btn_ed btn btn-success">Đã tư vấn</button>');
                           }else{
                               $($('.btn_status')[index]).html('<button class=" btn_none btn btn-warning">Chưa tư vấn</button>');
                           }
                       }else{
                           $($('.btn_status button')[index]).removeAttr('disabled');
                       }
                    }
                });
            });
            table.on('click','.btn_detail',function () {
                var customer_id =  $(table.row( $(this).parents('tr') ).data()['fullname']).attr('data');
                var consult_id =  $(table.row( $(this).parents('tr') ).data()['content']).attr('data');

                $.ajax({
                    url: "<?php echo e(route('controller/admin/consults-getInfo')); ?>",
                    beforeSend: function () {

                    },
                    data:{
                        'id': consult_id
                    },
                    dataType: "JSON",
                    type: "get",
                    success: function (result) {
                        //Tiếp tục xử lý xem thông tin yêu cầu
                        console.log(result[0]);
                        var info = result[0];
                        $('#modal_detail .modal-body #consult_content').val(info.content);
                        $('#modal_detail .modal-body #consult_note').val(info.note);
                        $('#modal_detail .modal-body #consult_id').val(info.consult_id);
                        $('#modal_detail .modal-body #customer_id').val(info.id);
                        $('#modal_detail .modal-body #slt_status').selectpicker('val',info.status);
                        $('#modal_detail .modal-body #slt_category_consult').selectpicker('val',info.category_consult_id);
                        $('#modal_detail .modal-body #slt_staff').selectpicker('val',info.staff_id);
                        $('#modal_detail .modal-body #customer_fullname').val(info.fullname);
                        $('#modal_detail .modal-body #customer_number_phone').val(info.number_phone);
                        $('#modal_detail .modal-body #customer_birthday').val(info.birthday);
                        $('#modal_detail .modal-body #customer_address').val(info.address);
                        $('#modal_detail').modal('show');
                    }
                });
            });
            $('#btn_edit_modal').on('click',function(){
                var input = $('#form_edit').serialize();
                console.log(input);
                $.ajax({
                    url: "<?php echo e(route('controller/admin/consults-edit')); ?>",
                    beforeSend: function () {

                    },
                    data:input,
                    dataType: "JSON",
                    type: "POST",
                    success: function (result) {
                       if(result.status == "Success"){
                           table.ajax.reload();
                           setTimeout(function () {
                               $('#modal_detail').modal('hide');
                           },2000);
                           swal({
                              title: "Thành công",
                              text: "Chỉnh sửa yêu cầu thành công.",
                              timer: 2000,
                              type: 'success',
                              showConfirmButton: false
                          });

                       }

                    }
                });
            });
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>