<?php $__env->startSection('css'); ?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
    <style>
        body{
            background-color: #525252;
        }
        .panel-heading{
            background-color: #108A93;
            color: white;
            text-align: center;
            padding: 20px 0px;
        }
        .panel-title{
            font-size: 20px;
        }

        .centered-form{
            margin-top: 10px;
        }

        .centered-form .panel{
            background: rgba(255, 255, 255, 0.8);
            box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('staff_pr','active'); ?>
<?php $__env->startSection('profiles_staff','active'); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row centered-form">
            <div class="col-xs-12 col-sm-12 col-md-8 col-sm-offset-0 col-md-offset-2 ">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Thông tin cá nhân<small></small></h3>
                    </div>
                    <div class="panel-body">
                        <form id="form" role="form" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <input type="hidden" name="id" value="<?php echo e($staff_info->id); ?>">
                            <div id="message" class="alert alert-success hide">
                                <strong></strong>
                                <span></span>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email" value="<?php echo e($staff_info->email); ?>" class="form-control input-lg" readonly>                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <div >
                                            <label>Level</label>
                                            <input type="text" class="form-control input-lg" value="<?php echo e($staff_info->level === 'admin'?'Quản trị viên':'Nhân Viên'); ?>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="col-xs-12 col-sm-12 col-md-12" style="padding: 0px">
                                        <div class="form-group">
                                            <label>Họ tên</label>
                                            <input type="text" name="fullname" id="fullname"  value="<?php echo e($staff_info->fullname); ?>" class="form-control input-lg" placeholder="Họ Tên">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12" style="padding: 0px">
                                        <div class="form-group">
                                            <div class="">
                                                <label>Số điện thoại</label>
                                                <input type="text" id="cmnd" name="number_phone" value="<?php echo e($staff_info->number_phone); ?>" class="form-control input-lg" placeholder="Số Điện thoại">
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Nhập Mật khẩu nếu muốn thay đổi">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label>Nhập lại Password</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Nhập lại Mật khẩu  nếu muốn thay đổi">
                                    </div>
                                </div>
                            </div>
                            <span style="font-size: 15px; font-style: italic; color: #f55954">*Lưu ý: Password để trống nghĩa là không thay đổi mật khẩu</span>
                            <hr>
                           
                            <hr>
                            <input type="button" id="btn_edit" value="Chỉnh Sửa" class="btn btn-info col-md-2 col-md-offset-5">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#message').addClass('hide');
            <!--Check Data-->
            $('#btn_edit').on('click',function () {
                var formData = $('#form').serialize();
                $.ajax({
                    url: "<?php echo e(route('controller/admin/edit-profile')); ?>",
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    async: false,
                    success: function (result) {

                        $("html,body").animate({scrollTop: 0}, 600);
                        if(result['status']=='Success'){
                            $('#message').addClass('alert-success');
                            $('#message').removeClass('alert-danger');
                            $('#message').removeClass('hide');
                            $('#message strong').text('Success');
                            $('#message span').text('Thêm tài khoản thành công');
                            $('input[type=password]').val('');
                        }else{
                            $('#message').addClass('alert-danger');
                            $('#message').removeClass('alert-success');
                            $('#message').removeClass('hide');
                            $('#message strong').text('Warning! Bạn có thể đang gặp các lỗi sau:  ');
                            $('#message span').html('<ul>'+result['error']+'</ul>');
                        }
                    }
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>