<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="public/css/style2.css">
    <link rel="stylesheet" type="text/css" href="public/css/jquery-ui.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/jquery-ui/jquery-ui.min.css">
    <style>
        .ui-autocomplete-loading {
            background: white url("public/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
        //overflow-x: hidden;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {
            height: 200px;
        }
    </style>
    <!--    <script type="text/javascript" src="assets/jquery-ui/external/jquery/jquery.js"></script>-->
    <!--    <script type="text/javascript" src="assets/jquery-ui/jquery-ui.min.js"></script>-->

<!------ Include the above in your HEAD tag ---------->
    <script>
        var kq = '';
        $( function() {
            $("#search").on("click",function () {
                var id = $('#idmodel').val();
               
                $.ajax({
                    url: "searchmodel",
                    data:{
                        'id': id
                    },
                    beforeSend: function () {
                        $('.modal-body').html('<img src="<?php echo e(asset('public/img/loading.gif')); ?>">');
                    },
                    dataType: "text",
                    type: "get",
                    success: function (result) {
                        console.log(result);
                        kq =JSON.parse(result);
                        $('#model_name').html(kq.model);
                        $('#cachhieuchuan').html(kq.cachhieuchuan)
                    }
                });
            });
            $("#model").autocomplete({
                source:  'getmodel',
                select: function (event, ui) {
                    $("#idmodel").val(ui.item.id); // save selected id to hidden input
                }
            });

        } );
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <center><h2>TRUNG TÂM HIỆU CHUẨN HHỞI TOÀN</h2></center>
        </div>
        <div class="col-md-6 col-md-offset-3">
            
            <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" id="model" name="model" placeholder="BSS01" />
                    <input type="hidden" id="idmodel">
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button" id="search">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-6 col-md-offset-3">
            <h3>Tên model : </h3><div id="model_name"></div>
            <h3>Cách Hiệu Chuẩn :</h3>
            <div id="cachhieuchuan"></div>
        </div>
    </div>
</div>
<body>