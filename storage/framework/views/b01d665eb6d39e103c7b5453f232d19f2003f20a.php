<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tìm kiếm model cân</title>
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="public/css/style2.css">
    <link rel="stylesheet" type="text/css" href="public/css/jquery-ui.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/jquery-ui/jquery-ui.min.css">
    <style>
        .ui-autocomplete-loading {
            background: white url("public/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }
        .ui-autocomplete {
            max-height: 200px;
            overflow-y: auto;
            /* prevent horizontal scrollbar */
        //overflow-x: hidden;
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {
            height: 200px;
        }
        .navbar {
            border-radius: 0;
        }

        .navbar-new {
            background-color: #23BAB5;
            border-color: #0FA6A1;
        }

        .navbar-new .navbar-brand,
        .navbar-new .navbar-brand:hover,
        .navbar-new .navbar-brand:focus {
            color: #FFF;
        }

        .navbar-new .navbar-nav > li > a {
            color: #FFF;
        }

        .navbar-new .navbar-nav > li > a:hover,
        .navbar-new .navbar-nav > li > a:focus {
            background-color: #0FA6A1;
        }

        .navbar-new .navbar-nav > .active > a,
        .navbar-new .navbar-nav > .active > a:hover,
        .navbar-new .navbar-nav > .active > a:focus {
            color: #FFF;
            background-color: #0FA6A1;
        }

        .navbar-new .navbar-text {
            color: #FFF;
        }

        .navbar-new .navbar-toggle {
            border-color: #0FA6A1;
        }

        .navbar-new .navbar-toggle:hover,
        .navbar-new .navbar-toggle:focus {
            background-color: #0FA6A1;
        }

        .navbar-new .navbar-toggle .icon-bar {
            background-color: #FFF;
        }
    </style>
    <!--    <script type="text/javascript" src="assets/jquery-ui/external/jquery/jquery.js"></script>-->
    <!--    <script type="text/javascript" src="assets/jquery-ui/jquery-ui.min.js"></script>-->

<!------ Include the above in your HEAD tag ---------->
    <script>
        var kq = '';
        $( function() {
            $("#search").on("click",function () {
                var id = $('#idmodel').val();
               
                $.ajax({
                    url: "searchmodel",
                    data:{
                        'id': id
                    },
                    beforeSend: function () {
                        $('.modal-body').html('<img src="<?php echo e(asset('public/img/loading.gif')); ?>">');
                    },
                    dataType: "text",
                    type: "get",
                    success: function (result) {
                        console.log(result);
                        kq =JSON.parse(result);
                        $('#model_name').html(kq.model);
                        $('#cachhieuchuan').html(kq.cachhieuchuan)
                    }
                });
            });
            $("#model").autocomplete({
                source:  'getmodel',
                select: function (event, ui) {
                    $("#idmodel").val(ui.item.id);
                    $("#search").trigger('click');// save selected id to hidden input
                }
            });

        } );
    </script>
</head>
<body>
<nav class="navbar navbar-new" role="navigation">
    <div class="container-fluid">
        <!-- logo -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Khởi Toàn Company</a>
        </div>
        <!-- menu -->
        <div class="collapse navbar-collapse navbar-right" id="navbar1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Tra cứu hiệu chuẩn cân</a></li>
                <li><a href="staff/login">Admin</a></li>
                <li><a href="http://khoitoan.com.vn/">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <center><h2>TRUNG TÂM HIỆU CHUẨN KHỞI TOÀN</h2></center>
        </div>
        <div class="col-md-6 col-md-offset-3">
            
            <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" id="model" name="model" placeholder="Nhập tên model" />
                    <input type="hidden" id="idmodel">
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button" id="search">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-6 col-md-offset-3">
            <h3>Tên model : </h3><div id="model_name"></div>
            <h3>Cách Hiệu Chuẩn :</h3>
            <div id="cachhieuchuan"></div>
        </div>
    </div>
</div>
<body>
