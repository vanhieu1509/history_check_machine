<?php $__env->startSection('title',' Website tài chính - Giá vàng'); ?>
<?php $__env->startSection('css'); ?>
<style type="text/css">
		#d,#u,.d,.u{
			background: url(http://giavangvn.org/Content/Img/bgCommon.gif) no-repeat;
		}
		span.u {
    background-position: -70px 0;
    color: green;
    padding-left: 17px;
}
span#d, span.d {
    color: red;
    background-position: -70px -19px;
    padding-left: 17px;
}
.table th {
    background-color: #dff0d8!important;
}
.text-center {
    text-align: center;
}

	</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
<h2 style="text-align: center">Bảng giá vàng trong nước và thế giới</h2>
	<div class="col-md-12">
		<?php echo $table; ?>

	</div>
	<h4 style="float: right;">Nguồn tham khảo : <a href="http://www.giavang.net/bang-gia-vang/">giavang.net</a></h4>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>