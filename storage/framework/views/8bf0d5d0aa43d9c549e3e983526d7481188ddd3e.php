<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tra Cứu Lịch Sử Hiệu Chuẩn</title>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="public/css/style2.css">
    <link rel="stylesheet" type="text/css" href="public/css/jquery-ui.min.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/jquery-ui/jquery-ui.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <style>
        @media (min-width: 978px) {
        body{
            padding: 17px;
        }
        }
        /* IE 6 doesn't support max-height
         * we use height instead, but this forces the menu to always be this tall
         */
        * html .ui-autocomplete {
            height: 200px;
        }
        select{
            font-size: 50px;
        }
        .navbar {
            border-radius: 0;
        }
        
        .navbar-new {
            background-color: #23BAB5;
            border-color: #0FA6A1;
        }
        
        .navbar-new .navbar-brand,
        .navbar-new .navbar-brand:hover,
        .navbar-new .navbar-brand:focus {
            color: #FFF;
        }
        
        .navbar-new .navbar-nav > li > a {
            color: #FFF;
        }
        
        .navbar-new .navbar-nav > li > a:hover,
        .navbar-new .navbar-nav > li > a:focus {
            background-color: #0FA6A1;
        }
        
        .navbar-new .navbar-nav > .active > a,
        .navbar-new .navbar-nav > .active > a:hover,
        .navbar-new .navbar-nav > .active > a:focus {
            color: #FFF;
            background-color: #0FA6A1;
        }
        
        .navbar-new .navbar-text {
            color: #FFF;
        }
        
        .navbar-new .navbar-toggle {
            border-color: #0FA6A1;
        }
        
        .navbar-new .navbar-toggle:hover,
        .navbar-new .navbar-toggle:focus {
            background-color: #0FA6A1;
        }
        
        .navbar-new .navbar-toggle .icon-bar {
            background-color: #FFF;
        }
    </style>
    <!--    <script type="text/javascript" src="assets/jquery-ui/external/jquery/jquery.js"></script>-->
    <!--    <script type="text/javascript" src="assets/jquery-ui/jquery-ui.min.js"></script>-->
    
    <!------ Include the above in your HEAD tag ---------->
    <script>
    $(document).ready(function () {
        $("#company_id").select2({
            placeholder: "Chọn Công Ty",
           
        });
    //Disable cut copy paste
    // $('body').bind('cut copy paste', function (e) {
    //     e.preventDefault();
    // });
        rs = '';
        var company_id = '';
        var machine_id = '';
        $("#company_id").on('change', function(){
            company_id = $("#company_id").val();
            $('#div_machine_id').hide();
            $('#cachhieuchuan').html('');
                $.ajax({
                    url: "getmachine",
                    data:{
                        'id': company_id
                    },
                    beforeSend: function () {
                        $('.modal-body').html('<img src="<?php echo e(asset('public/img/loading.gif')); ?>">');
                    },
                    dataType: "text",
                    type: "get",
                    success: function (result) {
                       
                        $('#machine_id')
                            .empty()
                            .append('<option disabled selected value> -- Chọn Loại Máy -- </option>')
                        ;
                        $.each(JSON.parse(result), function(k, v) {
                            $('<option>').val(v.id).text(v.name+'-'+v.model+'-'+v.seri+'-'+v.nsx).appendTo('#machine_id');
                        });
                        rs = result;
                        if($('#machine_id option').length > 1) {
                            $('#div_machine_id').show();
                            $("#machine_id").select2({
                                width: 'resolve'
                            })
                        }
                    }
                });
       
        });
        $("#machine_id").on('change', function() {
            machine_id = $("#machine_id").val();
            $('#show_history').html('')
            $.ajax({
                url: "gethistory",
                data:{
                    'id_company': company_id,
                    'id_machine': machine_id
                },
                beforeSend: function () {
                    $('.modal-body').html('<img src="<?php echo e(asset('public/img/loading.gif')); ?>">');
                },
                dataType: "text",
                type: "get",
                success: function (result) {
                    kq =JSON.parse(result);
                    $('#show_history').html(kq.content)
                }
            });
        });
        
    //Disable mouse right click
    // $("body").on("contextmenu",function(e){
    //     return false;
    // });
});
        var kq = '';
        $( function() {
            $("#search").on("click",function () {
                var id = $('#idmodel').val();

                $.ajax({
                    url: "searchmodel",
                    data:{
                        'id': id
                    },
                    beforeSend: function () {
                        $('.modal-body').html('<img src="<?php echo e(asset('public/img/loading.gif')); ?>">');
                    },
                    dataType: "text",
                    type: "get",
                    success: function (result) {
                        console.log(result);
                        kq =JSON.parse(result);
                        $('#model_name').html(kq.model);
                        $('#cachhieuchuan').html(kq.cachhieuchuan)
                    }
                });
            });
            $("#model").autocomplete({
                source:  'getmodel',
                select: function (event, ui) {
                    $("#idmodel").val(ui.item.id);
                    $("#search").trigger('click');// save selected id to hidden input
                }
            });

        } );
    </script>
</head>
<body>
    <h5 style="color: red"; margin-left: 4px">TRUNG TÂM HIỆU CHUẨN KIỂM ĐỊNH KHỞI TOÀN </h5>
<h6 style="color: red"margin-left: 4px"><i class="fa fa-address-card" aria-hidden="true"></i> Số 89 Đường số 6, ấp Tiền Lân, X.Bà Điểm, H.Hóc Môn, TP.HCM</h6>
<h6 style="color: red"margin-left: 4px"><i class="fa fa-phone" aria-hidden="true"></i> (84-28) 73016236 - 36205909 - Hotline: 0938 729 490</h6>
<h6 style="color: red"margin-left: 4px"><i class="fa fa-envelope" aria-hidden="true" style="margin-left: 2px"></i>cavec@khoitoan.com.vn</h6>

<nav class="navbar navbar-new" role="navigation">
    <div class="container-fluid">
        <!-- logo -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- menu -->
        <div class="collapse navbar-collapse navbar-left" id="navbar1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Tra cứu lịch sử kiểm định</a></li>
                <li><a href="staff/login" target="blank">Trang Quản Trị</a></li>
				<li><a href="http://khoitoan.com.vn/">Liên Hệ</a></li>
                <li><a href="admin/logout">Logout</a></li>
               
                
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <form class="form-horizontal">
        <fieldset>
            
            <!-- Form Name -->
            <legend align="center">Tra Cứu Lịch Sử Kiểm Định</legend>
            
            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="company_id">Công Ty:</label>
                <div class="col-md-4">
                    <select id="company_id" name="company_id" class="form-control">
                        <option disabled selected value> -- Chọn Công Ty -- </option>
                        <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </select>
                </div>
            </div>
            
            <!-- Select Basic -->
            <div id="div_machine_id" class="form-group" style="display: none">
                <label class="col-md-4 control-label" for="machine_id">Tên Máy:</label>
                <div class="col-md-4">
                    <select id="machine_id" name="machine_id" class="form-control">
                        <option disabled selected value> -- Chọn Máy -- </option>
                    </select>
                </div>
            </div>
        
        </fieldset>
    </form>
    
        <div class="col-md-6 col-md-offset-3">
        <h4  style="color: #663300">Lịch Sử Kiểm Tra :</h4>
        <div id="history">
            <div id="show_history"></div>
        </div>
    </div>
    
</div>
<body>
