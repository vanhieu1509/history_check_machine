<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(asset('lib/AdminLTE/dist/css/AdminLTE.min.css')); ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    
    
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo e(asset('lib/AdminLTE/dist/css/skins/_all-skins.min.css')); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo e(asset('lib/AdminLTE/plugins/iCheck/flat/blue.css')); ?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo e(asset('lib/AdminLTE/plugins/morris/morris.css')); ?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo e(asset('lib/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.css')); ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo e(asset('lib/AdminLTE/plugins/daterangepicker/daterangepicker.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('lib/AdminLTE/plugins/datepicker/datepicker3.css')); ?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo e(asset('lib/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   
    <![endif]-->
    <style>
        /* Paste this css to your style sheet file or under head tag */
        /* This only works with JavaScript,
        if it's not present, don't show loader */
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(<?php echo e(asset('/public')); ?>/img/Preloader_3.gif) center no-repeat #fff;
        }
        .se-pre-con2 {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            display: none;
            height: 100%;
            opacity: 0.7;
            z-index: 9999;
            background: url(<?php echo e(asset('/public')); ?>/img/Preloader_4.gif) center no-repeat #fff;
        }
        .select2-container .select2-selection--single {
            height: 34px !important;
        }
    </style>
    <script src="<?php echo e(asset('lib/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')); ?>"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script>
        //paste this code under head tag or in a seperate js file.
        // Wait for window load
        $(window).load(function() {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");;
        });
        var level = "<?php echo e($staff_info->level); ?>";
    </script>
    
    
    <?php echo $__env->yieldContent('css'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="se-pre-con"></div>
<div class="se-pre-con2"></div>
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>K</b>T</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>TRANG QUẢN TRỊ</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../public/img/avatar.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">

                                    <?php echo e(isset($staff_info->fullname) ? $staff_info->fullname : 'User'); ?>


                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="../public/img/avatar.jpg" class="img-circle" alt="User Image">
                                
                                <p>
                                    <?php echo e(isset($staff_info->fullname) ? $staff_info->fullname : 'USER'); ?>- Khởi Toàn Co,LTD
                                
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?php echo e(action("Admin\AdminHomeController@getViewProfile")); ?>" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!--    Left side column. contains the logo and sidebar-->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="../public/img/avatar.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>
                        <?php echo e(isset($staff_info->fullname) ? $staff_info->fullname : 'User'); ?>

                    </p>
                    
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
            <?php if($staff_info->level == 'admin'): ?>
                <!--Nhân Viên-->
                    <li class="treeview <?php echo $__env->yieldContent('staff'); ?>">
                        <a href="">
                            <i class="fa fa-user"></i> <span>Quản lý nhân viên</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu">
                            <li class=" list_staff <?php echo $__env->yieldContent('list_staff'); ?>"><a href="staff-list"><i class="fa fa-list-ul"></i> Danh sách nhân viên</a></li>
                            <li class=" add_staff <?php echo $__env->yieldContent('add_staff'); ?>"><a href="staff-add"><i class="fa fa-user-plus"></i> Thêm thành viên</a></li>
                        </ul>
                    </li>
                <?php endif; ?>
                <li class="treeview <?php echo $__env->yieldContent('staff_pr'); ?>">
                    <a href="">
                        <i class="fa fa-user"></i> <span>Thông tin cá nhân</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class=" profiles_staff <?php echo $__env->yieldContent('profiles_staff'); ?>"><a href="<?php echo e(action('Admin\ProfileController@getViewEdit')); ?>"><i class="fa fa-list-ul"></i>Chỉnh sửa thông tin</a></li>
                    </ul>
                </li>
                <li class="treeview <?php echo $__env->yieldContent('company'); ?> ">
                    <a href="">
                        <i class="fa fa-bank"></i> <span>Quản lý Công ty</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class=" list_bank <?php echo $__env->yieldContent('company-list'); ?>"><a href="company-list"><i class="fa fa-list-ul"></i> Danh sách Công ty</a></li>
                        <li class=" add_bank <?php echo $__env->yieldContent('company-add'); ?>"><a href="company-add"><i class="fa fa-plus"></i> Thêm Công ty</a></li>
                    
                    </ul>
                </li>
                <li class="treeview <?php echo $__env->yieldContent('machine'); ?> ">
                    <a href="">
                        <i class="fa fa-wrench"></i> <span>Quản lý Máy</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li class=" list_bank <?php echo $__env->yieldContent('machine-list'); ?>"><a href="machine-list"><i class="fa fa-list-ul"></i> Danh sách Máy</a></li>
                        <li class=" add_bank <?php echo $__env->yieldContent('machine-add'); ?>"><a href="machine-add"><i class="fa fa-plus"></i> Thêm Máy</a></li>
                        <li class=" add_bank"><a  href="../"><i class="fa fa-search"></i> Tra Cứu Lịch Sử Kiểm Tra</a></li>
        
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <section class="content-header" >
            <?php echo $__env->yieldContent('content_header'); ?>
        </section>
        <!-- Main content -->
        <section  class="content">
            <?php echo $__env->yieldContent('content'); ?>
        </section>
    </div>
    <!-- /.content-Footer -->
    <footer class="main-footer">
        <?php echo $__env->yieldContent('content_footer'); ?>
    </footer>
    
    
    <div class="control-sidebar-bg"></div>


</div>
<?php echo $__env->yieldContent('modal'); ?>
<!-- jQuery 2.2.3 -->

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>





<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->


<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo e(asset('lib/AdminLTE/plugins/morris/morris.min.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<!-- Sparkline -->
<script src="<?php echo e(asset('lib/AdminLTE/plugins/sparkline/jquery.sparkline.min.js')); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo e(asset('lib/AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(asset('lib/AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo e(asset('lib/AdminLTE/plugins/knob/jquery.knob.js')); ?>"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo e(asset('lib/AdminLTE/plugins/daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(asset('lib/AdminLTE/plugins/datepicker/bootstrap-datepicker.js')); ?>"></script>
<script src="<?php echo e(asset('lib/AdminLTE/plugins/ckeditor/ckeditor.js')); ?>" ></script>
<script src="<?php echo e(asset('lib/AdminLTE/plugins/ckfinder/ckfinder.js')); ?>" ></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo e(asset('lib/AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo e(asset('lib/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('lib/AdminLTE/plugins/fastclick/fastclick.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('lib/AdminLTE/dist/js/app.min.js')); ?>"></script>
<!-- Datatable -->


<!-- AdminLTE for demo purposes -->
<script src="<?php echo e(asset('lib/AdminLTE/dist/js/demo.js')); ?>"></script>
<script scrc="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<script src="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.js"></script>
<?php echo $__env->yieldContent('script'); ?>
</body>
</html>
