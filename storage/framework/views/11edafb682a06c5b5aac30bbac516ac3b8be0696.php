<?php $__env->startSection('css'); ?>
    <style>
        #panel_table {
            padding: 0px;
            border-color: #5CB85C;
            border-style: groove;
        }
        .panel-heading{
            padding-top: 20px;
            padding-bottom: 20px;
            font-size: 16px;
            background-color: #5CB85C!important;
            color: white!important;
        }
         .box-footer{
            padding: 0px;
        }
        .box-body{
            padding-bottom: 0px;
        }
        .modal-header{
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }
        .modal-header .modal-title{
            color: white;
        }
        .modal-content{
            border-radius: 6px;
        }
        #btn_create{
            padding-left: 25px;
            padding-right: 25px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('machine','active'); ?>
<?php $__env->startSection('machine-add','active'); ?>
<?php $__env->startSection('content'); ?>
    <div class="container" align="center">
        <div id="panel_table" class="panel panel-info col-md-10 col-md-offset-1">
            <div class="panel-heading text-center">Thêm Máy</div>
            <div class="panel-body">
                <form id="form" role="form" enctype="multipart/form-data" method="post">
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="form-group col-md-5">
                                <label>Tên Máy</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="vd: Cân điện tử">
                            </div>
                            <div class="form-group col-md-7">
                                <label>Tên Công ty</label>
                                <select class="form-control" id="company_id" name="company_id">
                                    <option disabled selected value> -- Chọn Công Ty -- </option>
                                    <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Model</label>
                                <input type="text" class="form-control" id="model" name="model"
                                       placeholder="vd: BDIXXX">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Seri</label>
                                <input type="text" class="form-control" id="seri" name="seri" placeholder="vd: CAS">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Hãng sản xuất</label>
                                <input type="text" class="form-control" id="nsx" name="nsx" placeholder="vd: CAS">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Lịch sử</label>
                                <textarea class="form-control" id="intro" name="intro"
                                          placeholder="Bước 1: ...."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button  id="btn_create" class="btn btn-primary">Tạo</button>
                    </div>
                </form>
            </div>
        </div>
        <!--modal error-->
        <div id="modal_error" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <h4 class="text-danger"></h4>
                            <div class="list_error"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default left" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>  <!--End modal error-->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript" charset="utf-8">
        $(document).on('ready', function () {
            $("#company_id").select2({
                placeholder: "Chọn Công Ty",
                width: 'resolve'
            });
            CKEDITOR.replace('intro', {
                filebrowserImageBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Images',
                filebrowserFlashBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Flash',
                filebrowserImageUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

            });
            $("#form").submit(function(){
                var intro = CKEDITOR.instances['intro'].getData();
                var formData = new FormData($(this)[0]);
                formData.append('intro',intro);
                $.ajax({
                    url: "<?php echo e(route('controller/admin/machine-add')); ?>",
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    async: false,
                    success: function (result) {
                        if(result['status'] == "Success"){
                            $('.text-danger  ').html('Success');
                            $('.list_error  ').html('');
                            $('.modal-header').css('background-color',' #2ecc71')
                            $('.modal-title').html('<i class="fa fa-check-circle fa-3x"></i>');
                            $('#modal_error').modal('show');
                            $('#form input[type=text],#form input[type=file]').val('');
                            CKEDITOR.instances['intro'].setData('');
                        }else{
                            $('.modal-body div').css('text-align','left');
                            $('.modal-header').css('background-color',' #D9534F')
                            $('.modal-title').html('<i class="fa fa-times-circle fa-3x"></i>');
                            $('.list_error  ').html('<ul>'+result['error']+'</ul>');
                            $('.text-danger  ').html('Bạn có thể đang gặp một trong số các lỗi sau:');
                            $('#modal_error').modal('show');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
                return false;
            });

        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>