<?php $__env->startSection('staff','active'); ?>
<?php $__env->startSection('add_staff','active'); ?>
<?php $__env->startSection('css'); ?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
    <style>
        body{
            background-color: #525252;
        }
        .panel-heading{
            background-color: #108A93;
            color: white;
            text-align: center;
            padding: 20px 0px;
        }
        .panel-title{
            font-size: 20px;
        }

        .centered-form{
            margin-top: 10px;
        }

        .centered-form .panel{
            background: rgba(255, 255, 255, 0.8);
            box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
        }
        /*CSS cho button selector-bootstrap*/
        .bootstrap-select >button {
            background-color: white;!important;
            font-size: 16px;
        }
        /*Khi đưa con trỏ vào button selector thì sẽ như nào*/
        .bootstrap-select >button:hover{
            background-color: whitesmoke;

        }
        /*Khi click vào button selector thì sẽ như nào*/
        .open>.dropdown-toggle.btn-default{
            background: whitesmoke!important;

        }
        /*Text cho option*/
        .text {
            font-size: 16px;
        }
        /*Biểu tượng selected bên option*/
        .check-mark{
            color: green;
        }
        div[role=combobox]{

            margin-top: 10px;
        }
        #file{
            font-size: 14px;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row centered-form">
            <div class="col-xs-12 col-sm-12 col-md-8 col-sm-offset-0 col-md-offset-2 ">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tạo tài khoản Nhân Viên <small></small></h3>
                    </div>
                    <div class="panel-body">
                        <form id="form" role="form" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div id="message" class="alert alert-success hide">
                                <strong></strong>
                                <span></span>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="fullname" id="fullname" class="form-control input-lg" placeholder="Họ Tên">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Địa chỉ Email">                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Mật khẩu">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Nhập lại Mật khẩu">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <div class="">
                                            <label>Số điện thoại</label>
                                            <input type="text" id="cmnd" name="number_phone" class="form-control input-lg" placeholder="Số Điện thoại">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <div >
                                            <label>Level</label>
                                            <select class="form-control input-lg" id="slt_level" name="slt_level" title="">
                                                <option value="admin">Admin</option>
                                                <option value="st_news" selected >Nhân Viên</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <hr>
                            <input type="button" id="btn_create" value="Tạo" class="btn btn-info col-md-2 col-md-offset-5">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#message').addClass('hide');
            <!--Selector-->
            $('.selectpicker').selectpicker({
                style: 'btn-default input-lg ',
                size: 4
            });

            <!--Preview Image before Send-->
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img_avatar').removeClass('hide');
                        $('#img_avatar').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#file").change(function(){
                $('#img_avatar').addClass('hide');
                readURL(this);
            });
            <!--Check Data-->
           $('#btn_create').on('click',function () {
                var formData = new FormData($('#form')[0]);
               var slt_category_consult = $('#slt_category_consult').val();
               var slt_level = $('#slt_level').val();
               formData.append('slt_category_consults',slt_category_consult);
               formData.append('slt_level',slt_level);
                $.ajax({
                    url: "<?php echo e(route('controller/admin/staff-add')); ?>",
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    async: false,
                    success: function (result) {
                        $("html,body").animate({scrollTop: 0}, 600);
                        if(result['status']=='Success'){
                            $('#message').addClass('alert-success');
                            $('#message').removeClass('alert-danger');
                            $('#message').removeClass('hide');
                            $('#message strong').text('Success');
                            $('#message span').text('Thêm tài khoản thành công');
                            $("html,body").animate({scrollTop: 0}, 600);
                            $('#form input[type=text],#form input[type=password],#form input[type=email],#form input[type=file],#slt_bank').val('');
                            $('#slt_bank').prop('selectedIndex',0);
                            $('#slt_category_consult').selectpicker('deselectAll');
                            $('#img_avatar').addClass('hide');
                        }else{
                            $('#message').addClass('alert-danger');
                            $('#message').removeClass('alert-success');
                            $('#message').removeClass('hide');
                            $('#message strong').text('Warning! Bạn có thể đang gặp các lỗi sau:  ');
                            $('#message span').html('<ul>'+result['error']+'</ul>');

                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>