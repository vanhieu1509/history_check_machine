<?php $__env->startSection('title',' Website tài chính - Tin trong nước'); ?>
<?php $__env->startSection('css'); ?>
    <style>
        .small-thumbs .clearfix{
            margin-bottom: 20px;!important;
            padding-bottom: 0px!important;
        }
        .small-thumbs .clearfix .entry-image a img{
            width: 300px!important;
            height: 225px!important;
        }
        .small-thumbs .clearfix .entry-c .entry-meta{
            margin-bottom: -30px;!important;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('news_active','current'); ?>
<?php $__env->startSection('national_news_active','current'); ?>
<?php $__env->startSection('banner'); ?>
    <!-- Page Title
            ============================================= -->
    <section id="page-title" style="background-image: url('<?php echo e(asset("lib/Canvas/")); ?>/images/slider/swiper/1.jpg')">

        <div class="container clearfix ">
            <h1 style="font-family: Tahoma!important; color: white; text-transform:capitalize">Tin trong nước</h1>
            <span style=" color: white">Cập nhật diễn biến tài chính trong nước</span>
            <ol class="breadcrumb">
                <li><a href="#"  style=" color: white">Trang chủ</a></li>
                <li class="active"  style=" color: white">Tin trong nước</li>
            </ol>
        </div>

    </section><!-- #page-title end -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <!-- Post Content
					============================================= -->
    <div class="postcontent nobottommargin clearfix">

        <!-- Posts
        ============================================= -->
        <div id="posts" class="small-thumbs">
            <?php $__currentLoopData = $news_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $news): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <div class="entry clearfix" style="height: 233px!important;">
                    <div class="entry-image">
                        <a href="<?php echo e($news->link); ?>" data-lightbox="image"><img style="width: 300px!important; height: 225px!important;" class="image_fade" src="<?php echo e($news->image); ?>" alt="Standard Post with Image"></a>
                    </div>
                    <div class="entry-c">
                        <div class="entry-title">
                            <h2><a href="<?php echo e($news->link); ?>"><?php echo e($news->title?$news->title:'No Title'); ?></a></h2>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><i class="icon-calendar3"></i><?php echo e($news->created_at); ?></li>
                            <li><a href="#"><i class="icon-eye-open"></i> <?php echo e($news->number_view); ?></a></li>
                        </ul>
                        <div class="entry-content">
                            <p><?php echo e($news->sumary); ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

        </div><!-- #posts end -->

        <!-- Pagination
        ============================================= -->
        <div style="text-align: center!important;">
            <?php echo e($news_list->links()); ?>

        </div>


    </div><!-- .postcontent end -->

    <!-- Sidebar
        ============================================= -->
    <div class="sidebar nobottommargin col_last clearfix">
        <div class="sidebar-widgets-wrap">
            <div class="widget clearfix">

                <div class="tabs nobottommargin clearfix" id="sidebar-tabs">

                    <ul class="tab-nav clearfix">
                        <li><a href="#tabs-1">Popular</a></li>
                        <li><a href="#tabs-2">Recent</a></li>
                    </ul>

                    <div class="tab-container">
                        <div class="tab-content clearfix" id="tabs-1">
                            <div id="popular-post-list-sidebar">
                                <?php $__currentLoopData = $popular_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href="#" class="nobg"><img class="img-circle"
                                                                          src="<?php echo e($item->image); ?>" alt=""></a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="/Finance/<?php echo e($item->link); ?>"><?php echo e($item->title?$item->title:'No Title'); ?></a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li><i class="icon-eye-open"></i><?php echo e($item->number_view); ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </div>
                        </div>
                        <div class="tab-content clearfix" id="tabs-2">
                            <div id="recent-post-list-sidebar">
                                <?php $__currentLoopData = $new_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href="#" class="nobg"><img class="img-circle"
                                                                          src="<?php echo e($item->image); ?>" alt=""></a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="<?php echo e($item->link); ?>"><?php echo e($item->title?$item->title:'No Title'); ?></a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li><?php echo e($item->created_at); ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div><!-- .sidebar end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>