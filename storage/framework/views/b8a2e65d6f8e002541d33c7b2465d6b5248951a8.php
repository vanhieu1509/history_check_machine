<?php $__env->startSection('news','active'); ?>
<?php $__env->startSection('news-add','active'); ?>
<?php $__env->startSection('css'); ?>
    <style>
        #panel_table {
            padding: 0px;
            border-color: #1ab1b8;
            border-style: groove;
        }
        .panel-heading{
            padding-top: 20px;
            padding-bottom: 20px;
            font-size: 16px;
            background-color: #1ab1b8 !important;
            color: white!important;
        }
        .box-footer{
            padding: 0px;
        }
        .box-body{
            padding-bottom: 0px;
        }
        .modal-header{
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }
        .modal-header .modal-title{
            color: white;
        }
        .modal-content{
            border-radius: 6px;
        }
        #btn_create{
            padding-left: 25px;
            padding-right: 25px;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="container" align="center">
        <div id="panel_table" class="panel panel-info col-md-10 col-md-offset-1">
            <div class="panel-heading text-center">Thêm Tin Tức</div>
            <div class="panel-body">
                <form id="form" role="form" enctype="multipart/form-data" method="post" >
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="staff_id" id="staff_id" value="<?php echo e(isset($staff_info->id) ? $staff_info->id : 0); ?>">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div id="message" class="alert alert-success hide">
                                <strong></strong>
                                <span></span>
                            </div>
                            <div class="form-group col-md-8">
                                <label>Tiêu đề bài viết: </label>
                                <input type="text" class="form-control" id="add_title" name="title" placeholder="Giá vàng hôm nay (25/4) giảm kỷ lục trong 20 năm qua.">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="usr">Loại bài viết:</label>
                                <select class="form-control" id="slt_category_news" name="category_news_id" title="Lựa chọn loại tin">
                                    <?php $__currentLoopData = $category_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="usr">Nội dung tóm tắt của bài viết:</label>
                                <textarea style=" resize:vertical; height: 100px!important; max-height: 100px!important;" type="text" name="sumary" class="form-control" id="add_sumary"></textarea>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group col-md-12">
                                    <label>Nguồn bài viết</label>
                                    <input type="text" class="form-control" id="add_source" name="source"
                                           placeholder="Nguồn bài viết (Nếu có)">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Ảnh đại diện cho bài viết</label>
                                    <input id="add_image" name="image" type="file" accept=".jpg,.png"
                                           class="form-control file-loading">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="text-center" style="width: 100%">
                                        <img class="img-thumbnail center" id="img_avatar" src="<?php echo e(asset('public/img/no-image.png')); ?>"  style="height: 225px; width: 300px" alt="Avatar" />
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group col-md-12 block">
                                <label style="display: inherit;text-align: left!important;"  class="control-label">Nội dung chính của bài viết</label>
                                <textarea class="form-control" id="add_content"
                                          placeholder="www.abbank.com"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button  type="button" id="btn_create" class="btn btn-primary">Tạo</button>
                    </div>
                </form>
            </div>
        </div>
        <!--modal error-->
        <div id="modal_error" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <h4 class="text-danger"></h4>
                            <div class="list_error"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default left" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>  <!--End modal error-->
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript" charset="utf-8">
        $(document).on('ready', function () {
            CKEDITOR.replace('add_content', {
                filebrowserImageBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Images',
                filebrowserFlashBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Flash',
                filebrowserImageUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

            });
            <!--Preview Image before Send-->
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img_avatar').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#add_image").change(function(){
                $('#img_avatar').attr('src',"<?php echo e(asset('public/img/no-image.png')); ?>");
                readURL(this);
            });

            $('#btn_create').on('click',function () {
                var content = CKEDITOR.instances['add_content'].getData();
                var formData = new FormData($('#form')[0]);
                var category_news_id = $('#slt_category_news').val();
                formData.append('category_news_id',category_news_id);
                formData.append('contenta',content);
                $.ajax({
                    url: "<?php echo e(route('controller/admin/news-add')); ?>",
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    async: false,
                    success: function (result) {
                        console.log(result);
                        $("html,body").animate({scrollTop: 0}, 600);
                        if(result['status']==1){
                            $('#message').addClass('alert-success');
                            $('#message').removeClass('alert-danger');
                            $('#message').removeClass('hide');
                            $('#message strong').text('Success');
                            $('#message span').text('Thêm bài viết thành công');
                            $("html,body").animate({scrollTop: 0}, 600);
                            $('#form input[type=text],#form input[type=file],#slt_category_news,textarea').val('');
                            $('#slt_category_news').prop('selectedIndex',0);
                            CKEDITOR.instances['add_content'].setData();
                            $('#img_avatar').attr('src',"<?php echo e(asset('public/img/no-image.png')); ?>");
                        }else{
                            $('#message').addClass('alert-danger');
                            $('#message').removeClass('alert-success');
                            $('#message').removeClass('hide');
                            $('#message strong').text('Warning! Bạn có thể đang gặp các lỗi sau:  ');
                            $('#message span').html('<ul>'+result['message']+'</ul>');

                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });

        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>