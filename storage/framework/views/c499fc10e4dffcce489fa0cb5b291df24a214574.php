<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <style>
        body{
            background-color: #525252;
        }
        #panel_table {
            padding: 0;
            border-color: #525252;
            border-style: groove;
        }
        .panel-heading{
            background-color: #108A93!important;
            color: white!important;
        }
        .modal-content{
            border-radius: 6px;
        }
        .modal-header{
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;

        }
        #modal_delete .modal-header{
            background-color: #D9534F;
        }
        #modal_edit .modal-header{
            background-color: #428BCA;
        }
        .modal-header .modal-title{
            text-align: center;
            color: white;
        }
        thead th{
            text-align: center;
        }
        #btn_edit_advanced{
            text-align: center!important;
        }
        /*CSS cho button selector-bootstrap*/
        .bootstrap-select >button {
            background-color: white;!important;
            font-size: 14px;
        }
        /*Khi đưa con trỏ vào button selector thì sẽ như nào*/
        .bootstrap-select >button:hover{
            background-color: whitesmoke;

        }
        /*Khi click vào button selector thì sẽ như nào*/
        .open>.dropdown-toggle.btn-default{
            background: whitesmoke!important;

        }
        /*Text cho option*/
        .text {
            font-size: 14px;
        }
        /*Biểu tượng selected bên option*/
        .check-mark{
            color: green;
        }
    </style>
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo e(asset("lib/AdminLTE/plugins/iCheck/square/blue.css")); ?>">
    <link rel="stylesheet" href="<?php echo e(asset("lib/AdminLTE/plugins/iCheck/all.css")); ?>">


<?php $__env->stopSection(); ?>
<?php $__env->startSection('staff','active'); ?>
<?php $__env->startSection('list_staff','active'); ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <!--content-->
            <div id="panel_table" class="panel panel-info col-md-11" >
                <div class="panel-heading text-center">Danh Sách Nhân Viên</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tb_staff" class=" table table-hover">
                            <thead>
                            <th style="display: block; align-items: center!important;">
                                <div class="text-center" style="display: block; align-items: center!important; margin-left: 20px">
                                    <input  type="checkbox" id="cb_all" class="cb_all hide text-center" title="">
                                </div>
                            </th>
                            <th>Avatar</th>
                            <th>Họ tên</th>
                            <th>Email</th>
                            <th>Cấp độ</th>
                            <th>Lĩnh vực</th>
                            <th>Ngân Hàng</th>
                            <th>Phone</th>
                            
                            </thead>
                        </table>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <button type="button" name="btn_edit" id="btn_edit" class="form-control btn btn-primary ">Chỉnh sửa</button>
                        </div>
                        <div class="form-group col-md-4 ">
                            <button type="button" name="btn_change_password" id="btn_change_password" class=" form-control btn" style="background-color: #00ca6d; color: white;">Đổi mật khẩu</button>
                        </div>
                        <div class="form-group col-md-4 ">
                            <button type="button" name="btn_delete" id="btn_delete" class=" form-control btn btn-danger">Xóa</button>
                        </div>

                    </div>
                </div>
            </div> <!--End content-->
            <!--modal edit-->
            <div id="modal_edit" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Thay đổi thông tin</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="alert alert-success message">
                                    <strong></strong>
                                    <span></span>
                                </div>
                                <form id="form-simple" style="display: block;">
                                    <div class=" text-center col-md-3" id="change">
                                        <img style="height: 204px; min-width: 190px" src="" id="edit_avatar" class="img-thumbnail" alt="User Image">
                                    </div>
                                    <div class="form-group  col-md-9 form_content">
                                        <div class="form-group col-md-6">
                                            <label for="edit_fullname">Họ tên:</label>
                                            <input type="text" class="form-control" name="edit_fullname" id="edit_fullname">
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <div >
                                                    <label>Level</label>
                                                    <select class="form-control" id="slt_level" name="slt_level" title="">
                                                        <option value="st_consult">Tư Vấn Viên</option>
                                                        <option value="st_news">Tin Tức Viên</option>
                                                        <option value="admin">Admin</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="usr">Ngân Hàng đang làm việc: </label>
                                            <select class="form-control" id="slt_bank" name="slt_bank" title="">
                                                <option selected disabled class="">Lựa chọn 1 ngân hàng</option>
                                                <?php $__currentLoopData = $bank_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bank): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                    <option value="<?php echo e($bank->id); ?>"><?php echo e($bank->fullname); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <div>
                                                    <label>Lĩnh vực hoạt động</label>
                                                    <select class="form-control selectpicker" multiple name="slt_category_consult" id="slt_category_consult" title="Lựa chọn lĩnh vực tư vấn">
                                                        <?php $__currentLoopData = $category_consult_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category_consult): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                            <option value="<?php echo e($category_consult->id); ?>"><?php echo e($category_consult->name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="edit_number_phone">Số Điện Thoại:</label>
                                            <input type="text" name="edit_number_phone" class="form-control" id="edit_number_phone">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="edit_cmnd">CMND:</label>
                                            <input type="text" name="edit_cmnd" class="form-control" id="edit_cmnd" >
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn_edit_modal">Chỉnh sửa</button>
                            <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>

                </div>
            </div><!--End modal edit-->
            <!--modal error-->
            <div id="modal_error" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #D9534F">
                            <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-times-circle fa-3x"></i></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class=" text-center"><h4 id="modal_error_content" style="color: #ad001b;"></h4></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>

                </div>
            </div><!--End modal error-->
            <div id="modal_change" class="modal fade"  role="dialog">
                <div class="modal-dialog modal-md">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #00ca6d">
                            <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                            <div class="modal-title">
                                <h2 class="text-center" style="color: white">Đổi mật khẩu</h2>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="row" id="modal-change-content">
                                <div class="alert alert-success message">
                                    <strong></strong>
                                    <span></span>
                                </div>
                                <div class="col-md-12 ">
                                    <form id="form-change" class="col-md-12">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <div class="form-group ">
                                            <label for="edit_email">Email:</label>
                                            <input type="email" class="form-control" name="modal_email" id="modal_email" disabled>
                                        </div>
                                        <div class="form-group  ">
                                            <label for="password">Nhập Mật Khẩu Mới:</label>
                                            <input type="password" class="form-control" name="password" id="password">
                                        </div>
                                        <div class=" form-group  ">
                                            <label for="password_confirmation">Nhập lại Mật khẩu:</label>
                                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btn_change_modal" class="btn btn-primary left">Thay đổi</button>
                            <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>

                </div>
            </div><!--End modal edit-->
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="<?php echo e(asset("lib/AdminLTE/plugins/iCheck/icheck.min.js")); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
    <script type="text/javascript">
        //UI CheckBox with iCheck
        $('input[type="checkbox"].cb_all').iCheck({
            checkboxClass: 'icheckbox_minimal-blue'
        });

        $(document).ready(function () {
            var info_id;
            var staff_email='';
            var info_fullname = '';
            var info_level = '';
            var info_bank_id = '';
            var info_category_consult_id_array = '';
            var info_number_phone = '';
            var info_avatar = '';

            //SelectorPicker for Category-Consult
            $('.selectpicker').selectpicker({
                style: 'btn-default',
                size: 4
            });


            //Datatables
            var tb_staff =  $('#tb_staff');
            var table = tb_staff.DataTable({
                "order": [],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Vietnamese.json"
                },
                "processing": true,
                "serverSide": true,
                "ajax":"<?php echo e(route('controller/admin/staff-list')); ?>",
                "drawCallback": function() {
                    $('input[type="checkbox"].cb_select').iCheck({
                        checkboxClass: 'icheckbox_minimal-blue'
                    });
                    $('#cb_all').iCheck('uncheck');
                },
                "columns": [
                    {data: 'cb_select', orderable:false, "searchable": false},
                    {data: 'avatar', orderable:false, "searchable": false},
                    {data: 'fullname'},
                    {data: 'email'},
                    {data: 'level'},
                    {data: 'category_consult', orderable:false, "searchable": false},
                    {data: 'bank_id',"searchable": false},
                    {data: 'number_phone', orderable:false, "searchable": true}
                ]});


            //Event CheckBox Checked and UnChecked
            var cb_all =  $('#cb_all');
            cb_all.on('ifChecked', function(){
                $('.cb_select').iCheck('check');
            });
            cb_all.on('ifUnchecked', function(){
                $('.cb_select').iCheck('uncheck');
            });
            tb_staff.on('ifChecked','.cb_select', function(){
                 $(this).parents('tr') .addClass('slted')
            });
            tb_staff.on('ifUnchecked','.cb_select', function(){
                $(this).parents('tr') .removeClass('slted')
            });

            //Event when click button Delete
            $('#btn_delete').on('click',function () {
                var arr_email = [];
                var temp_email ='';
                //Check length Row item selected
                if(table.rows('.slted').data().length >0){
                    for (var i = 0; i <table.rows('.slted').data().length ; i++) {
                        temp_email = $(table.rows('.slted').data()[i]['email']).html();
                        arr_email[i] = temp_email;
                    }
                    $.ajax({
                        url: "<?php echo e(route('controller/admin/staff-remove')); ?>",
                        data:{
                            'array_email': arr_email
                        },
                        beforeSend: function () {

                        },
                        dataType: "text",
                        type: "get",
                        success: function (result) {
                            if(result == "Removed"){
                                table.rows('.slted').remove().draw();
                            }else {
                                showModalError("Lỗi mạng");
                            }
                        }
                    });
                }else{

                    showModalError("Vui lòng lựa chọn các mục cần xóa!");
                }
            });


            //Event when click button Edit
            $('#btn_edit').on('click',function () {

                $('#modal_edit .modal-dialog .modal-content .modal-body .row .message').addClass('hide');
                if(table.rows('.slted').data().length == 0) {
                    showModalError('Không có mục nào được chọn');
                }else if(table.rows('.slted').data().length >1){
                    showModalError("Tính năng chỉnh sửa nhiều mục chưa được phát triển! <br> Vui lòng chỉ lựa chọn 1 mục.");
                }else{
                    staff_email = $(table.row('.slted').data()['email']).html();
                    $.ajax({
                        url: "<?php echo e(route('controller/admin/staff-info')); ?>",
                        data:{
                            'email': staff_email
                        },
                        beforeSend: function () {

                        },
                        dataType: "json",
                        type: "get",
                        success: function (result) {
                            info_id = result['id'];
                            console.log(info_id);
                            info_fullname = result['fullname'];
                            info_level = result['level'];
                            info_bank_id = result['bank_id'];
                            info_category_consult_id_array = result['category_consult_id_array'];
                            info_number_phone = result['number_phone'];
                            info_avatar = result['avatar'];

                            $('#edit_fullname').val(info_fullname);
                            $('#slt_level').val(info_level);
                            $('#slt_bank').val(info_bank_id);
                            $('.selectpicker').selectpicker('val',info_category_consult_id_array );
                            $('#edit_number_phone').val(info_number_phone);
                            $('#edit_avatar').attr('src',info_avatar);
                            $('#modal_edit').modal('show');
                        }
                    });
                }
            });

            //Event when click button Edit in Modal
            $('#btn_edit_modal').on('click',function () {
                var modal_fullname = $.trim($('#edit_fullname').val());
                var modal_level = $('#slt_level').val();
                var modal_bank_id = $('#slt_bank').val();
                var modal_category_consult_id_array = $('.selectpicker').val();
                var modal_number_phone= $.trim($('#edit_number_phone').val().replace(/\s/g, ''));
                var modal_avatar = $('#edit_avatar').attr('src');
                var is_same = false;
                console.log(modal_category_consult_id_array);
                if(modal_category_consult_id_array != null){
                    if(modal_category_consult_id_array.length>0){
                        is_same = (info_category_consult_id_array.length == modal_category_consult_id_array.length) && info_category_consult_id_array.every(function(element, index) {
                                    return element == modal_category_consult_id_array[index];
                                });
                    }
                }

                if((info_fullname == modal_fullname) && (info_level == modal_level) && (info_bank_id == modal_bank_id) && (is_same==true)  && (info_number_phone == modal_number_phone) && (info_avatar == modal_avatar)){
                    showError('#modal_edit','<li>Bạn chưa thay đổi giá trị nào!</li>');
                }else{

                    $.ajax({
                        url: "<?php echo e(route('controller/admin/staff-edit')); ?>",
                        data:{
                            'id':info_id,
                            'email': staff_email,
                            'fullname': modal_fullname,
                            'level': modal_level,
                            'bank_id': modal_bank_id,
                            'category_consult_id_array': modal_category_consult_id_array,
                            'number_phone': modal_number_phone,
                            'avatar': modal_avatar
                        },
                        beforeSend: function () {

                        },
                        dataType: "json",
                        type: "get",
                        success: function (result) {
                            if(result['status'] == "Success"){
                                showSuccess('#modal_edit','Success! Thay đổi thành công!');
                                setTimeout(function () {
                                    $('#modal_edit').modal('hide');
                                },2000);
                                table.draw();
                            }else {
                                showError('#modal_edit',result['error']);
                            }
                        }
                    });
                }

            });
            $('#btn_change_password').on('click',function () {
                $('#modal_change .modal-dialog .modal-content .modal-body .row .message').addClass('hide');
                if(table.rows('.slted').data().length == 0) {
                    showModalError('Không có mục nào được chọn');
                }else if(table.rows('.slted').data().length >1){
                    showModalError("Tính năng thay đổi mật khẩu nhiều nhân viên chưa được phát triển! <br> Vui lòng chỉ lựa chọn 1 mục.");
                }else{
                    $('#form-change input[type=password]').val('');
                    $('#modal_change').modal('show');
                    staff_email = $(table.row('.slted').data()['email']).html();
                    $('#modal_email').val(staff_email);
                }
            });
            $('#btn_change_modal').on('click',function () {
                var form =$('#form-change');
                var disabled = form.find(':input:disabled').removeAttr('disabled');
                var input = form.serialize();
                disabled.attr('disabled','disabled');
                $.ajax({
                    url: "<?php echo e(route('controller/admin/staff-change')); ?>",
                    data:input,
                    dataType: "json",
                    type: "post",
                    success: function (result) {
                        if(result['status'] == "Success"){
                            showSuccess('#modal_change','Chỉnh sửa thành công!');

                            setTimeout(function () {
                                $('#modal_change').modal('hide');
                            },2000);
                            table.draw();
                        }else {
                            showError('#modal_change',result['error']);
                        }
                    }
                });
            });
            function showError(id_modal,error) {
                var message = $(''+id_modal +' .modal-dialog .modal-content .modal-body .row .message');
                message.addClass('alert-danger');
                message.removeClass('alert-success');
                message.slideDown();
                message.removeClass('hide');
                message.find(' strong').text('Warning! Bạn có thể đang gặp các lỗi sau:  ');
                message.find('  span ').html('<ul>'+error+'</ul>');
            }
            function showSuccess(id_modal,success) {
                var message = $(''+id_modal +' .modal-dialog .modal-content .modal-body .row .message');
                message.addClass('alert-success');
                message.slideDown();
                message.removeClass('alert-danger');
                message.removeClass('hide');
                message.find(' strong').text(success);
                message.find('  span ').html('');

            }
            function showModalError(error) {
                var modal_error = $('#modal_error');
                modal_error.modal('show');
                $('#modal_error_content').html(error);
            }
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin.master", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>