<?php $__env->startSection('css'); ?>
    <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
    <!-- polyfiller file to detect and load polyfills -->
    <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
    <script>
        webshims.setOptions('waitReady', false);
        webshims.setOptions('forms-ext', {types: 'date'});
        webshims.polyfill('forms forms-ext');
    </script>
    <style>

        #panel_table {
            padding: 0px;
            border-color: #5CB85C;
            border-style: groove;
        }

        .content {
            margin: 0px;
        }

        #tb_banks {
            width: 100% !important;
        }

        .panel-heading {
            background-color: #5CB85C !important;
            color: white !important;
        }

        .modal-content {
            border-radius: 6px;
        }

        .modal-header {
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;

        }

        #modal_delete .modal-header {
            background-color: #D9534F;
        }

        #modal_edit .modal-header {
            background-color: #428BCA;
        }

        .modal-header .modal-title {
            text-align: center;
            color: white;
        }

        thead th {
            text-align: center;
        }

        td {
            text-align: center;
            vertical-align: middle;
        }

    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content_header'); ?>
    <h1>Danh Sách Khách Hàng</h1>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customer','active'); ?>
<?php $__env->startSection('list_customer','active'); ?>

<?php $__env->startSection('content'); ?>
    <style>
        .datepicker {
            z-index: 1151 !important;
        }
    </style>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                    <h3 class="box-title">Bảng khách hàng</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Họ Tên</th>
                            <th>Số Điện Thoại</th>
                            <th>Email</th>
                            <th>Ngày Sinh</th>
                            <th>Tỉnh</th>
                            <th>Địa chỉ</th>
                            <th>Nghề Nghiệp</th>
                            <th>Hành Động</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                            <tr id="<?php echo e($a->id); ?>">
                                <td><?php echo e($a->id); ?></td>
                                <td><?php echo e($a->fullname); ?>

                                </td>
                                <td><?php echo e($a->number_phone); ?></td>
                                <td><?php echo e($a->email); ?></td>
                                <td><?php echo e($a->birthday); ?></td>
                                <td><?php echo e($a->name); ?></td>
                                <td><?php echo e($a->address); ?></td>
                                <td><?php echo e($a->job); ?></td>
                                <td style="text-align:center;
              vertical-align:middle;"><a id="btndel" class="btndel" title="Xóa" type="button" data-toggle="modal" data-target="#modal_delete"
                                         onclick="delcustomer('<?php echo e($a->id); ?>');">
                                        <i class="fa fa-times-circle fa-2x"></i>
                                    </a><a id="btnEdit" class="btnEdit" onclick="getcustomer('<?php echo e($a->id); ?>');"
                                           data-toggle="modal" data-target="#modal_edit">
                                        <i class="fa fa-pencil-square-o fa-2x"></i>
                                    </a></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <div id="modal_error" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-times-circle fa-3x"></i></h4>
                </div>
                <div class="modal-body">
                    <div style=" text-align:left !important">
                        <h4 class="text-danger">Bạn có thể đang gặp một trong số các lỗi sau:</h4>
                        <div class="list_error">
                            <ul>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default left" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal_delete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Xác thực tác vụ</h4>
                </div>
                <div id="notification" class=" alert  col-md-12 hide">
                    <strong>Success!</strong> <span>Indicates a successful or positive action.</span>
                </div>
                <div class="modal-body text-center" id="delquestion">
                    Bạn Có muốn xóa khách hàng này ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btnxoa">Xóa</button>
                    <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Chỉnh sửa thông tin khách hàng
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div id="notification1" class=" alert  col-md-12 hide">
                        <strong>Success!</strong> <span>Indicates a successful or positive action.</span>
                    </div>
                    <form class="form-horizontal" role="form" id="editform">
                        <input type="hidden" id="id" name="id"/>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="txtHoten">Họ Tên</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="fullname"
                                       id="fullname"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="radios">Giới Tính</label>
                            <div class="col-sm-10">
                                <label class="radio-inline" for="radios-0">
                                    <input type="radio" name="gender" id="radios-0" value="1" checked="checked">Nam
                                </label>
                                <label class="radio-inline" for="radios-1">
                                    <input type="radio" name="gender" id="radios-1" value="0">Nữ
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="txtBirthday">Ngày sinh</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="birthday" name="birthday"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="txtPhone">Số Điện Thoại</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control"
                                       id="phone" name="phone"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="txtEmail">Email</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control"
                                       id="email" name="email"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="txtAddress">Tỉnh/Thành Phố</label>
                            <div class="col-sm-10">
                                <select id="province" name="province" class="form-control">
                                    <?php $__currentLoopData = $provinces; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                        <option value="<?php echo e($a->id); ?>"><?php echo e($a->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="txtAddress">Địa chỉ</label>
                            <div class="col-sm-10">
      <textarea class="form-control"
                id="address" name="address"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"
                                   for="txtJob">Nghề Nghiệp</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control"
                                       id="job" name="job"/>
                            </div>
                        </div>

                    </form>


                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" onclick="editcustomer();" class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>


    <script>

        var url1 = "";

        function getcustomer(id) {
            $('#notification1').addClass('hide');
            $.ajax({
                url: "<?php echo e(route('controller/admin/customers-getInfo')); ?>",
                data: {
                    'id': id
                },
                dataType: "json",
                type: "get",
                success: function (result) {
                    var id = result['id'];
                    var name = result['fullname'];
                    var gender = result['gender'];
                    var email = result['email'];
                    var phone = result['number_phone'];
                    var province = result['province_id'];
                    var birthday1 = result['birthday'];
                    var address = result['address']
                    var job = result['job']
                    if (gender == 0) {
                        $('#radios-1').prop('checked', true);
                    }
                    else {
                        $('#radios-0').prop('checked', true);
                    }
                    $("#id").val(id);
                    $("#fullname").val(name);
                    $("#province").val(province);
                    $("#phone").val(phone);
                    $("#address").val(address);
                    $("#email").val(email);
                    $('#birthday').val(birthday1);
                    $("#job").val(job);
                }
            });

        }
        ;
        function editcustomer() {
            // id = $('#txtID').val();
            // fullname = $("#txtHoten").val();
            // phone = $("#txtPhone").val();
            // address = $("#txtAddress").val();
            // email = $("#txtEmail").val();
            // province = $("#txtProvince").val();
            // provincename = $("#txtProvince option:selected").text();
            // job =  $("#txtJob").val();
            // birthday = $("#datepicker").val();
            // gender = $('input[name=gender]:checked', '#editform').val()
            var input = $("form").serialize();
            console.log(input);
            $.ajax({
                url: "<?php echo e(route('controller/admin/customers-edit')); ?>",
                data: input,
                dataType: "json",
                type: "get",
                success: function (result) {
                    if (result['status'] == 'Success') {
                        $('#notification1').removeClass('hide');
                        $('#notification1').removeClass('alert-danger');
                        $('#notification1').addClass('alert-success');
                        $('#notification1 strong').text('Success:')
                        $('#notification1 span').text('Dữ liệu đã được thay đổi thành công!!');

                        setTimeout(function () {
                            $('#modal_edit').modal('hide');
                            //table.draw(true);
                        }, 1000);
                        id = $('#id').val();
                        $('#' + id + " td:eq(1)").text($('#fullname').val());
                        $('#' + id + " td:eq(2)").text($('#phone').val());
                        $('#' + id + " td:eq(3)").text($('#email').val());
                        $('#' + id + " td:eq(4)").text($('#birthday').val());

                        $('#' + id + " td:eq(5)").text($("#province option:selected").text());
                        $('#' + id + " td:eq(6)").text($('#address').val());
                        $('#' + id + " td:eq(7)").text($('#job').val());

                    }

                    else {
                        $('#notification1').removeClass('hide');
                        $('#notification1').addClass('alert-danger');
                        $('#notification1').removeClass('alert-success');
                        $('#notification1 strong').html('Error:')
                        $('#notification1 span').html('<ul>' + result['error'] + '</ul>');


                    }
                }
            });

        }
        function delcustomer(id) {
            $('#btnxoa').click(function () {
                $.ajax({
                    url: 'customer/del/' + id,
                    type: 'post',
                    data: {
                        "_token": "<?php echo e(csrf_token()); ?>"
                    }

                    ,
                    success: function (result) {
                        setTimeout(function () {
                            $('#modal_delete').modal('hide');
                            table.draw(true);
                        }, 1000);
                        swal("Thành công", "Bạn đã xóa thành công", "success")
                        $('#' + id).remove();

                    }
                });
            });

        }
        $(document).ready(function () {
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy'
            });

            $('#table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "info": true,
                "autoWidth": false,
                "columnDefs": [
                    {"orderable": false, "targets": 8}
                ]
            });
            $(".btndel").attr("data-target", "#modal_delete")
            $(".btnEdit").attr("data-target", "#modal_edit")

        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>