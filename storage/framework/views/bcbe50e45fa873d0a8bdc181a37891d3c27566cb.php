<?php $__env->startSection('title',' Website tài chính - Tỉ giá hối đoái'); ?>
<?php $__env->startSection('meta'); ?>
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<style type="text/css">
	td{
		text-align: center
	}
	th{
		text-align: center
	}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-8">
		<div class="postconent nobottommargin clearfix">
			<h3 style="border-bottom: 1px solid #dedede; text-align: center;">Tỷ giá hối đoái cập nhật ngày hôm nay <?php echo e(date("d/m/Y")); ?> - <?php echo e(date("H:i")); ?> </h3>

			<div class="row">
				<div class="col-md-4 col-md-offset-2">
					<label>Chọn đồng tiền</label>
					<select class="form-control" id='typemoney'>
						<?php $__currentLoopData = $catemoney; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
						<option value="<?php echo e($a->id); ?>"><?php echo e($a->name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped" id='tb'>
						<thead>
							<tr>
								<th>Ngân Hàng</th>
								<th>Mua Tiền Mặt</th>
								<th>Mua Chuyển Khoản</th>
								<th>Bán Chuyến Khoản</th>
							</tr>
						</thead>
						<tbody id='idbody'>
							<?php $__currentLoopData = $exchange; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ex): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
							<tr>
								<td><?php echo e($ex->name); ?></td>
								<td><?php echo e($ex->muatienmat); ?></td>
								<td><?php echo e($ex->muack); ?></td>
								<td><?php echo e($ex->banck); ?></td>
							</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="sidebar nobottommargin col_last clearfix" style="border: 1px solid #E2E2E2; padding: 20px">
			<h2 style="border-bottom: 1px solid #dedede;">Công cụ tra cứu</h2>
			<div class="row">
				<div class="col-md-6">
					<a href="/Finance/tools/vay-von">
						<div>
							<img src="http://bietthubiennhatrang.net/wp-content/uploads/2016/03/icon-biet-thu.gif" width="80px" height="80px">
							<p style="margin-left: 15px">Vay vốn</p>
						</div>
					</a>
				</div>
				<div class="col-md-6">
					<a href="/Finance/tools/gui-tiet-kiem">
						<div>
							<img src="http://ieltstolinh.vn/wp-content/uploads/2016/01/Icon-02.png" width="80px" height="80px">
							<p>Gửi tiết kiệm</p>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<a href="/Finance/gia-vang">
						<div>
							<img src="http://lh3.googleusercontent.com/EKKBTvcKg-AvzdUykGrtGfDpf2TbHMcqzIVm9iR2SRgShko7EMl5TrYwu7NJiprG9FkP=w300" width="80px" height="80px">
							<p style="margin-left: 15px">Giá vàng</p>
						</div>
					</a>
				</div>
				<div class="col-md-6">
					<a href="/Finance/ti-gia-hoi-doai">
						<div>
							<img src="http://bankingonline.vn/images/dola.png" width="80px" height="80px">
							<p>Tỉ giá hối đoái</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
	$("#typemoney").change(function()
	{
		$.ajax({
			url: "<?php echo e(route('guest/getexchange')); ?>",
			data:{
				'_token' : $('meta[name="csrf-token"]').attr('content'),
				'id': $("#typemoney").val()
			},
			dataType: "json",
			type: "post",
			success: function (result) {
				console.log(result.length)  ;
				
				$('#idbody').remove();
				var tboyd = "<tbody id='idbody'>";
				for (i=0; i<8;i++)
				{
					console.log(result[i].name) ;
					tboyd = tboyd+"<tr><td>"+result[i].name+"</td><td>"+result[i].muatienmat+"</td><td>"+result[i].muack+"</td><td>"+result[i].banck+"</td></tr>"
				}
				tboyd = tboyd + "</tbody>"
				$('#tb').append(tboyd);

			}
		})
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>