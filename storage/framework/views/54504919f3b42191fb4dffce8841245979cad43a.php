<?php $__env->startSection('css'); ?>
    <style>
        #panel_table {
            padding: 0px;
            border-color: #5CB85C;
            border-style: groove;
        }
        .content{
            margin: 0px;
        }
        #tb_scales{
            width: 100%!important;
        }
        .panel-heading{
            background-color: #5CB85C!important;
            color: white!important;
        }
        .modal-content{
            border-radius: 6px;
        }
        .modal-header{
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;

        }
        #modal_delete .modal-header{
            background-color: #D9534F;
        }
        #modal_edit .modal-header{
            background-color: #428BCA;
        }
        .modal-header .modal-title{
            text-align: center;
            color: white;
        }
        thead th{
            text-align: center;
        }
        #edit_logo{
            opacity: 1.0;
            height: 133px;
            width: 300px;
        }
        #edit_logo:hover {

            opacity: 0.5;
            filter: alpha(opacity=50); /* For IE8 and earlier */
        }
        #edit_advanced{
            text-align: left!important;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scales','active'); ?>
<?php $__env->startSection('scales-list','active'); ?>
<?php $__env->startSection('content'); ?>
   
   <div class="container">
       <div class="row">
           <!--content-->
           <div id="panel_table" class="panel panel-info col-md-9 col-md-offset-1">
               <div class="panel-heading text-center">Danh Sách Cân</div>
               <div class="panel-body">

                   <table id="tb_scales" class="table table-bordered table-hover">
                       <thead>
                       <th>ID</th>
                       <th>Tên Cân </th>
                       <th>Model</th>
                       <th>Hãng SX</th>
                       <th>Ngày tạo</th>
                       <?php if($staff_info->level == 'admin'): ?>
                       <th>Chỉnh sửa</th>
                       <?php else: ?>
                           <th>Chi tiết</th>
                           <?php endif; ?>
                       <?php if($staff_info->level == 'admin'): ?>
                       <th>Xóa</th>
                       <?php endif; ?>
                       </thead>
                   </table>
               </div>
           </div> <!--End content-->
           <!--modal delete-->
           <div id="modal_delete" class="modal fade" role="dialog">
               <div class="modal-dialog">
                   <!-- Modal content-->
                   <div class="modal-content">
                       <div class="modal-header">
                           <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Xác thực tác vụ</h4>
                       </div>
                       <div class="modal-body text-center">

                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-danger" id="btn_remove">Xóa</button>
                           <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>
                       </div>
                   </div>

               </div>
           </div>  <!--End modal delete-->
           <!--modal edit-->
           <div id="modal_edit" class="modal fade" role="dialog">
               <div class="modal-dialog modal-lg">
                   <!-- Modal content-->
                   <div class="modal-content">
                       <div class="modal-header">
                           <button id="icon_close" type="button" class="close" data-dismiss="modal">&times;</button>
                           <h4 class="modal-title">Chi Tiết Cân</h4>
                       </div>
                       <div class="modal-body">
                           <div class="row">
                               <form>
                                   <div id="notification" class="alert alert-success hide">
                                       <strong></strong>
                                       <span></span>
                                   </div>
                                   <div class=" text-center col-md-4" id="change">

                                   
                                   </div>
                                   <div class="form-group  col-md-12 form_content">
                                       <div class="form-group col-md-12">
                                           <label for="usr">Tên Cân:</label>
                                           <input type="text" class="form-control" id="edit_name">
                                       </div>
                                       <div class="form-group col-md-4">
                                           <label for="usr">Model: </label>
                                           <input type="text" class="form-control" id="edit_model">
                                       </div>
                                       <div class="form-group col-md-8">
                                           <label for="usr">Hãng sản xuất:</label>
                                           <input type="text" class="form-control" id="edit_brand">
                                       </div>
                                   </div>
                                   <div class="form-group" id="edit_advanced_form" style="margin: 0 15px 0 15px;">
                                       <label>Cách Hiệu Chuẩn</label>
                                       <textarea class="form-control" id="edt_intro" name="intro"
                                                 ></textarea>
                                   </div>
                               </form>

                           </div>
                       </div>
                       <div class="modal-footer">
                           <?php if($staff_info->level == 'admin'): ?>
                               <button type="button" class="btn btn-primary"  id="btn_edit">Chỉnh sửa</button>
                           <?php endif; ?>
                               <button type="button" class="btn btn-default left" data-dismiss="modal">Đóng</button>


                       </div>
                   </div>

               </div>
           </div><!--End modal edit-->
       </div>
   </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            var id='';
            var name = '';
            var model = '';
            var brand = '';
            var logo = '';
            var intro = '';

            CKEDITOR.replace('intro', {
                filebrowserImageBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Images',
                filebrowserFlashBrowseUrl: '../lib/AdminLTE/plugins/ckfinder/ckfinder.html?Type=Flash',
                filebrowserImageUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '../lib/AdminLTE/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

            });

if (level == 'admin') {
    var table = $('#tb_scales').DataTable({
        scrollY: 300,
        scrollCollapse: true,
        "processing": true,
        "serverSide": true,
        "ajax": '<?php echo e(route('controller/admin/scales-list')); ?>',
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Vietnamese.json"
        },
        "columns": [
            {data: 'id'},
            {data: 'name'},
            {data: 'model'},
            {data: 'brand'},
            {data: 'created_at'},
            {data: 'edit', orderable: false, "searchable": false},
            {data: 'delete', orderable: false, "searchable": false}
        ]
    });
}else {
    var table = $('#tb_scales').DataTable({
        scrollY: 300,
        scrollCollapse: true,
        "processing": true,
        "serverSide": true,
        "ajax": '<?php echo e(route('controller/admin/scales-list')); ?>',
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Vietnamese.json"
        },
        "columns": [
            {data: 'id'},
            {data: 'name'},
            {data: 'model'},
            {data: 'brand'},
            {data: 'created_at'},
            {data: 'edit', orderable: false, "searchable": false},
           
        ]
    });
}
            /*Delete Bank*/
            $('#tb_scales').on('click','.icon_delete',function () {
                var fullname = $(table.row( $(this).parents('tr') ).data()['fullname']).text();
                var id = $(table.row( $(this).parents('tr') ).data()['id']).text();
                $('#modal_delete .modal-body').html(
                        "<p>Bạn muốn xóa</p>" +
                "<strong>"+fullname+"</strong>");
                $('#btn_remove').removeClass('hide')
                $('#modal_delete').modal('show');

                $('#btn_remove').on('click',function () {
                    $.ajax({
                        url: "<?php echo e(route('controller/admin/scales-remove')); ?>",
                        data:{
                            'id': id
                        },
                        beforeSend: function () {
                            $('.modal-body').html('<img src="<?php echo e(asset('public/img/loading.gif')); ?>">');
                        },
                        dataType: "text",
                        type: "get",
                        success: function (result) {
                            if(result == "Removed"){
                                table.row( $(this).parents('tr') ).remove().draw();
                                $('.modal-body').html(
                                        "<strong style='font-size: 1.5em' class='text-success'>Đã xóa <i class='glyphicon glyphicon-ok'></i></strong>");
                                $('#btn_remove').addClass('hide')
                            }else {
                                $('.modal-body').html("<strong>Lỗi kết nối, vui lòng thử lại sau!</strong>");
                                $('#modal_delete').modal('show');
                            }
                        }
                    });
                });
            });
            /*Edit Bank*/
            $('#tb_scales').on('click','.icon_edit',function () {
                CKEDITOR.instances['edt_intro'].setData('');
                $('#notification').addClass('hide');
                 id = $(table.row( $(this).parents('tr') ).data()['id']).text();
                $.ajax({
                    url: "<?php echo e(route('controller/admin/scale-getInfo')); ?>",
                    data:{
                        'id': id
                    },
                    dataType: "json",
                    type: "get",
                    success: function (result) {
                         name = result['name'];
                         model = result['model'];
                         brand = result['brand'];
                         intro = result['cachhieuchuan'];
                        $('#edit_name').val(name);
                        $('#edit_model').val(model);
                        $('#edit_brand').val(brand);
                        if(intro !=''){
                            CKEDITOR.instances['edt_intro'].setData(intro);
                        }
                        $('#modal_edit').modal('show');
                    }
                });

            });
            $('#btn_edit').on('click',function () {
                $('#notification').addClass('hide');
                var edit_name = $('#edit_name').val();
                var edit_model = $('#edit_model').val();
                var edit_brand = $('#edit_brand').val();
                var edit_intro =  CKEDITOR.instances['edt_intro'].getData();
                if((name == edit_name)&&(model == edit_model&&(brand == edit_brand)&&(intro == edit_intro))){
                    $('#notification').removeClass('hide');
                    $('#notification').addClass('alert-danger');
                    $('#notification strong').text('Error:')
                    $('#notification span').text('Dữ liệu không có gì thay đổi, vui lòng kiểm tra lại!');
                }else{
                    $.ajax({
                        url: "<?php echo e(route('controller/admin/scales-edit')); ?>",
                        data:{
                            'id':id,
                            'name': edit_name,
                            'model': edit_model,
                            'brand': edit_brand,
                            'intro': edit_intro
                        },
                        dataType: "json",
                        type: "get",
                        success: function (result) {
                            if(result['status'] == "Success"){
                                $('#notification').removeClass('hide');
                                $('#notification').removeClass('alert-danger');
                                $('#notification').addClass('alert-success');
                                $('#notification strong').text('Success:')
                                $('#notification span').text('Dữ liệu đã được thay đổi!!');
                                $('#edit_name').val(name);
                                $('#edit_model').val(model);
                                $('#edit_brand').val(brand);
                                setTimeout(function () {
                                    $('#modal_edit').modal('hide');
                                    table.draw(true);
                                },2000);
                            }else {
                                $('#notification').removeClass('hide');
                                $('#notification').removeClass('alert-success');
                                $('#notification').addClass('alert-danger');
                                $('#notification strong').html('Error:')
                                $('#notification span').html('<ul>'+result['error']+'</ul>');
                            }
                        }
                    });
                }
            });
            $('#edit_advanced').on('click',function () {
                if($('#edit_advanced_form').hasClass('hide')){
                    $('#edit_advanced_form').removeClass('hide');

                }else {
                    $('#edit_advanced_form').addClass('hide');
                }
            });


        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("admin.master", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>