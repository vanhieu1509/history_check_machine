CKEDITOR.plugins.add('timestamp', {
	init: function (editor) {
		editor.addCommand('insertTimestamp', {
			exec: function (editor) {
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth() + 1; //January is 0!
				var yyyy = today.getFullYear();

				if (dd < 10) {
					dd = '0' + dd
				}

				if (mm < 10) {
					mm = '0' + mm
				}

				today = dd + '/' + mm + '/' + yyyy;
				editor.insertHtml('<strong>' + today + '</strong>');
			}
		});
		editor.ui.addButton('Timestamp', {
			label: 'Insert Timestamp',
			command: 'insertTimestamp',
			icon: this.path + 'images/timestamp.png'
		});
	}
});
