<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Scale extends Model
{
    protected $table = 'scales';
    protected $fillable = [
        'model', 'name','brand', 'cachhieuchuan','created_at'
    ];
    public $timestamps = true;

    function staff(){
        return $this->hasMany("App\Staff");
    }

    function getList(){
        $list = Scale::select(['id','name','model','brand','cachhieuchuan','created_at']);
        return $list;
    }
    function getScale(){
        $array = DB::table('scales')->select('id','name')->orderBy('name', 'asc')->get()->toArray();
        return $array;
    }
    function removeScale($id){
        DB::table('scales')->where('id',$id)->delete();
    }
    function addScaleAndGetID($name, $model,$brand,$intro){
        $created_at = $this->getDateCurrent();
        $id = DB::table('scales')->insertGetId (
            [
                'name' => $name,
                'model' => $model,
                'brand' => $brand,
                'cachhieuchuan' => $intro,
                'created_at'=>$created_at
            ]
        );
        return $id;
    }
    function editInfoScale($id,$name,$model,$brand,$cachhieuchuan){
        DB::table('scales')->where('id',$id)->update(['name'=>$name,'model'=>$model,'brand'=>$brand,'cachhieuchuan'=>$cachhieuchuan]);
    }
    function changeLogoPath($id,$path){
        DB::table('scales')->where('id',$id)->update(['logo'=>$path]);
    }
    function getInfoScaleByID($id){
        $scale = DB::table('scales')->select('name','model','brand','cachhieuchuan')->where('id',$id)->first();
        return json_encode($scale);
    }
    function getScaleWithIdAndFullname(){
        $scale_list = DB::table('scales')->select('id','fullname')->get();
        return $scale_list;
    }
    public function getScaleNameById($scale_id){
        $scale_name = DB::table('scales')->select('fullname')->where('id',$scale_id)->get();
        return $scale_name;
    }
    public function getScaleIdbyName($name){
        $scale = DB::table('scales')->select('id','name')->where('name',$name)->first();
        return $scale;
    }
    public function getScaleIdLikeName($name){
        $scale = DB::table('scales')->select('id','model')->where('model','LIKE','%'.$name.'%')->get()->toArray();
        return $scale;
    }
    function getDateCurrent(){
        date_default_timezone_set("Asia/Ho_Chi_Minh");
       
        return date("Y-m-d H:i:s");
    }
    function getLogoScaleByID($id){
        $scale = DB::table('scales')->select('name','logo')->where('id',$id)->first();
        return $scale;
    }


}
