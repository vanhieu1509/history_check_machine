<?php
/**
 * Created by PhpStorm.
 * User: van_hieu
 * Date: 11/14/2018
 * Time: 5:57 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Company extends Model
{
    protected $table = 'company';
    public function getListDemo(){
        $result = DB::table('company')->select(['id', 'name'])->where('disable',0)->orderBy('name')->get();
        return $result;
    }
    public function getMachine($id){
        $result = DB::table('machine')->select(['machine.id', 'machine.name','machine.model','machine.seri','machine.nsx','machine.content'])->where('machine.company_id',$id)->where('machine.disable',0)->orderBy('name')->get()->toArray();
        return $result;
    }
    public function getHistory($id_machine){
        $result = DB::table('machine')->select('*')->where('id',$id_machine)->first();
        return $result;
    }
    function staff(){
        return $this->hasMany("App\Staff");
    }

    function getList(){
        $list = Company::select(['id','name','created_at'])->where('disable',0);
        return $list;
    }
    function getListMachine(){
        $list = DB::table('machine')->join('company', 'machine.company_id', '=', 'company.id')->select(['machine.id','machine.name','company.name as company_name','machine.model','machine.seri','machine.nsx','machine.created_at'])->where('machine.disable',0)->where('company.disable',0);
        return $list;
    }
    function getInfoCompanyByID($id){
        $company = DB::table('company')->select('name','about')->where('id',$id)->where('disable',0)->first();
        return json_encode($company);
    }
    function getInfoMachineByID($id){
        $machine = DB::table('machine')->select(['machine.name','machine.company_id','machine.model','machine.seri','machine.nsx','machine.content'])->where('id',$id)->where('disable',0)->first();
        return json_encode($machine);
    }
    function editInfoCompany($id,$name,$about){
        DB::table('company')->where('id',$id)->update(['name'=>$name,'about'=>$about]);
    }
    function editInfoMachine($id,$name,$model,$seri,$nsx,$about, $user_id = 0){
        $info = json_decode($this->getInfoMachineByID($id),true);
        $update = array();
        $update['machine_id'] = $id;
        if ($info['name'] != $name){
            $update['name_log'] = '['.$info['name'].'] => ['.$name.']';
        }
        if ($info['model'] != $model){
            $update['model_log'] = '['.$info['model'].'] => ['.$model.']';
        }
        if ($info['seri'] != $seri){
            $update['seri_log'] = '['.$info['seri'].'] => ['.$seri.']';
        }
        if ($info['nsx'] != $nsx){
            $update['nsx_log'] = '['.$info['nsx'].'] => ['.$nsx.']';
        }
        if ($info['content'] != $nsx){
            $update['content_log'] = '['.$info['content'].'] => ['.$about.']';
        }
        $created_at = $this->getDateCurrent();
        $update['created_at'] = $created_at;
        $update['user_id'] = $user_id;
        DB::table('operation_log')->insertGetId($update);
        DB::table('machine')->where('id',$id)->update(['name'=>$name,'model'=> $model, 'seri'=> $seri, 'nsx'=> $nsx,'content'=>$about]);
    }
    function removeCompany($id){
        DB::table('company')->where('id',$id)->update(['disable'=>1]);
    }
    function removeMachine($id){
        DB::table('machine')->where('id',$id)->update(['disable'=>1]);
    }
    public function getCompanyIdLikeName($name){
        $company = DB::table('company')->select('id','name')->where('name','LIKE','%'.$name.'%')->get()->toArray();
        return $company;
    }
    public function checkExistCompanyName($name){
        $company = DB::table('company')->select('id','name')->where('name',$name)->where('disable',0)->get()->toArray();
        if ($company) {
            return true;
        }
        return false;
    }
    public function checkExistMachineName($name,$company_id, $model,$seri,$nsx){
        $company = DB::table('machine')->select('id','name')->where('name',$name)->where('company_id',$company_id)->where('model',$model)->where('seri',$seri)->where('nsx',$nsx)->where('disable',0)->get()->toArray();
        if ($company) {
            return true;
        }
        return false;
    }
    function addCompanyAndGetID($name,$intro){
        $created_at = $this->getDateCurrent();
        $id = DB::table('company')->insertGetId (
            [
                'name' => $name,
                'about' => $intro,
                'created_at'=>$created_at
            ]
        );
        return $id;
    }
    function addMachineAndGetID($name,$company_id,$model,$seri,$nsx,$intro){
        $created_at = $this->getDateCurrent();
        $id = DB::table('machine')->insertGetId (
            [
                'name' => $name,
                'company_id' => $company_id,
                'model' => $model,
                'seri' => $seri,
                'nsx' => $nsx,
                'content' => $intro,
                'created_at'=>$created_at
            ]
        );
        return $id;
    }
    function getDateCurrent(){
        date_default_timezone_set("Asia/Ho_Chi_Minh");

        return date("Y-m-d H:i:s");
    }
    function getOperationLogByID($id){
        $machine = DB::table('operation_log')->where('machine_id',$id)->orderBy('created_at', 'desc')->get()->toArray();
        return $machine;
    }
}