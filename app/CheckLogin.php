<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckLogin extends Model
{
    protected $table = 'check_logins';
    protected $fillable = [
        'staff_id'
    ];
    public $timestamps = false;
    function staff(){
        return $this->belongsTo("App\Staff");
    }
}
