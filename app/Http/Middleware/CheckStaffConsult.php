<?php

namespace App\Http\Middleware;

use App\Staff;
use Closure;

class CheckStaffConsult
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->has('email')){
            return redirect('staff/login');
        }
        $level = Staff::getLevelStaffByEmail(session('email')[0])->level;
        if(($level != 'st_consult')){
            return response()->view('errors.staff.404');
        }
        return $next($request);
    }
}
