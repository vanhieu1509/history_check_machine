<?php

namespace App\Http\Middleware;

use App\Staff;
use Closure;

class CheckGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('email')){
            $email = session('email');
            $level = Staff::getLevelStaffByEmail($email)->level;
            if ($level =='admin' || $level =='st_news'){
                return redirect('admin/home');
            }else{
                return redirect('staff/home');
            }
        }
        return $next($request);
    }
}
