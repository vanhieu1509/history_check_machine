<?php

namespace App\Http\Middleware;

use App\Staff;
use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->has('email')){
            return redirect('staff/login');
        }
        $staff = new Staff();
        $staff_info = $staff->getInfoByEmail(session('email')[0]);
        $level = $staff_info->level;
        if($level != 'admin' && $level != 'st_news'){
            return redirect('staff/home');
        }
        $request->staff_info = $staff_info;
        return $next($request);
    }
}
