<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class StaffChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'=>'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ];
    }
    public function messages()
    {
        return [
            'password.required'=>"Vui lòng nhập Password mới",
            'password.min'=>"Password phải lớn hơn 6 ký tự",
            'password_confirmation.required' => "Vui lòng nhập Xác Nhận Password",
            'password_confirmation.min' => " Xác Nhận Password phải lớn hơn 6 ký tự",
            'password.confirmed' => "Password không khớp, vui lòng kiểm tra lại!",
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        $kq = array(
            'status' => 'Failed',
            'error' => ''
        );
        $errors = $validator->errors()->all();
        for($i = 0; $i<count($errors); $i++){
            $kq['error'] = $kq['error'].$errors[$i].'<br>';
        }
        die( json_encode($kq));
    }
}
