<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class StaffEditByStaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'=>'required',
            'number_phone'=>'required|numeric|min:100000000|max:1000000000000',
            'password'=>'min:6|confirmed|required_with:password_confirmation',
            'password_confirmation'=>'min:6',
        ];
    }
    public function messages()
    {
        return [
            'fullname.required'=>'Họ tên Nhân Viên không được để trống',
            'number_phone.required'=>'Số điện thoại không được để trống',
            'number_phone.min'=>'Số điện thoại không hợp lệ',
            'number_phone.max'=>'Số điện thoại không hợp lệ',
            'number_phone.numeric'=>'Sai định dạng Số (không bao gồm chữ)',
            'password.min'=>'Mật khẩu phải lớn hơn 6 ký tự',
            'password.required_with'=>'Vui lòng nhập mật khẩu',
            'password.confirmed'=>'Mật khẩu không khớp',
            'password_confirmation.min'=>'Mật khẩu nhập lại phải lớn hơn 6 ký tự',
        ];
    }
    protected function formatErrors(Validator $validator)
    {
        $kq = array(
            'status' => 'Failed',
            'error' => ''
        );
        $errors = $validator->errors()->all();
        for($i = 0; $i<count($errors); $i++){
            $kq['error'] = $kq['error'].'<li>'.$errors[$i].'</li>';
        }
        die(json_encode($kq));
    }
}
