<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email',
            'password' =>'required|min:6'
        ];
    }
    public function messages()
    {
        return [
            'email.required'=>'Vui lòng nhập địa chỉ Email',
            'email.email'=>'Sai định dạng Email',
            'password.required'=>'Vui lòng nhập Password',
            'password.min'=>'Mật khẩu phải lơn hơn 6 ký tự'
        ];
    }
    protected function formatErrors(Validator $validator)
    {
        $kq = array(
            'status' => 'Failed',
            'error' => ''
        );
        $errors = $validator->errors()->all();
        for($i = 0; $i<count($errors); $i++){
            $kq['error'] = $kq['error'].$errors[$i].'<br>';
        }
        die( json_encode($kq));
    }
}
