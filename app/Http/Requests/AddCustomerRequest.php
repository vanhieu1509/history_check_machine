<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;


class AddCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;// này đổi thành true, làm cm gì thằng kia nói quên rồi
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'=>        'required|min:2',
            'phone'=>    'required',
            'address'=>       'required'
                ];
    }
     public function messages()
    {
        return [
          'fullname.required' => 'Tên viết tắt không được để trống.',
          'fullname.min' => 'Tên viết tắt không nhỏ hơn 2 ký tự.',
          'phone.required' => 'Số điện thoại không được để trống.',
          'address.required' => 'Địa chỉ không được để trống.',
        ];
    }
    protected function formatErrors(Validator $validator)
    {
        $kq = array(
            'status' => 'Failed',
            'error' => ''
        );
        $errors = $validator->errors()->all();
        for($i = 0; $i<count($errors); $i++){
            $kq['error'] = $kq['error'].'<li><i>'.$errors[$i].'</i></li>';
        }
        die( json_encode($kq));
    }

}
