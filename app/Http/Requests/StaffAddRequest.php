<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class StaffAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'=>'required',
            'email'=>'required|unique:staff,email|email',
            'password'=>'required|min:6|confirmed',
            'password_confirmation'=>'required',
            'number_phone'=>'required|numeric|min:100000000|max:1000000000000',
        ];
    }
    public function messages()
    {
        return [
            'fullname.required'=>'Họ tên Nhân Viên không được để trống',
            'email.required'=>'Địa chỉ Email Nhân Viên không được để trống',
            'email.unique'=>'Địa chỉ Email đã được sử dụng',
            'email.email'=>'Định dạng Email chưa thật sự chính xác',
            'password.required'=>'Password không được để trống',
            'password.min'=>'Password ít nhất 6 ký tự',
            'password.confirmed'=>'Xác nhận Password không khớp',
            'password_confirmation.required'=>'Xác nhận Password không được để trống',
            'phone_number.required'=>'Số điện thoại không được để trống',
            'number_phone.min'=>'Số điện thoại không hợp lệ1',
            'number_phone.max'=>'Số điện thoại không hợp lệ',
            'number_phone.numeric'=>'Sai định dạng Số điện thoại',
        ];
    }
    protected function formatErrors(Validator $validator)
    {
        $kq = array(
            'status' => 'Failed',
            'error' => ''
        );
        $errors = $validator->errors()->all();
        for($i = 0; $i<count($errors); $i++){
            $kq['error'] = $kq['error'].'<li>'.$errors[$i].'</li>';
        }
        die(json_encode($kq));
    }
}
