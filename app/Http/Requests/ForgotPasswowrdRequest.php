<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswowrdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email',
        ];
    }
    public function  messages()
    {
        return [
            'email.email' => 'Sai định dạng Email',
            'email.required'=>'Vui lòng nhập địa chỉ Email',
        ];
    }
    protected function formatErrors(Validator $validator)
    {
        $kq = array(
            'status' => 'Failed',
            'error' => ''
        );
        $errors = $validator->errors()->all();
        for($i = 0; $i<count($errors); $i++){
            $kq['error'] = $kq['error'].$errors[$i].'<br>';
        }
        die( json_encode($kq));
    }
}
