<?php

namespace App\Http\Controllers;

use App\CategoryConsult;
use App\Province;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category_Product;
use App\Bank;
use App\Product;
use App\InterestRate;
use App\RateSaving;
use Yangqi\Htmldom\Htmldom;
use Illuminate\Support\Facades\DB;


class InterestRateController extends Controller
{
    function getViewRateSaving(){
        $bank = new Bank();
        $bank_list = $bank->getList()->get();
        $category_product = new Category_Product();
        $category_product_list = $category_product->getCategoryProduct();
        return view('guest.compare.laisuatguitietkiem',['bank_list'=>$bank_list,'category_product_list'=>$category_product_list]);

    }
    function getViewBorrow(){
        $bank = new Bank();
        $bank_list = $bank->getList()->get();
        $category_product = new Category_Product();
        $category_product_list = $category_product->getCategoryProduct();
        return view('guest.compare.laisuatchovay',['bank_list'=>$bank_list,'category_product_list'=>$category_product_list]);
    }

    function getViewRateSavingDetail($slug,$id){
        $ratesaving = new RateSaving;
        $bank = new Bank;
        $ratesavings = $ratesaving->getInfoSavingByID($id);
        $logo = $bank->getLogoBankByID($ratesavings[0]->bank_id)->logo;
        $name = $bank->getLogoBankByID($ratesavings[0]->bank_id)->name;
        return view('guest.guitietkiem_details',['id'=>$id,'ratesavings'=>$ratesavings[0],'logo'=>$logo,'name'=>$name]);
    }
    function getViewBorrowDetail($slug,$id){
        $product = new Product;
        $bank = new Bank;
        $products = $product->getInfoProductByID($id);
        $logo = $bank->getLogoBankByID($products[0]->bank_id)->logo;
        return view('guest.product_details',['id'=>$id,'products'=>$products,'logo'=>$logo]);
    }
    function updateGold()
    {
        $html = new Htmldom('http://giavangvn.org/');
        $table = $html->find('table')[0]->find('tr');
        for ($i = 10; $i <= 13; $i++) {
            $row = $table[$i];
            $name = $row->find('td')[0]->find('strong')[0]->innertext;
            $gia1 = $row->find('td')[1];
            echo $gia1;
            $gia2 = $row->find('td')[2];
            DB::table('gold_price')->where('bank_name', $name)->update(['muavao' => $gia1, 'banra' => $gia2]);
        }
         for ($i = 16; $i <= 17; $i++) {
            $row = $table[$i];
            $name = $row->find('td')[0]->find('strong')[0]->innertext;
            $gia1 = $row->find('td')[1];
            echo $gia1;
            $gia2 = $row->find('td')[2];
            DB::table('gold_price')->where('bank_name', $name)->update(['muavao' => $gia1, 'banra' => $gia2]);
        }
        return 'ok';
    }

    function getViewInterestRate()
    {
        $cate = new Category_Product;
        $bank = new Bank;
        $categoryprduct = $cate->getCategoryProduct();
        $listbank = $bank->getBank();
        $data = array(

            'cateproduct' => $categoryprduct,
            'listbank' => $listbank

        );
        return view('Comparisons.interest_rate')->with($data);
    }

    function findProduct(Request $request)
    {
        $product = new Product;
        $result = $product->findProduct($request->all())->get();
        if (count($result)>0){
            $kq['status']=1;
            foreach ($result as $item){
                $item->bank_slug = str_slug( $item->bank_slug);
            }
            $kq['data'] =$result;
        }else{
            $kq['status']=0;
            $kq['msessage']="Data not available";
        }
        return json_encode($kq);
    }

    function getViewAddRate(Request $request)
    {
        $bank = new Bank;
        $banks = $bank->getBank();
        return view('admin.rate.rate_add', ['banks' => $banks, 'staff_info' => $request->staff_info]);
    }

    function getViewAddRate1(Request $request)
    {
        $rate = new RateSaving;
        $arraydate = $rate->getDateUpdate();
        $donga = $rate->getDateNew(6);
        $anbinh = $rate->getDateNew(5);
        $baca = $rate->getDateNew(7);
        $maritime = $rate->getDateNew(8);
        $nama = $rate->getDateNew(3);
        $hdbank = $rate->getDateNew(9);
        $saigon = $rate->getDateNew(10);
        $exim = $rate->getDateNew(11);
        return view('admin.rate.view_add', ['donga' => $donga, 'anbinh' => $anbinh, 'baca' => $baca, 'maritime' => $maritime, 'nama' => $nama, 'hdbank' => $hdbank, 'saigon' => $saigon, 'exim' => $exim, 'arraydate' => $arraydate, 'staff_info' => $request->staff_info]);
    }

    function getRateBank(Request $request)
    {

        $ratesave = new RateSaving;
        $rate = $ratesave->findByBankID($request->bank_id);
        if ($rate != 'null') {
            return $rate;
        } else {
            $ratesave->bank_id = $request->bank_id;
            $ratesave->sotiengui = 1;
            $ratesave->month1 = 0;
            $ratesave->month3 = 0;
            $ratesave->month6 = 0;
            $ratesave->month12 = 0;
            $ratesave->month24 = 0;
            $ratesave->unlimited = 0;
            $ratesave->contents = '';
            $ratesave->save();
            return 'new bank';
        }
    }

    function getDataMonth(Request $request)
    {
        $rate = new InterestRate;
        $url = $request->url;
        $bank_id = $request->bank_id;
        if ($bank_id == 6) {
            $array = $rate->changeDotToComma($rate->RateDongA($url, 1));
        } else if ($bank_id == 7)
            $array = $rate->RateBacA($url, 1);
        else if ($bank_id == 8)
            $array = $rate->RateMsb($url, 5);
        else if ($bank_id == 3)
            $array = $rate->RateNamA($url, 0);
        else if ($bank_id == 9)
            $array = $rate->RateHD($url, 1);
        else if ($bank_id == 12)
            $array = $rate->RateMB($url, 5);
        else if ($bank_id == 13)
            $array = $rate->RateVib($url, 0);
        else if ($bank_id == 10)
            $array = $rate->changeDotToComma($rate->RateSaigon($url, 0));
        else if ($bank_id == 11)
            $array = $rate->changeDotToComma($rate->RateExim($url, 21));
        else if ($bank_id == 2)
            $array = $rate->RateVietcom($url, 0);
        else if ($bank_id == 14)
            $array = $rate->changeDotToComma($rate->RateViettin($url, 0));
        else if ($bank_id == 4)
            $array = $rate->changeDotToComma($rate->RateBidv($url, 3));
        else if ($bank_id == 16)
            $array = $rate->changeDotToComma($rate->RateAgri($url, 1));
        else $array = $rate->RateAB($url, 0);
        $kq['month1'] = $array[1];
        $kq['month3'] = $array[2];
        $kq['month6'] = $array[3];
        $kq['month12'] = $array[4];
        $kq['month24'] = $array[5];
        return json_encode($kq);
    }

    function getDateUpdate($id)
    {
        // $bank_id= $request->bank_id;
        $rate = new InterestRate;
        $date = $rate->getDateUpdate($id);
        $kq['date'] = $date;
        return $date;
    }

    function updatedate1()
    {
        $html = new Htmldom('http://kinhdoanh.dongabank.com.vn/widget/temp/-/DTSCDongaBankIView_WAR_DTSCDongaBankIERateportlet?/interest/54');
        $day11 = $html->find('table')[0]->find('tr')[0]->find('td')[0]->innertext;
        $html1 = new Htmldom('https://www.abbank.vn/lai-suat-tiet-kiem-vnd-p3132r9.html');
        $day1 = $html1->find('.content-news-detail')[0]->find('p')[0]->innertext;
        $dayy = substr($day1, 138, 10);
        $day12 = str_replace('/', '-', $dayy);
        $html1 = new Htmldom('http://www.baca-bank.vn/chuyen-muc/77/lai-suat-tiet-kiem');
        $day1 = $html1->find('table')[0]->find('tr')[0]->find('td')[1]->find('p')[1]->innertext;
        $dayy = substr($day1, 41, 10);
        $day13 = str_replace('/', '-', $dayy);
        $html1 = new Htmldom('https://www.msb.com.vn/Bieu-phi-va-Lai-suat/1177/3/Lai-suat');
        $day1 = $html1->find('.tab-pane')[0]->find('p')[254]->find('b')[0]->innertext;
        $dayy = substr($day1, 52, 10);
        $day14 = str_replace('/', '-', $dayy);
        $html1 = new Htmldom('http://www.namabank.com.vn/lai-suat/lai-suat-tien-gui-danh-cho-ca-nhan/407/7796');
        $day1 = $html1->find('.answer')[0]->find('p')[0]->find('span')[0]->find('strong')[0]->innertext;
        $dayy = substr($day1, 38, 10);
        $day15 = str_replace('/', '-', $dayy);
        $html1 = new Htmldom('https://www.hdbank.com.vn/?ArticleId=202f528f-ce44-459a-992a-c517f8b673cb');
        $day1 = $html1->find('table')[1]->find('tr')[0]->find('td')[0]->find('span')[0]->find('strong')[0]->innertext;
        $dayy = substr($day1, 86, 10);
        $day16 = str_replace('/', '-', $dayy);
        $html1 = new Htmldom('https://www.saigonbank.com.vn/vi/truy-cap-nhanh/lai-suat/Lai-suat-tien-gui-tiet-kiem');
        $day1 = $html1->find('table')[0]->find('tr')[0]->find('td')[0]->find('strong')[2]->innertext;
        $dayy = '2' . $day1;
        $day17 = str_replace('/', '-', $dayy);
        $html1 = new Htmldom('https://www.eximbank.com.vn/home/Static/cn_tietkiem_vnd.aspx');
        $day1 = $html1->find('table')[19]->find('tr')[1]->find('td')[0]->find('span')[0]->innertext;
        $dayy = substr($day1, 24, 22);
        $day18 = str_replace(array(' tháng ', ' năm '), array('-', '-'), $dayy);
        $rate = new RateSaving;
        $rate->UpdateDate(11, $day18);
        $rate->UpdateDate(6, $day11);
        $rate->UpdateDate(5, $day12);
        $rate->UpdateDate(7, $day13);
        $rate->UpdateDate(8, $day14);
        $rate->UpdateDate(3, $day15);
        $rate->UpdateDate(9, $day16);
        $rate->UpdateDate(10, $day17);

        return 'ok';

    }

    function updateRate(Request $request)
    {
        $rate = new RateSaving;
        $bank_id = $request->bank_id;
        $sotiengui = 1;
        $month1 = $request->month1;
        $month3 = $request->month3;
        $month6 = $request->month6;
        $month12 = $request->month12;
        $month24 = $request->month24;
        $unlimited = $request->unlimited;
        $contents = $request->contents;
        $link = $request->link;
        $date_update = '11-11-2011';
        $rate->UpdateRate2($bank_id, $month1, $month3, $month6, $month12, $month24, $unlimited, $link, $date_update, $contents);
        $kq['status'] = "Success";
        return json_encode($kq);


    }

    function updateRate1()
    {

        $rate = new InterestRate;
        $ratesave = new RateSaving;
        $date_update = '11-11-2011';
        $unlimited = 'NA';
        $link = '';
        $arr = $rate->changeDotToComma($rate->RateDongA('http://kinhdoanh.dongabank.com.vn/widget/temp/-/DTSCDongaBankIView_WAR_DTSCDongaBankIERateportlet?/interest/54', 1));
        $ratesave->UpdateRate(6, $arr[1], $arr[2], $arr[3], $arr[4], $arr[5], $arr[6], $link, $date_update);

        $arr = $rate->RateBacA('http://www.baca-bank.vn/chuyen-muc/77/lai-suat-tiet-kiem', 1);
        $month24 = explode('<', $arr[5])[0];
        $ratesave->UpdateRate(7, $arr[1], $arr[2], $arr[3], $arr[4], $month24, $arr[6], $link, $date_update);

        $arr = $rate->RateAB('https://www.abbank.vn/lai-suat-tiet-kiem-vnd-p3132r9.html', 0);
        $ratesave->UpdateRate(5, $arr[1], $arr[2], $arr[3], $arr[4], $arr[5], $arr[6], $link, $date_update);

        $arr = $rate->RateMsb('https://www.msb.com.vn/Bieu-phi-va-Lai-suat/1177/3/Lai-suat', 6);
        $ratesave->UpdateRate(8, trim($arr[1]), trim($arr[2]), trim($arr[3]), trim($arr[4]), trim($arr[5]), trim($arr[6]), $link, $date_update);
        $arr = $rate->RateNamA('http://www.namabank.com.vn/lai-suat/lai-suat-tien-gui-danh-cho-ca-nhan/407/7796', 0);
        $ratesave->UpdateRate(3, trim($arr[1]), trim($arr[2]), trim($arr[3]), trim($arr[4]), trim($arr[5]), trim($arr[6]), $link, $date_update);

        $arr = $rate->RateHD('https://www.hdbank.com.vn/?ArticleId=202f528f-ce44-459a-992a-c517f8b673cb', 1);
        $ratesave->UpdateRate(9, trim($arr[1]), trim($arr[2]), trim($arr[3]), trim($arr[4]), trim($arr[5]), trim($arr[6]), $link, $date_update);

        $arr = $rate->changeDotToComma($rate->RateSaigon('https://www.saigonbank.com.vn/vi/truy-cap-nhanh/lai-suat/Lai-suat-tien-gui-tiet-kiem', 0));
        $ratesave->UpdateRate(10, substr(trim($arr[1]), 0, 4), substr(trim($arr[2]), 0, 4), substr(trim($arr[3]), 0, 4), substr(trim($arr[4]), 0, 4), substr(trim($arr[5]), 0, 4), substr(trim($arr[6]), 0, 4), $link, $date_update);

        $arr = $rate->changeDotToComma($rate->RateExim('https://www.eximbank.com.vn/home/Static/cn_tietkiem_vnd.aspx', 21));
        $ratesave->UpdateRate(11, $arr[1], $arr[2], $arr[3], $arr[4], $arr[5], $arr[6], $link, $date_update);
        return 'ok';

    }

    function updateRate2()
    {
        $rate = new InterestRate;
        $ratesave = new RateSaving;
        $date_update = '11-11-2011';
        $link = "";
        $unlimited = 'NA';
        // $arr = $rate->RateMB('https://www.mbbank.com.vn/congcu/Lists/LaiSuat/lai-suat.aspx',5);
        // $ratesave->UpdateRate(12,substr($arr[1],0,3),substr($arr[2],0,3),substr($arr[3],0,3),substr($arr[4],0,3),substr($arr[5],0,3),substr($arr[6],0,3),$date_update);

        $arr = $rate->RateVib('https://vib.com.vn/wps/portal/vn/product-landing/tai-khoan-ngan-hang/tien-gui-tiet-kiem-co-ky-han', 0);
        $ratesave->UpdateRate(13, $arr[1], $arr[2], $arr[3], $arr[4], $arr[5], $arr[6], $link, $date_update);

        $arr = $rate->RateVietcom('https://www.vietcombank.com.vn/InterestRates', 0);
        $ratesave->UpdateRate(2, substr($arr[1], 0, 4), substr($arr[2], 0, 4), substr($arr[3], 0, 4), substr($arr[4], 0, 4), substr($arr[5], 0, 4), substr($arr[6], 0, 4), $link, $date_update);

        $arr = $rate->changeDotToComma($rate->RateViettin('https://www.vietinbank.vn/web/home/vn/lai-suat/', 0));
        $ratesave->UpdateRate(14, substr($arr[1], 0, 4), substr($arr[2], 0, 4), substr($arr[3], 0, 4), substr($arr[4], 0, 4), substr($arr[5], 0, 4), substr($arr[6], 0, 4), $link, $date_update);

// ngan hang bidv table thay doi moi lan reload
        // $arr = $rate->changeDotToComma($rate->RateBidv('http://bidv.com.vn/Tra-cuu-lai-suat.aspx',3));
        // $ratesave->UpdateRate(4,substr($arr[1],0,3),substr($arr[2],0,3),substr($arr[3],0,3),substr($arr[4],0,3),substr($arr[5],0,3),$unlimited,$date_update);

        $arr = $rate->changeDotToComma($rate->RateAgri('http://sgd-agribank.com.vn/lai-suat.html', 1));
        $ratesave->UpdateRate(16, $arr[1], $arr[2], $arr[3], $arr[4], $arr[5], $arr[6], $link, $date_update);
        return 'ok';

    }

    function findRateSaving(Request $request){
        $month = $request->thoihan;
        $money = $request->vaytoida;
        $rate_saving = new RateSaving();
        $rate_saving_list_search = $rate_saving->findRateSaving($money,$month)->get();
       foreach ($rate_saving_list_search as $value) {
           $value->bank_slug = str_slug($value->bank_slug);
       }
    
        // dd($rate_saving_list_search);
       if(count($rate_saving_list_search)>0){
           $result['status']=1;
           $result['data']=$rate_saving_list_search;
       }else{
           $result['status']=0;
           $result['message']="Data not Available";
       }
       return json_encode($result);
    }
}
