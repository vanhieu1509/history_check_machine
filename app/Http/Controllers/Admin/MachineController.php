<?php
/**
 * Created by PhpStorm.
 * User: Hung
 * Date: 10/4/2016
 * Time: 2:28 PM
 */

namespace App\Http\Controllers\Admin;


use App\Scale;
use App\Company;
use App\Staff;

use App\Http\Controllers\Controller;
use App\Http\Requests\MachineAddRequest;
use App\Http\Requests\CompanyAddRequest;
use App\Http\Requests\MachineEditRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class MachineController extends Controller
{
    /*Get view for http://localhost/Finance/admin/scalelist */
    function getViewListCompany(Request $request){
        return view('admin.company.company_list')->with(['staff_info'=>$request->staff_info]);
    }
    function getViewListMachine(Request $request){
        $company  = new Company();
        $list = $company->getListDemo();
        return view('admin.machine.machine_list')->with(['staff_info'=>$request->staff_info,'list'=>$list]);
    }
    /*Get scales list for http://localhost/Finance/admin/scales/scalelist */
    function getListCompany(){
        $scales = new Company();
        $scale_list = $scales->getList();
//        var_dump($scale_list);
        return Datatables::of($scale_list)
            //Thêm Column edit
            ->addColumn('edit',function (){
                return "<div style=\"display: block;\" class=\"text-center\">
                        <button type=\"button\" class=\"btn btn-link icon_edit\">
                             <i class=\"fa fa-gear \"></i>
                        </button>
                   </div>";
            })
            //Thêm Column delete
            ->addColumn('delete',function (){
                return  "<div style=\"display: block;\" class=\"text-center\">
                        <button type=\"button\" class=\"btn btn-link icon_delete\">
                             <i style=\"color: red;\" class=\"fa fa-remove \" ></i>
                        </button>
                   </div>";
            })
            //Edit Column
            ->editColumn('id', '<div class=\'text-center\'>{{$id}}</div>')
            ->editColumn('name', '<div class=\'text-center\'>{{$name}}</div>')
            ->editColumn('created_at', '<div class=\'text-center\'>{{$created_at}}</div>')
            ->make(true);
    }
    function getListMachine(){
        $machine = new Company();
        $machine_list = $machine->getListMachine();
        return Datatables::of($machine_list)
            //Thêm Column edit
            ->addColumn('edit',function (){
                return "<div style=\"display: block;\" class=\"text-center\">
                        <button type=\"button\" class=\"btn btn-link icon_edit\">
                             <i class=\"fa fa-gear \"></i>
                        </button>
                   </div>";
            })
            //Thêm Column delete
            ->addColumn('delete',function (){
                return  "<div style=\"display: block;\" class=\"text-center\">
                        <button type=\"button\" class=\"btn btn-link icon_delete\">
                             <i style=\"color: red;\" class=\"fa fa-remove \" ></i>
                        </button>
                   </div>";
            })
            //Edit Column
            ->editColumn('id', '<div class=\'text-center\'>{{$id}}</div>')
            ->editColumn('name', '<div class=\'text-center\'>{{$name}}</div>')
            ->editColumn('created_at', '<div class=\'text-center\'>{{$created_at}}</div>')
            ->make(true);
    }
    /*Remove Scale in http://localhost/Finance/admin/scales/scalelist*/
    function removeCompany(Request $request){
        $id = $request->id;
        $company = new Company();
        $company->removeCompany($id);
        Storage::delete('public/avatar/avatar'.$id);
        return "Removed";
    }
    function removeMachine(Request $request){
        $id = $request->id;
        $machine = new Company();
        $machine->removeMachine($id);
        Storage::delete('public/avatar/avatar'.$id);
        return "Removed";
    }
    /*Get Infomation about Machine to load Machine List Table*/
    function getInfoMachine(Request $request){
        $id = $request->id;
        $machine = new Company();
        $machine_info = $machine->getInfoMachineByID($id);
        return $machine_info;
    }
    function getInfoCompany(Request $request){
        $id = $request->id;
        $company = new Company();
        $company_info = $company->getInfoCompanyByID($id);
        return $company_info;
    }
    function editCompany(CompanyAddRequest $request){
        $id = $request->id;
        $name = $request->name;
        $about = $request->intro;
        $company = new Company();
        $company->editInfoCompany($id,$name,$about);
        $kq = array();
        $kq['status'] = "Success";
        $kq['error'] = "null";
        return json_encode($kq);
    }
    /*Edit Machine with value from Modal of Machine List Page*/
    function editMachine(MachineEditRequest $request){
        $user_id = Session::get('user_id');
        $id = $request->id;
        $name = $request->name;
        $model = $request->model;
        $seri = $request->seri;
        $nsx = $request->nsx;
        $about = $request->intro;
        $company = new Company();
        $company->editInfoMachine($id,$name,$model,$seri,$nsx,$about, $user_id);
        $kq = array();
        $kq['status'] = "Success";
        $kq['error'] = "null";
        return json_encode($kq);
    }
    /*Get view for http://localhost/Finance/admin/company-add */
    function getViewAddCompany(Request $request){
        if ($request->staff_info->level == 'admin') {
            return view('admin.company.company_add')->with(['staff_info' => $request->staff_info]);
        } else{
            return redirect('/admin/home');


        }
    }
    function getViewAddMachine(Request $request){
        if ($request->staff_info->level == 'admin') {
            $company = new Company();
            $list = $company->getListDemo();
            return view('admin.machine.machine_add')->with(['staff_info' => $request->staff_info, 'list' => $list]);
        } else{
            return redirect('/admin/home');
        }
    }
    /*Add new Company*/
    function addCompany(CompanyAddRequest $request){
        $kq = array();
        $company = new Company();
        if ($company->checkExistCompanyName($request->name)) {
            $kq['status']= "Error";
            $kq['error'] = "Tên trùng";
        } else {
            $id = $company->addCompanyAndGetID($request->name, $request->intro);
            $kq = array();
            $kq['status'] = "Success";
            $kq['error'] = null;
        }
        return json_encode($kq);
    }
    function addMachine(MachineAddRequest $request){
        $kq = array();
        $company = new Company();
        if ($company->checkExistMachineName($request->name, $request->company_id, $request->model, $request->seri,$request->nsx)) {
            $kq['status']= "Error";
            $kq['error'] = "Tên trùng";
        } else {
            $id = $company->addMachineAndGetID($request->name,$request->company_id, $request->model, $request->seri,$request->nsx, $request->intro);
//        if($request->logo !=''){
//            $logo = env('SERVER_GET').'public/avatar/avatar'.$id;
//            $file = $request->file('logo')->storeAs('avatar','avatar'.$id,'public');
//            $scale->changeLogoPath($id,$logo);
//        }
            $kq = array();
            $kq['status'] = "Success";
            $kq['error'] = null;
        }
        return json_encode($kq);
    }
    function getViewHistoryMachine(Request $request){
        $machine = new Company();
        $logs = $machine->getOperationLogByID($request->id);
        foreach ($logs as &$log){
            if ($log->user_id != 0){
                $staff = new Staff();
                $user = $staff->getInfoById($log->user_id);
                $log->user_id = $user->email;
            } else {
                $log->user_id = 'Không xác định';
            }
            if($log->name_log != ''){
                $str_log = $log->name_log;
                $split = explode('] => [',$str_log);
                $log->before_name = ltrim($split[0],'[');
                $log->after_name= rtrim($split[1],']');
            }
            if($log->model_log != ''){
                $str_log = $log->model_log;
                $split = explode('] => [',$str_log);
                $log->before_model = ltrim($split[0],'[');
                $log->after_model= rtrim($split[1],']');
            }
            if($log->seri_log != ''){
                $str_log = $log->seri_log;
                $split = explode('] => [',$str_log);
                $log->before_seri = ltrim($split[0],'[');
                $log->after_seri =rtrim($split[1],']');
            }
            if($log->nsx_log != ''){
                $str_log = $log->nsx_log;
                $split = explode('] => [',$str_log);
                $log->before_nsx = ltrim($split[0],'[');
                $log->after_nsx =rtrim($split[1],']');
            }
            if($log->content_log != ''){
                $str_log = $log->content_log;
                $split = explode('] => [',$str_log);
                $log->before_content = ltrim($split[0],'[');
                $log->after_content =rtrim($split[1],']');
            }
        }
        $machine_info = json_decode($machine->getInfoMachineByID($request->id));
        return view('admin.machine.machine_history')->with(['staff_info'=>$request->staff_info, 'logs'=>$logs, 'machine_info'=>$machine_info]);
    }

}