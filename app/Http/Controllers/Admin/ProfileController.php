<?php

namespace App\Http\Controllers\Admin;

use App\Bank;
use App\CategoryConsult;
use App\CategoryConsultStaff;
use App\Http\Requests\StaffEditByStaffRequest;
use App\Staff;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    function getViewEdit(Request $request){




        return view('admin.profiles.edit', ['staff_info'=>$request->staff_info]);
    }
    function edit(StaffEditByStaffRequest $request){
        $input = array();
        $input = $request->all();
        $staff = new Staff();
        $kq = $staff->updateStaffFromStaff($input);
        return $kq;
    }

}
