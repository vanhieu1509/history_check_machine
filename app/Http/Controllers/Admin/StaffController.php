<?php
/**
 * Created by PhpStorm.
 * User: Hung
 * Date: 10/7/2016
 * Time: 9:11 AM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\StaffAddRequest;
use App\Http\Requests\StaffChangePasswordRequest;
use App\Http\Requests\StaffEditRequest;
use App\Staff;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

class StaffController extends Controller
{
    function getViewAddStaff(Request $request){
        if ($request->staff_info->level == 'admin') {
            return view('admin.staff.staff_add', ['staff_info' => $request->staff_info]);
        } else {
            return redirect('/admin/home');
        }
    }

    function addStaff(StaffAddRequest $request){
      
        $date = $request->date;
        $fullname = $request->fullname;
        $email = $request->email;
        $password = $request->password;
        $number_phone = $request->number_phone;
        $slt_level = $request->slt_level;
//        $cmnd = $request->cmnd; Từ từ rồi sẽ có

        $staff = new Staff();
        $staff_id = $staff->addStaffGetId($email,$password,$fullname,$slt_level,$number_phone);
        $kq = array();
        $kq['status']= "Success";
        $kq['error']= null;
        return json_encode($kq);
    }

    function getViewListStaff(Request $request){
        if ($request->staff_info->level == 'admin') {
            return view('admin.staff.staff_list', ['staff_info' => $request->staff_info]);
        } else {
            return redirect('/admin/home');
        }
    }
    function getListStaff(){
        $staff = new Staff();
        $staff_list = $staff->getList();
        return Datatables::of($staff_list)
            ->editColumn('cb_select',' 
                             <div class="text-center" style="display: block; align-items: center">
                                 <input  type="checkbox" class="cb_select text-center ">
                            </div>
              ')
            ->editColumn('fullname','<div class="text-center">{{$fullname}}</div>')
            ->editColumn('email','<div class="text-center">{{$email}}</div>')
            ->editColumn('level','<div class="text-center">@if($level == \'st_consult\') Tư Vấn Viên @elseif($level == \'st_news\') Nhân Viên @elseif($level == \'admin\') Admin  @endif</div>')
            ->editColumn('number_phone','<div class="text-center">{{$number_phone}}</div>')
            ->make(true);
    }
    function removeStaff(Request $request){
        $staff = new Staff();
        $arr_email = array();
        $arr_email=  $request->array_email;

        for($i = 0; $i<count($arr_email); $i++){
            $info = $staff->getInfoByEmail($arr_email[$i]);
            Storage::delete('public/staff/avatar/avatar'.$info->id);
            $staff->remove($arr_email[$i]);

        }
        return "Removed";

    }
    function getInfoStaff(Request $request){
        $email = $request->email;
        $staff = new Staff();
        //id fullname sdt bank_id level avatar
        $staff_info = $staff->getInfoByEmail($email);
        $info = array(
            'id'=> $staff_info->id,
            'fullname'=>$staff_info->fullname,
            'level'=>$staff_info->level,
            'number_phone'=>$staff_info->number_phone,
        );
        return json_encode($info);
    }
    function editStaff(StaffEditRequest $request){
        $staff_id = $request->id;
        $email = $request->email;
        $fullname = $request->fullname;
        $level = $request->level;
        $number_phone = $request->number_phone;
        $staff = new Staff();
        $staff->updateStaff($email,$fullname,$level,$number_phone);
        $kq = array();
        $kq['status']= "Success";
        $kq['error']= null;
        return json_encode($kq);
    }

    function getViewChangePasswordStaff(Request $request){
        return view('admin.staff.staff_change_password')->with(['staff_info'=>$request->staff_info]);
    }

    function changePasswordStaff(StaffChangePasswordRequest $request){
        $email = $request->modal_email;
        $password = $request->password;
        $staff = new Staff();
        $staff->ResetPassword($email,$password);
        $kq = array();
        $kq['status']= "Success";
        $kq['error']= null;
        return json_encode($kq);
    }



    function findStaff(Request $request){
        $email = $request->email;

        $staff = new Staff();
        $staff_list = $staff->FindAndGetInfo($email);
        return Datatables::of($staff_list)
            ->editColumn('fullname','<div class="text-center">{{$fullname}}</div>')
            ->editColumn('email','<div class="text-center">{{$email}}</div>')
            ->editColumn('level','<div class="text-center">{{$level}}</div>')
            ->editColumn('number_phone','<div class="text-center">{{$number_phone}}</div>')
            ->make(true);
    }
}