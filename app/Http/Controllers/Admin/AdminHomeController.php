<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\StaffEditByStaffRequest;
use App\Staff;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddCustomerRequest;
use Illuminate\Support\Facades\Session;

class AdminHomeController extends Controller
{
    function getViewHome(Request $request)
    {
        //Count consults on today
        $consult = new Consult();
        $count_consult_today = $consult->countConsultToday();
        //Count news on today
        $new = new News();
        $count_news_today = $new->countNewsToday();
        //Count customer on today
        $customer = new Customer();
        $count_customer_today = $customer->countCustomerToday();
        $dt = Carbon::now();
        $subday=$dt->subDays(7);
        $dateCurrent = Carbon::now()->format('Y-m-d');
        $begin = $subday->format('Y-m-d');
        $vistors = new Vistor;
        $check = $vistors->fillDateVisitor($begin,$dateCurrent);
        $visitor=$vistors-> getCountByDay($begin,$dateCurrent);
        
        //dd($visitor);
        return view('admin.home')->with(['staff_info' => $request->staff_info, 'count_consult_today' => $count_consult_today, 'count_news_today' => $count_news_today, 'count_customer_today' => $count_customer_today,'visitor'=>$visitor]);
    }

    function getViewProfile(Request $request){
        $bank = new Bank();
        $bank_name = $bank->getBankNameById($request->staff_info->bank_id)[0]->fullname;
        $category_consult_name = array();
        $category_consult_staff = new CategoryConsultStaff();
        $list_category_consult_by_id = $category_consult_staff->getCategoryConsultByStaffID($request->staff_info->id);
        foreach ($list_category_consult_by_id as $category_consult_item){
            $category_consult = new CategoryConsult();
            $cate_consult_name = $category_consult->getNameCategoryConsultById($category_consult_item->category_consult_id);
            array_push($category_consult_name,$cate_consult_name);
        }
        return view('admin.profile', ['bank_name'=>$bank_name,'staff_info'=>$request->staff_info, 'category_consult_name'=>$category_consult_name]);
    }
    function edit(StaffEditByStaffRequest $request){
        $input = array();
        $input = $request->all();
        $staff = new Staff();
        $kq = $staff->updateStaffFromStaff($input);
        return $kq;
    }

    function logout()
    {
        Session::forget('email');
        return redirect()->route('staff/login');
    }
}
