<?php

namespace App\Http\Controllers;
use App\Scale;
use App\Staff;
use App\Company;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Yangqi\Htmldom\Htmldom;
class HomeController extends Controller
{
    function getViewHome( Request $request){
        //Get News - Tin Tức
        $company  = new Company();
        $list = $company->getListDemo();
        return response()->view('guest.index',['list'=>$list
        ]);
    }
    function index(){
        return view('Comparisons.exchange');
    }

    function getNewsInHome($slug=null,$id=null){

        if (empty($slug) || empty($id) || !is_numeric($id)){
            return view('errors.staff.404');
        }else{
            $news = new News();
            $news_current = News::find($id);
            if (isset($news_current)){
                if($slug === $news_current->slug){
                    $number_view_current = $news_current['number_view']+1;
                    $news_current->update(['number_view'=>$number_view_current]);
                    $info = $news->getInfoById($id);
                    $new_news = $news->getNewsforIndex(6);
                    $popular_news = $news->getPolularNews(6);
                    return view('guest.news.news',['news'=>$info,'new_news'=>$new_news,'popular_news'=>$popular_news]);
                }else{
                    return view('errors.staff.404');
                }
            }else{
                return view('errors.staff.404');
            }
        }
    }
    function getViewAbout(){
        $count_customer = Customer::count();
        $count_staff = Staff::where('level','st_consult')->count();
        $visitor = new Vistor();
        $count_number_access_day = $visitor->countVisitorPerDay();
        return view('guest.about',['count_customer'=>$count_customer,'count_staff'=>$count_staff,'count_number_access_day'=>$count_number_access_day]);
    }
    function getViewNationalNews(){
        $news  = new News();
        $news_list = $news->getListNews(1,5);
        $new_news = $news->getNewsforIndex(6);
        $popular_news = $news->getPolularNews(6);
        return view('guest.news.national_news',['news_list'=>$news_list,'new_news'=>$new_news,'popular_news'=>$popular_news]);
    }

    function getViewWorldNews(){
        $news  = new News();
        $news_list = $news->getListNews(2,5);
        $new_news = $news->getNewsforIndex(6);
        $popular_news = $news->getPolularNews(6);
        return view('guest.news.world_news',['news_list'=>$news_list,'new_news'=>$new_news,'popular_news'=>$popular_news]);
    }
    function getViewPromotionNews(){
        $news  = new News();
        $news_list = $news->getListNews(3,5);
        $new_news = $news->getNewsforIndex(6);
        $popular_news = $news->getPolularNews(6);
        return view('guest.news.promotion_news',['news_list'=>$news_list,'new_news'=>$new_news,'popular_news'=>$popular_news]);
    }
    function getViewSendConsult(){
        $province = new Province;
        $categoryConsult = new CategoryConsult;
        $provinces = $province->getProvince();
        $categoryConsults = $categoryConsult ->getCategory();
        return view('guest.form_consult',['provinces' => $provinces,'categoryconsults'=>$categoryConsults]);
    }
    function getViewGold(){
        $html = new Htmldom('http://giavangvn.org/');
        $table = $html->find('table')[0];
        return view('guest.gold_price')->with('table',$table);
    }
    function getViewFAQ(){
        $new = new News;
        $faq = $new->getFaqNew();
        return view('guest.faq')->with('faq',$faq);
    }

    function getViewExchange(){
        $catemoneys = new CategoryMoney;
        $exchanges = new Exchange;
        $catemoney = $catemoneys->getCateMoney();
        $exchange = $exchanges->getExchangeByCategory(1);
        return view('guest.exchange',['catemoney'=>$catemoney,'exchange'=>$exchange]);
    }
    function getViewToolRateSaving(){
        return view('guest.tools.tool_tietkiem');
    }
    function getViewToolBorrow(){
        return view('guest.tools.tool_vayvon');
    }
    function updateNewsCafeF()
    {
        $html = new Htmldom('http://cafef.vn/tai-chinh-ngan-hang.chn');
        $table = $html->find('li.top')[0];
        $title = $table ->find('a')[0]->title;
        $link = $table ->find('a')[0]->href;
        $img1 = $table ->find('a')[0]->find('img')[0]->src;
        $tomtat = $table ->find('p')[1]->innertext;
        $html=new Htmldom('http://cafef.vn'.$link);
        $body = $html->find('div.newsbody')[0]->innertext;

        $img = $html->find('div.big-avatar-wrap');
        if(!empty($img))
        {
            $image = $img[0]->find('img')[0]->src;
            $showimg = 1;
        }
        else
        {
            $image= $img1;
            $showimg = 0;
        }
        $object = new \stdClass();

        $object -> category_news_id = 1;
        $object -> title =$title;
        $object ->  slug=str_slug($title,'-');
        $object ->  sumary=$tomtat;
        $object ->  contenta=$body;
        $object ->  source='http://cafef.vn'.$link;
        $object ->  staff_id=0;
        $object ->   img=$image;
        $object -> showimg = $showimg;

        $news = new News ;
        $newsContain = $news->checkSource($object->source);

        if ($newsContain==1)
        {
            $news->addNews($object);
            return 'ok';
        }
        else return 'false';
    }
    function updateNewsVneconomy()
    {
        $html= new Htmldom('http://vneconomy.vn/tai-chinh.htm');
        $table = $html->find('div.boxhdcmleft')[0];
        $tomtat = $table ->find('p.hdcontencmmid')[0]->innertext;
        // $image = $table ->find('div.hdcimgfoucs')[0]->find('img')[0]->src;
        $link = $table ->find('h2')[0]->find('a')[0]->href;
        $title = $table ->find('h2')[0]->find('a')[0]->innertext;
        $html=new Htmldom('http://vneconomy.vn'.$link);

        $img = $html->find('span.spimgbaiviet')[0]->find('img')[0]->src;
        $body = $html->find('div.detailsbaiviet')[0]->innertext;
        $object = new \stdClass();
        $object -> category_news_id = 1;
        $object -> title =$title;
        $object ->  slug=str_slug($title,'-');
        $object ->  sumary=$tomtat;
        $object ->  contenta=$body;
        $object ->  source='http://vneconomy.vn'.$link;
        $object ->  staff_id=0;
        $object ->   img=$img;
        $object ->   showimg=1;

        $news = new News ;
        $newsContain = $news->checkSource($object->source);

        if ($newsContain==1)
        {
            $news->addNews($object);
            return 'ok';
        }
        else return 'false';

    }
    function getModel(){
        $term = isset($_GET['term'])? $_GET['term']:false;
        $scale = new Scale();
        $result = $scale->getScaleIdLikeName($term);
//        var_dump($result);
        $data = array();
        foreach ($result as $item) {
            $item = (array) $item;

            $data[] = array('id'=>$item['id'],'value'=>$item['model'].' - '. $item['brand']);
        }
        echo json_encode($data);
    }
    function getNameCompnay(){
        $term = isset($_GET['term'])? $_GET['term']:false;
        $company = new Company();
        $result = $company->getCompanyIdLikeName($term);
//        var_dump($result);
        $data = array();
        foreach ($result as $item) {
            $item = (array) $item;

            $data[] = array('id'=>$item['id'],'value'=>$item['name']);
        }
        echo json_encode($data);
    }
    function searchMachine(request $request){
        $machine = new Company();
        $result = $machine->getMachine($request->id);
        echo json_encode($result);
    }
    function searchHistory(request $request){
        $machine = new Company();
        $result = $machine->getHistory($request->id_machine);
        $str = "<table class=\"table table-bordered table-sm\">
                <thead class=\"thead-dark\">
                <tr>
                    <th>Tên</th>
                    <th>Model</th>
                    <th>Seri</th>
                    <th>Nhà sản xuất</th>
                    <th>Ghi chú kiểm tra</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>$result->name</td>
                    <td>$result->model</td>
                    <td>$result->seri</td>
                    <td>$result->nsx</td>
                    <td>$result->content</td>
                </tr>
                </tbody>
            </table>";
        $result->kq = $str;
        echo json_encode($result);
    }
}
