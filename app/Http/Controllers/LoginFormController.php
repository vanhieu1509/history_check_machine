<?php

namespace App\Http\Controllers;

use App\CheckLogin;
use App\Http\Requests\ForgotPasswowrdRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserLoginRequest;
use App\Mail\ResetPasswordMail;
use App\ResetPassword;
use App\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class LoginFormController extends Controller
{
    /*LoginFormController xử lý các yêu cầu sau:
        - Login
        - Khi Staff quên Password
        - Reset Password cho Staff
    */
    /*------------------------------------------------------------------------------------------------------*/
    /*-----------------------------------------------LOGIN--------------------------------------------------*/
    /*-------------------------------------------------------------------------------------------------------*/
    /* ___Hiển thị giao diện khi người dùng truy cập vào đường dẫn trong Route.___*/
    function getViewLogin(){
        return view('staff.login');
    }
    /*
        Xử lý dữ liệu người dùng gửi lên đã được xử lý bên UserLoginRequest.
        Kiểm tra email và password của staff có trùng với dữ liệu ở trong Database hay không.
            - Nếu có thì lưu Session của Staff với giá trị là email của họ, đồng thời trả về dữ liệu với status = Success
                   và quyền hạn của người đăng nhập là admin hay ttv
            - Nếu không thì trả về lỗi: "Sai Email hoặc Password"
    */
    function Login(UserLoginRequest $request){
        $kq = array(
            'status' => 'Failed',
            'error' =>'null',
            'level'=>'null'
        );
        $email = $request->email;
        $password = $request->password;
        $staff = new Staff();
        $user = $staff->CheckLogin($email,$password);
        if(count($user)>0){
            Session::push('email', $request->email);
            Session::put('user_id', $user[0]->id);
            $kq['status'] = "Success";
            $kq['level'] = $user[0]->level;
            $result = json_encode($kq);
        }else{
            $kq['status'] = "Failed";
            $kq['error'] = "Sai Email hoặc Mật Khẩu";
            $result = json_encode($kq);
        }
        return $result;
    }


    /*-----------------------------------------------------------------------------------------------------------------*/
    /*------------------------------------------------FORGOT-PASSWORD--------------------------------------------------*/
    /*-----------------------------------------------------------------------------------------------------------------*/
    /* ___Hiển thị giao diện khi người dùng truy cập vào đường dẫn trong Route.___*/
    function getViewForgot(){
        return view('staff.forgot_password');
    }
    /*
        Xử lý dữ liệu người dùng gửi lên đã được xử lý bên ForgotPasswowrdRequest, tiến hành kiểm tra Email có tồn tại
        hay không.
            - Nếu không thì trả về lỗi cho người dùng.
            - Có tồn tại thì tạo một record bên trong bảng reset_passwords với các thuộc tính:
                + staff_id: Để xác định là yêu cầu reset của nhân viên nào.
                + code: Để đánh dấu, bảo mật quyền reset đối với mỗi yêu cầu
            Sau đó, gửi một Email với đường dẫn đi kèm với staff_id, email và code tới địa chỉ Email mà người dùng đã nhập.
    */
    function Forgot(ForgotPasswowrdRequest $request){
        $kq = array();
        $email = $request->email;
        $staff = new Staff();
        $user = $staff->CheckEmailContain($email);
        if(count($user)>0){
            $staff_id = $staff->getIdStaff($email);
            $code = $this->CreateCode();
            $reset_password = new ResetPassword();
            $reset_password->CreateRequestResetPassword($staff_id,$code);
            $content = 'http://khoitoan.xyz/scale/staff/reset-password?i='.$staff_id.'&e='.$email.'&c='.$code;
            Mail::to($email)
                ->send(new ResetPasswordMail($content,$email));
            $kq['status'] = "Success";
            $result = json_encode($kq);
        }else{
            $kq['status'] = "Failed";
            $kq['error'] = "Địa chỉ Email không tồn tại!";
            $result = json_encode($kq);
        }
        return $result;
    }


    /*-----------------------------------------------------------------------------------------------------------------*/
    /*-------------------------------------------------RESET-PASSWORD--------------------------------------------------*/
    /*-----------------------------------------------------------------------------------------------------------------*/
    /* ___Hiển thị giao diện khi người dùng truy cập vào đường dẫn trong Route.___*/
    function getViewReset(Request $request){
        $staff_id = $request->i;
        $email = $request->e;
        $code = $request->c;
        if(!empty($staff_id) && !empty($email) && !empty($code)){
            $reset_password = new ResetPassword();
            $deadline = $reset_password->CheckContainRequest($staff_id,$code);
            if(count($deadline) >0){
                return view('staff.reset_password')->with('email',$email);
            }elsE{
                return redirect()->route('reset-password-deadline');
            }
        }else{
            return redirect()->route('reset-password-deadline');
        }

    }
    function getViewResetDeadline(){
        return view('errors.reset_password_deadline');
    }
    function Reset(ResetPasswordRequest $request){
        $kq = array();
        $staff = new Staff();
        $staff->ResetPassword($request->email,$request->password);
        $kq['status'] = "Success";
        $kq['error'] = "Change Password Success";
        $rsp = new ResetPassword();
        $rsp->DeleteRequest($request->email);
        return json_encode($kq);
    }

    /*Hàm này có chức năng tạo ra một chuỗi String bất kì để kèm theo yêu cầu Reset Password của Nhân Viên*/
    public function CreateCode(){
        $string = str_random(40);
        return $string;
    }
}