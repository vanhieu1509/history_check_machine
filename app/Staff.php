<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Staff extends Model
{
    protected $table = 'staff';
    protected $fillable = [
         'emaill', 'password','fullname','level','number_phone'
    ];
    protected $hidden = ['password'];
//    public $timestamps = true;


    public  function bank(){
        return $this->belongsTo('App\Bank');
    }

    public  function category_consult(){
        return $this->belongsToMany('App\CategoryConsult');
    }
    public function category_consult_staff(){
        return $this->hasMany('App\CategoryConsultStaff');
    }

    public  function check_logins(){
        return $this->hasOne('App\CheckLogin');
    }
    public function reset_password(){
        return $this->hasOne('App\ResetPassword');
    }

    function addStaffGetId($email,$password,$fullname,$level,$number_phone){

        $staff = new Staff();
        $staff->email = $email;
        $staff->password = md5($password);
        $staff->fullname = $fullname;
        $staff->level = $level;
        $staff->number_phone = $number_phone;
        $staff->save();
        $id = $staff->id;
        return $id;
    }
    function changeAvatarPath($id,$path){
        DB::table('staff')->where('id',$id)->update(['avatar'=>$path]);
    }
//    Login form
    function getIdStaff($email){
        $id = DB::table('staff')->where('email',$email)->value('id');
        return $id;
    }
    function CheckLogin($email,$password){
        $user = DB::table('staff')->where('email',$email)->where('password',md5($password))->get()->toArray();
        return $user;
    }
    function CheckEmailContain($email){
        $user = DB::table('staff')->where('email',$email)->get();
        return $user;
    }

    function ResetPassword($email, $password){
        DB::table('staff')->where('email',$email)->update(['password'=>md5($password)]);
    }
    function getList(){
        $list = Staff::select(['id','fullname','email','level','number_phone'])->where('level','st_news');
        return $list;
    }
    function remove($email){
        DB::table('staff')->where('email',$email)->delete();
    }
    function getInfoByEmail($email){
        $staff = DB::table('staff')->select('*')->where('email',$email)->first();
        return $staff;
    }
    function getInfoById($id){
        $staff = DB::table('staff')->select('*')->where('id',$id)->first();
        return $staff;
    }
    function updateStaff($email, $fullname, $level, $number_phone){
        DB::table('staff')->where('email',$email)->update([
            'fullname'=>$fullname,
            'level'=>$level,
            'number_phone'=>$number_phone,
        ]);
    }
    function updateStaffFromStaff($input){
        if(($input['password'] =='') && ($input['password_confirmation']  =='')){
            unset($input['password']);
            unset($input['password_confirmation']);
        }else{
            $input['password'] = md5($input['password']);
        }
        DB::beginTransaction();
        try {
            $staff = Staff::find($input['id']);
            $staff->update($input);
            $kq['status'] = "Success";
            DB::commit();
        } catch (Exception $e) {
            $kq['status'] = "Tạm thời không thể thay đổi thông tin!";
            DB::rollback();
        }
        return json_encode($kq);
    }
    public static function getLevelStaffByEmail($email){
        $result = DB::table('staff')->select('level')->where('email',$email)->first();
        return $result;
    }
    public function FindAndGetInfo($email)
    {
        $user = DB::table('staff')->select(['fullname','email','level','number_phone'])->where('email','LIKE',$email.'%');
        return $user;
    }
    public function getListDemo(){
        $result = DB::table('staff')->select(['id', 'fullname'])->orderBy('fullname')->get();
        return $result;
    }
}
