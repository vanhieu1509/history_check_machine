<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ResetPassword extends Model
{
    protected $table = 'reset_passwords';
    protected $fillable = [
        'staff_id','code','created_at'
    ];
    public $timestamps = false;
    public function staff(){
        return $this->belongsTo('App\Staff');
    }
    function CreateRequestResetPassword($staff_id,$code){
        $datetime = $this->getDateCurrent();
        $id = DB::table('reset_passwords')->where('staff_id',$staff_id)->value('id');
        if(count($id) >0){
            ResetPassword::find($id)->delete();
        }
        DB::table('reset_passwords')->insert(
            ['staff_id' => $staff_id, 'code' => $code,'created_at'=>$datetime]
        );
    }

    function CheckContainRequest($staff_id,$code){
        $request = DB::table('reset_passwords')->where('staff_id',$staff_id)->where('code',$code)->where('created_at','>=',$this->getTimeDeadlineRequest())->get();
        return $request;
    }
    function DeleteRequest($email){
        ResetPassword::with('staff')->where('staff_id',DB::table('staff')->where('email',$email)->value('id'))->delete();

    }
    function getDateCurrent()
    {
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        return date("Y-m-d H:i:s");
    }
    public function getTimeDeadlineRequest(){
        //Get time Current -20s
        $time = Carbon::now("Asia/Ho_Chi_Minh");
        $time = $time->subSeconds(200000000000000);
        return $time;
    }
}
