<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Category_Product;
use App\CategoryRequest;
use App\ResetPassword;
use App\Staff;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yangqi\Htmldom\Htmldom;
use App\Bank;
use App\Province;
use App\CategoryConsult;
use App\News;
use App\Product;
use App\CategoryMoney;
use App\Exchange;
use App\RateSaving;
use Yajra\Datatables\Facades\Datatables;

//For Guest
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@getViewHome', 'middleware' => 'checkadmin']);
Route::get('/getmodel', ['as' => 'get-models', 'uses' => 'HomeController@getModel']);
Route::get('admin/getcompany', ['as' => 'get-company', 'uses' => 'HomeController@getNameCompnay']);
Route::get('/getmachine', ['as' => 'search-models', 'uses' => 'HomeController@searchMachine']);
Route::get('/gethistory', ['as' => 'search-history', 'uses' => 'HomeController@searchHistory']);

//End For Guest

Route::get(
	'docs/files/{filename}',
	function ($filename) {
		if (!(request()->session()->has('email'))) {
			return redirect('staff/login');
		}
		$path = public_path('uploads/docs/files/' . $filename);

		if (!File::exists($path)) {
			abort(404);
		}

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}
);

Auth::routes();
/*TEMP*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*//*-------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------Admin------------------------------------------------------------*/

Route::get(
	'docs/files/{folder}/{filename}',
	function ($folder, $filename) {
		if (!(request()->session()->has('email'))) {
			return redirect('staff/login');
		}
		$path = public_path('uploads/docs/files/' . $folder . '/' . $filename);

		if (!File::exists($path)) {
			abort(404);
		}

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}
);
Route::get(
	'docs/files/{folder1}/{folder2}/{filename}',
	function ($folder1, $folder2, $filename) {
		if (!(request()->session()->has('email'))) {
			return redirect('staff/login');
		}
		$path = public_path('uploads/docs/files/' . $folder1 . '/' . $folder2 . '/' . $filename);

		if (!File::exists($path)) {
			abort(404);
		}

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}
);
Route::get(
	'docs/files/{folder1}/{folder2}/{folder3}/{filename}',
	function ($folder1, $folder2, $folder3, $filename) {
		if (!(request()->session()->has('email'))) {
			return redirect('staff/login');
		}
		$path = public_path('uploads/docs/files/' . $folder1 . '/' . $folder2 . '/' . $folder3 . '/' . $filename);

		if (!File::exists($path)) {
			abort(404);
		}

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}
);
Route::get(
	'docs/files/{folder1}/{folder2}/{folder3}/{folder4}/{filename}',
	function ($folder1, $folder2, $folder3, $folder4, $filename) {
		if (!(request()->session()->has('email'))) {
			return redirect('staff/login');
		}
		$path = public_path('uploads/docs/files/' . $folder1 . '/' . $folder2 . '/' . $folder3 . '/' . $folder4 . '/' . $filename);

		if (!File::exists($path)) {
			abort(404);
		}

		$file = File::get($path);
		$type = File::mimeType($path);

		$response = Response::make($file, 200);
		$response->header("Content-Type", $type);

		return $response;
	}
);
/*-------------------------------------------------------------------------------------------------------------------*/
Route::group(
	['prefix' => 'admin', 'middleware' => 'checkadmin'],
	function () {
		Route::get('home', ['as' => 'admin/home', 'uses' => 'Admin\MachineController@getViewListMachine']);

		Route::get('history-machine', 'Admin\MachineController@getViewHistoryMachine');

		Route::get('company-add', 'Admin\MachineController@getViewAddCompany');
		Route::get('company-list', 'Admin\MachineController@getViewListCompany');
		Route::get('machine-list', 'Admin\MachineController@getViewListMachine');
		Route::get('machine-add', 'Admin\MachineController@getViewAddMachine');
		Route::get('company-getInfo', ['as' => 'controller/admin/company-getInfo', 'uses' => 'Admin\MachineController@getInfoCompany']);
		Route::get('machine-getInfo', ['as' => 'controller/admin/machine-getInfo', 'uses' => 'Admin\MachineController@getInfoMachine']);

		/*Staff*/
										Route::get('staff-list', 'Admin\StaffController@getViewListStaff');
		Route::get('staff-add', 'Admin\StaffController@getViewAddStaff');
		Route::get('staff-change-password', 'Admin\StaffController@getViewChangePasswordStaff');

		/*Profile*/
										Route::get('profile', 'Admin\AdminHomeController@getViewProfile');
		Route::get('logout', 'Admin\AdminHomeController@logout');
		Route::get('profile', 'Admin\ProfileController@getViewEdit');
	}
);

/*-------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------Staff------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/

/*Group Login Form - GET VIEW*/
Route::group(
	['prefix' => 'staff'],
	function () {
    //Các Route cho Staff Khi đăng nhập
		Route::group(
			['prefix' => '', 'middleware' => 'checkstaff'],
			function () {
				Route::get('home', ['as' => 'staff/home', 'uses' => 'Staff\HomeController@getViewHome']);
				/*Consult*/
																				Route::group(
					['prefix' => '', 'middleware' => 'checkstaff_consult'],
					function () {
						Route::get('consult-list', 'Staff\ConsultController@getViewListConsult');
					}
				);
				/*News*/
																				Route::group(
					['prefix' => '', 'middleware' => 'checkstaff_news'],
					function () {
						Route::get('news-list', 'Staff\NewsController@getViewListNews');
						Route::get('news-add', 'Staff\NewsController@getViewAddNews');
					}
				);
				/*Profile*/
																				Route::get('profile', 'Staff\ProfileController@getViewEdit');
				Route::get('logout', 'Staff\HomeController@logout');
			}
		);
		Route::group(
			['prefix' => '', 'middleware' => 'checkguest'],
			function () {
				Route::get('login', ['as' => 'staff/login', 'uses' => 'LoginFormController@getViewLogin']);
				Route::get('forgot-password', 'LoginFormController@getViewForgot');
				Route::get('reset-password', 'LoginFormController@getViewReset');
				Route::get('reset-password-deadline', ['as' => 'reset-password-deadline', 'uses' => 'LoginFormController@getViewResetDeadline']);
			}
		);
	}
);

/*-------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------EXECUTE------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
Route::group(
	['prefix' => 'controller'],
	function () {
		/*  STAFF CONTROLLER   */
										Route::group(
			['prefix' => 'staff'],
			function () {
				/*Login*/
																				Route::post('login', ['as' => 'controller/login', 'uses' => 'LoginFormController@Login']);
				Route::post('forgot-password', ['as' => 'controller/staff/forgot-password', 'uses' => 'LoginFormController@Forgot']);
				Route::post('reset-password', ['as' => 'controller/staff/reset-password', 'uses' => 'LoginFormController@Reset']);
        //Profile
				Route::post('profile', ['as' => 'controller/staff/staff-edit', 'uses' => 'Staff\ProfileController@edit']);
			}
		);
		/*ADMIN CONTROLLER*/
										Route::group(
			['prefix' => 'admin'],
			function () {
				Route::get('profile', ['as' => 'controller/admin/profile-edit', 'uses' => 'Admin\ProfileController@edit']);

				/*Scale*/
																				Route::get('company-list', ['as' => 'controller/admin/company-list', 'uses' => 'Admin\MachineController@getListCompany']);
				Route::get('machine-list', ['as' => 'controller/admin/machine-list', 'uses' => 'Admin\MachineController@getListMachine']);
				Route::get('company-remove', ['as' => 'controller/admin/company-remove', 'uses' => 'Admin\MachineController@removeCompany']);
				Route::get('machine-remove', ['as' => 'controller/admin/machine-remove', 'uses' => 'Admin\MachineController@removeMachine']);
				Route::get('company-edit', ['as' => 'controller/admin/company-edit', 'uses' => 'Admin\MachineController@editCompany']);
				Route::post('machine-edit', ['as' => 'controller/admin/machine-edit', 'uses' => 'Admin\MachineController@editMachine']);
				Route::post('company-add', ['as' => 'controller/admin/company-add', 'uses' => 'Admin\MachineController@addCompany']);
				Route::post('machine-add', ['as' => 'controller/admin/machine-add', 'uses' => 'Admin\MachineController@addMachine']);
//        Route::post('profile',['as'=>'controller/admin/edit-profile','uses'=>'Admin\ProfileController@edit']);
				/*Staff*/
																				Route::get('staff-list', ['as' => 'controller/admin/staff-list', 'uses' => 'Admin\StaffController@getListStaff']);
				Route::post('edit-profile', ['as' => 'controller/admin/edit-profile', 'uses' => 'Admin\ProfileController@edit']);
				Route::post('staff-add', ['as' => 'controller/admin/staff-add', 'uses' => 'Admin\StaffController@addstaff']);
				Route::get('staff-remove', ['as' => 'controller/admin/staff-remove', 'uses' => 'Admin\StaffController@removeStaff']);
				Route::get('staff-edit', ['as' => 'controller/admin/staff-edit', 'uses' => 'Admin\StaffController@editStaff']);
				Route::get('staff-info', ['as' => 'controller/admin/staff-info', 'uses' => 'Admin\StaffController@getInfoStaff']);
				Route::get('staff-find', ['as' => 'controller/admin/staff-find', 'uses' => 'Admin\StaffController@findStaff']);
				Route::post('staff-change', ['as' => 'controller/admin/staff-change', 'uses' => 'Admin\StaffController@changePasswordStaff']);
			}
		);
	}
);
